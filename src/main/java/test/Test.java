package test;

import org.phm.util.MysqlUtil;

import java.text.SimpleDateFormat;
import java.util.*;
import java.text.DateFormat;

public class Test {
    public static void main(String[] args) throws Exception {
        String sql = MysqlUtil.getHostnameQuery("127.0.0.13");
        System.out.println(sql);

//        String ss = MysqlUtil.getDayStr(MysqlUtil.getDateInt("1608595200"),0);
//        System.out.println(ss);
//        ss = MysqlUtil.getDayStr(MysqlUtil.getDateInt("1608598800"),0);
//        System.out.println(ss);

//        String starttime =MysqlUtil.getViewBy1Str(2020110205);
//        System.out.println(starttime);
//                System.out.println(pad(6,8));

//        Date date = new Date(2020-1900,11-1,1,0,0);
//        long ii = date.getTime();
//        System.out.println(ii);

//
//
//        int starttime =MysqlUtil.addHour(getDateInt("1608595200"),-8);
//        System.out.println(starttime);
//        starttime =MysqlUtil.addHour(getDateInt("1608598800"),-8);
//        System.out.println(starttime);


//        System.out.println(starttime/100);
//        int endtime =MysqlUtil.addHour(getDateInt("1608440399"),-13);
//        System.out.println(endtime);
//        int starttime2 =getDateInt("1608354000")-13;
//        System.out.println(starttime2);
////        System.out.println(starttime/100);
//        int endtime2 =getDateInt("1608440399")-13;
//        System.out.println(endtime2);

        //
//        int currtime = starttime;
//        while (currtime<=endtime) {
//            String mondayStr = getMondayStr(currtime);
//            System.out.println(mondayStr);
//            currtime = addDays(currtime,7);
//            if (currtime>endtime) {
//                mondayStr = getMondayStr(currtime);
//                System.out.println(mondayStr);
//            }
//        }



//        System.out.println("get hour:" + getHour(2020010405));
//
//        System.out.println("get createRandomColor:" + createRandomColor());
//        System.out.println("get month str:" + getMonthStr(getDateInt("1607295600")));
//
//        System.out.println("addMonths :" + addMonths(getDateInt("1607295600"),2));
//        System.out.println("start date:" + getDateInt("1605654000"));
//        System.out.println("end date:" + getDateInt("1608332340"));
//        System.out.println("getMondayStr:" + getMondayStr(2020020405));
//        System.out.println("Day date:" + getDayStr(2020010405));

//        String ss = MysqlUtil.getCallLogActivityReport(2020111210,2020111818,4,1,"5");
//        String ss = MysqlUtil.getCallLogActivityReport(2020111210,2020111818,4,2,"5");
//        String ss = MysqlUtil.getCallLogActivityReport(2020111210,2020111818,4,3,"5");
//        String ss = MysqlUtil.getCallLogActivityReport(2020111210,2020111818,4,4,"5");


//        System.out.println("getCallLogActivityReport:" + ss);


    }
    public static String pad(int number,int digits) {
        String sResult = String.valueOf(number);

        while (sResult.length()<digits) {
            sResult = "0" + sResult;
        }
        return sResult;
    }
    public static String createRandomColor() {
        String red;
        String green;
        String blue;
        Random random = new Random();
        red = Integer.toHexString(random.nextInt(256)).toUpperCase();
        green = Integer.toHexString(random.nextInt(256)).toUpperCase();
        blue = Integer.toHexString(random.nextInt(256)).toUpperCase();

        red = red.length()==1 ? "0" + red : red ;
        green = green.length()==1 ? "0" + green : green ;
        blue = blue.length()==1 ? "0" + blue : blue ;
        String color = "#"+red+green+blue;

        System.out.println(color);

        return color.toUpperCase();
    }
    public static String getDayStr(int iCurrday) {
        int iYear = iCurrday/1000000;
        int iMonth = (iCurrday-iYear*1000000)/10000;
        int iDay = (iCurrday-iYear*1000000-iMonth*10000)/100;
        int iHour =(iCurrday-iYear*1000000-iMonth*10000-iDay*100);

        String[] monthList = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(",");
        return monthList[iMonth-1] + "-"+String.valueOf(iDay);
    }
    public static String getMonthStr(int iCurrday) {
        int iYear = iCurrday/1000000;
        int iMonth = (iCurrday-iYear*1000000)/10000;
        int iDay = (iCurrday-iYear*1000000-iMonth*10000)/100;
        int iHour =(iCurrday-iYear*1000000-iMonth*10000-iDay*100);

        String[] monthList = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(",");
        return monthList[iMonth-1] + "-"+String.valueOf(iYear);
    }
    public static int addMonths(int iCurrday,int months) {
        int iYear = iCurrday/1000000;
        int iMonth = (iCurrday-iYear*1000000)/10000;
        int iDay = (iCurrday-iYear*1000000-iMonth*10000)/100;
        int iHour =(iCurrday-iYear*1000000-iMonth*10000-iDay*100);
        Date date = new Date(iYear-1900,iMonth-1,iDay,iHour,0);
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        cld.add(Calendar.MONTH,months);
        date = cld.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHH");
        return Integer.parseInt(df.format(date));
    }
    public static int getHour(int iCurrday) {
        int iYear = iCurrday/1000000;
        int iMonth = (iCurrday-iYear*1000000)/10000;
        int iDay = (iCurrday-iYear*1000000-iMonth*10000)/100;
        int iHour =(iCurrday-iYear*1000000-iMonth*10000-iDay*100);
        return iHour;
    }
    public static int addDays(int iCurrday,int days) {
        int iYear = iCurrday/1000000;
        int iMonth = (iCurrday-iYear*1000000)/10000;
        int iDay = (iCurrday-iYear*1000000-iMonth*10000)/100;
        int iHour =(iCurrday-iYear*1000000-iMonth*10000-iDay*100);
        Date date = new Date(iYear-1900,iMonth-1,iDay,iHour,0);
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        cld.add(Calendar.DATE,days);
        date = cld.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHH");
        return Integer.parseInt(df.format(date));
    }
    public static String getMondayStr(int iCurrday) {
        int iYear = iCurrday/1000000;
        int iMonth = (iCurrday-iYear*1000000)/10000;
        int iDay = (iCurrday-iYear*1000000-iMonth*10000)/100;
        int iHour =(iCurrday-iYear*1000000-iMonth*10000-iDay*100);
        Date date = new Date(iYear-1900,iMonth-1,iDay);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cld = Calendar.getInstance();
//        System.out.println("date:" + date);
        cld.setTime(date);
//        System.out.println("cld:" + df.format(cld.getTime()));

        int dayWeek = cld.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cld.add(Calendar.DAY_OF_MONTH, -1);
        }
//        System.out.println("cld:" + df.format(cld.getTime()));

        cld.setFirstDayOfWeek(Calendar.MONDAY);//2020100201
        int day = cld.get(Calendar.DAY_OF_WEEK);
        cld.add(Calendar.DATE, cld.getFirstDayOfWeek() - day);
//        System.out.println("cld:" + df.format(cld.getTime()));

        String ss= df.format(cld.getTime());
//        ss= ss.substring(5,7)+"-"+ss.substring(8);

        return ss;
    }

    public static int getDateInt(String sTimestamp) {
        int iDate = 0;
        try {
            Long dateLong =Long.parseLong(sTimestamp)*1000;
            Date date3 = new Date(dateLong);//Long型毫秒数转换为Date型
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            iDate = Integer.parseInt(dateFormat.format(date3));
        }
        catch(Exception e) {
            iDate = 0;
        }
        return iDate;

    }

}