package org.phm.util;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Progressable;

public class UtilSys {

    //日志配置路径
    //正式打包的时候需要修改这个目录
    //测试使用
//    public static final String SYSTEMCONFIGFILENAME = "D:/phm-main-platform/phm-hbase/phm-online-maincontrol/src/main/resources/config/config.properties";
//    public final static String LOG4JCONFFILENAME = "D:/phm-main-platform/phm-hbase/phm-online-maincontrol/src/main/resources/config/log4j.properties";
    //正式环境使用
    public static final String SYSTEMCONFIGFILENAME = "/root/app/spark/config/config.properties";
    public final static String LOG4JCONFFILENAME = "/root/app/spark/config/log4j.properties";

	@SuppressWarnings("finally")
	static public Long Stringtotimestamp(String tsStr) {
		Long oout = null;
        try {  
            Timestamp ts = Timestamp.valueOf(tsStr);
            oout = ts.getTime();
            //System.out.println(mts);  
        } catch (Exception e) {
			e.printStackTrace();
        } finally {
        	return oout;
        }
	}

    /*
     * get current datetime number out:String
     */
    public static String getHostName() {
        String myhosename = "";
        try {
            myhosename = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
			e.printStackTrace();
        } finally {
            return myhosename;
        }
    }

	@SuppressWarnings("finally")
	public boolean writeHiveFile(String filename, Map<String, String> mdata) {
		boolean oout = false;
		PrintWriter out = null;
		FileWriter file = null;
		try {
			file = new FileWriter(filename);
			out = new PrintWriter(file, true);

			String iivalue = "";
			for (Map.Entry<String, String> ientry : mdata.entrySet()) {
				iivalue = iivalue + ientry.getValue().replaceAll(",", "-")
						+ ",";
			}
			out.println(iivalue);
			out.close();
			file.close();
			oout = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return oout;
		}
	}
	
	

	@SuppressWarnings("finally")
	public boolean writeDetailHiveFile(String data_head, String filename,
			Map<String, String> mdata) {
		boolean oout = false;
		PrintWriter out = null;
		FileWriter file = null;
		try {
			file = new FileWriter(filename);
			out = new PrintWriter(file, true);
			for (Map.Entry<String, String> ientry : mdata.entrySet()) {
				out.println(data_head + ientry.getKey() + ","
						+ ientry.getValue());
			}
			out.close();
			file.close();
			oout = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return oout;
		}
	}

	public static byte[] getBooleanArray(byte b) {
		byte[] array = new byte[8];
		for (int i = 7; i >= 0; i--) {
			array[i] = (byte) (b & 1);
			b = (byte) (b >> 1);
		}
		return array;
	}

	public static String toStringHex1(String s) {
		byte[] baKeyword = new byte[s.length() / 2];
		for (int i = 0; i < baKeyword.length; i++) {
			try {
				baKeyword[i] = (byte) (0xff & Integer.parseInt(
						s.substring(i * 2, i * 2 + 2), 16));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			s = new String(baKeyword, "utf-8");// UTF-16le:Not
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * @功能: BCD码转为10进制串(阿拉伯数据)
	 * @参数: BCD码
	 * @结果: 10进制串
	 */
	public static String bcd2Str(byte[] bytes) {
		StringBuffer temp = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			temp.append((byte) ((bytes[i] & 0xf0) >>> 4));
			temp.append((byte) (bytes[i] & 0x0f));
		}
		return temp.toString().substring(0, 1).equalsIgnoreCase("0") ? temp
				.toString().substring(1) : temp.toString();
	}

	/**
	 * @功能: 10进制串转为BCD码
	 * @参数: 10进制串
	 * @结果: BCD码
	 */
	public static byte[] str2Bcd(String asc) {
		int len = asc.length();
		int mod = len % 2;
		if (mod != 0) {
			asc = "0" + asc;
			len = asc.length();
		}
		byte abt[] = new byte[len];
		if (len >= 2) {
			len = len / 2;
		}
		byte bbt[] = new byte[len];
		abt = asc.getBytes();
		int j, k;
		for (int p = 0; p < asc.length() / 2; p++) {
			if ((abt[2 * p] >= '0') && (abt[2 * p] <= '9')) {
				j = abt[2 * p] - '0';
			} else if ((abt[2 * p] >= 'a') && (abt[2 * p] <= 'z')) {
				j = abt[2 * p] - 'a' + 0x0a;
			} else {
				j = abt[2 * p] - 'A' + 0x0a;
			}
			if ((abt[2 * p + 1] >= '0') && (abt[2 * p + 1] <= '9')) {
				k = abt[2 * p + 1] - '0';
			} else if ((abt[2 * p + 1] >= 'a') && (abt[2 * p + 1] <= 'z')) {
				k = abt[2 * p + 1] - 'a' + 0x0a;
			} else {
				k = abt[2 * p + 1] - 'A' + 0x0a;
			}
			int a = (j << 4) + k;
			byte b = (byte) a;
			bbt[p] = b;
		}
		return bbt;
	}

	public static String transMapToString(Map map) {
		Map.Entry entry;
		StringBuffer sb = new StringBuffer();
		for (Iterator iterator = map.entrySet().iterator(); iterator.hasNext();) {
			entry = (Map.Entry) iterator.next();
			sb.append(entry.getKey().toString())
					.append("'")
					.append(null == entry.getValue() ? "" : entry.getValue()
							.toString()).append(iterator.hasNext() ? "^" : "");
		}
		return sb.toString();
	}
	public static boolean isStringBegin(String str,String str0) {
		if(str.startsWith(str0)) {
			return true;
		}else {
			return false;
		}
	}
	public static byte[] ObjectToByte(List<String> obj) {
		byte[] bytes = null;
		try {
			// object to bytearray
			ByteArrayOutputStream bo = new ByteArrayOutputStream();
			ObjectOutputStream oo = new ObjectOutputStream(bo);
			oo.writeObject(obj.toString());

			bytes = bo.toByteArray();

			bo.close();
			oo.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return bytes;
		}
	}

	public static String getBytetoString(byte[] idata) {
		String isoString = "";
		try {
			isoString = new String(idata, "UTF8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return isoString;
		}
	}

	/*
	 * get current datetime number out:String
	 */
	public static String getTimeNumber() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		String mdf = df.format(new Date());
		return mdf;// new Date()
	}

	/*
	 * get current datetime number out:String
	 */
	public static String getHTimeNumber() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhh");
		String mdf = df.format(new Date());
		return mdf;// new Date()
	}
	
	public static String getCurrTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String mdf = df.format(new Date());
		return mdf;// new Date()
	}

	public static String getSCurrTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String mdf = df.format(new Date());
		return mdf;// new Date()
	}
    public static String getSCurrHourtoSecondTime() {
        SimpleDateFormat df = new SimpleDateFormat("HHmmss");
        String mdf = df.format(new Date());
        return mdf;// new Date()
    }

	/*
	 * get current datetime number out:String
	 */
	public static String getAddDateOne() {

		SimpleDateFormat df = new SimpleDateFormat("yyyyMM01");
		String mdf = df.format(new Date());
		return mdf;// new Date()

	}

	/*
	 * get current datetime out:String
	 */
	public static String getCurrentTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(new Date());// new Date()

	}

	public static String getHCurrentTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		return df.format(new Date());// new Date()

	}

	/*
	 * get current datetime out:String
	 */
	public static String getCurrentHTime() {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		return df.format(new Date());// new Date()

	}

	public static Boolean getMTime(String icurtime, String btime) {
		// 离线在线计算公式
		Boolean mouturn = false;
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date d2;
			Date d1 = df.parse(icurtime);
			if (btime.indexOf("-") > -1)
				d2 = df.parse(btime);
			else {
				SimpleDateFormat bdf = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				d2 = bdf.parse(btime);
			}
			long diff = d1.getTime() - d2.getTime();// 这样得到的差值是微秒级别
			long days = diff / (1000 * 60 * 60 * 24);
			long hours = (diff - days * (1000 * 60 * 60 * 24))
					/ (1000 * 60 * 60);
			long minutes = (diff - days * (1000 * 60 * 60 * 24) - hours
					* (1000 * 60 * 60))
					/ (1000 * 60);
			// 小于60分钟--进行延迟计算。业务规则为10分钟，由于铁科会延迟15分钟。因此调整成20分钟（王川提供）
			if (minutes < 5) {
				mouturn = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return mouturn;
		}

	}

	/*
	 * function:when speed is zero,
	 * 
	 */
	@SuppressWarnings("finally")
	public static Boolean getZeroSpeedMTime(String icurtime, String btime,int chazhi) {
		// 离线在线计算公式
		Boolean mouturn = false;
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");
			Date d2 = df.parse(btime);
			Date d1 = df.parse(icurtime);
			//if (btime.indexOf("-") == -1)
			//	d2 = df.parse(btime);
			//else {
			//	SimpleDateFormat bdf = new SimpleDateFormat(
			//			"yyyy-MM-dd HH:mm:ss");
			//	d2 = bdf.parse(btime);
			//}
			long diff = d1.getTime() - d2.getTime();// 这样得到的差值是微秒级别
			long days = diff / (1000 * 60 * 60 * 24);
			long hours = (diff - days * (1000 * 60 * 60 * 24))
					/ (1000 * 60 * 60);
			long minutes = (diff - days * (1000 * 60 * 60 * 24) - hours
					* (1000 * 60 * 60))
					/ (1000 * 60);
			// 小于10分钟--进行延迟计算。业务规则为10分钟，由于铁科会延迟15分钟。因此调整成20分钟（王川提供）
			if (minutes < chazhi) {
				mouturn = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return mouturn;
		}

	}

	/*
	 * get current date out:String
	 */
	public static String getCurrentDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(new Date());// new Date()

	}
	
	/*
	 * get current date out:String
	 */
	public static String getCurrentSmallDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		return df.format(new Date());// new Date()

	}

	/*
	 * get current date add date out:String
	 */
	public static String getAddDate(int iadd) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE, iadd);
		date = calendar.getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(date);
		// new Date()
	}

	/*
	 * 得到数据日期中的年
	 */
	public static String getYearString(String mdate) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		Date date = df.parse(mdate);
		return df.format(date);
		// new Date()
	}

	public static String getMonthString(String mdate) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("MM");
		Date date = df.parse(mdate);
		return df.format(date);
		// new Date()
	}

	public static String getDayString(String mdate) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("dd");
		Date date = df.parse(mdate);
		return df.format(date);
		// new Date()
	}

	public static String getAddDateF(int iadd) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE, iadd);
		date = calendar.getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		return df.format(date);

		// new Date()
	}

	/*
	 * get current date add date in:iweek :week number out:String
	 */
	public static String getAddWeekDate(int iweek) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE, iweek * 7 + 1);
		date = calendar.getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(date);
		// new Date()
	}

	/*
	 * getNowDateLong in:format yyyyMMdd out:long
	 */
	public static long getNowDateLong(String format) {
		try{
			SimpleDateFormat df = new SimpleDateFormat(format);
			String mdf = df.format(new Date());
			Date date = df.parse(mdf);
			return date.getTime();
		}catch (Exception e){
			return 0;
		}
	}

	/*
	 * getDateLong in:date format yyyyMMddHHmmss out:long
	 */
	public static long getDateLong(String date ,String format) {
		try{
			SimpleDateFormat df = new SimpleDateFormat(format);
			Date outDate = df.parse(date);
			return outDate.getTime();
		}catch (Exception e){
			return 0;
		}
	}

	public static void getAllFileName(String path, Map<String, List> fileName) {
		File file = new File(path);
		File[] files = file.listFiles();
		String[] names = file.list();
		if (names != null) {
			fileName.put(path, Arrays.asList(names));
		}
		if (files != null)
			if (files.length > 0) {
				for (File a : files) {
					if (a.isDirectory()) {
						getAllFileName(a.getAbsolutePath(), fileName);
					}
				}
			}
	}

	/**
	 * <pre>
	 * 递归的列出文件
	 * 
	 * fileFilter ex.
	 * fileFilter = new FileFilter() {
	 *       public boolean accept(File pathname) {
	 *           return !".svn".equals(pathname.getAbsolutePath());
	 *       }
	 *  };
	 * </pre>
	 * 
	 * @param strPath
	 * @return List<File>
	 * @throws IOException
	 */
	public static List<File> getFileList(String strPath) {
		File dir = new File(strPath);
		List<File> filelist = new ArrayList<File>();
		File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (files[i].isDirectory()) { // 判断是文件还是文件夹
					getFileList(files[i].getAbsolutePath()); // 获取文件绝对路径
				} else if (fileName.endsWith("txt")) { // 判断文件名是否以.avi结尾
					String strFileName = files[i].getAbsolutePath();
					filelist.add(files[i]);
				} else {
					continue;
				}
			}

		}
		return filelist;
	}

	/*
	 * 将文件转换成16进制的字符串的字符串
	 */
	public static String readHexFile(String filename) {
		String oStr = "";
		try {
			File mfile = new File(filename);
			if (mfile.isFile()) {
				FileInputStream stream = new FileInputStream(mfile);
				int c;
				// logger.info(filename);
				while ((c = stream.read()) != -1) {
					if (c <= 15) {
						oStr = oStr + "0" + Integer.toHexString(c);
					} else {
						oStr = oStr + Integer.toHexString(c);
					}
				}
				stream.close();

				stream = null;
			}
			mfile = null;
			return oStr;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static void copfiletohdfs(String localFile, String hdfspath)
			throws IOException {
		String localSrc = localFile;
		String dst = hdfspath;
		// logger.info("------hadoop hdfs put files--start----local FileName:"+localFile+"--to hdfs path:"+hdfspath);
		InputStream in = null;
		FileSystem fs = null;
		FSDataOutputStream out = null;
		try {
			in = new BufferedInputStream(new FileInputStream(localSrc));
			Configuration conf = new Configuration();
			//liqin 2016-12-28 14:55
			conf.set("dfs.datanode.max.xcievers", "8192");//解决HDFS同时打开文件数太小问题
			fs = FileSystem.get(URI.create(dst), conf);
			dst = dst + localFile.substring(localFile.lastIndexOf("/"));
			Path hdfspathfile = new Path(dst);
			if (!fs.exists(hdfspathfile)) {
				// logger.error("------hadoop hdfs put files------Path Name:"+hdfspath+"--is no exist, now to do create path!!");
				fs.create(hdfspathfile);
			}
			out = fs.create(new Path(dst), new Progressable() {
				public void progress() {
					// logger.info("Doing---------hadoop put file");
				}
			});
			org.apache.hadoop.io.IOUtils.copyBytes(in, out, conf);

			if (in != null)
				in.close();

			// 删除本地临时文件--不做删除，由后续流程完成后进行删除
			// FileUtils.forceDelete(new File(localFile));
			// logger.info("------hadoop hdfs put files-----local FileName:"+localFile+"--to hdfs path:"+hdfspath+"--succend!!");
		} catch (IOException e) {
			e.printStackTrace();
			// logger.error(e);
		}
	}

	/*
	 * function:拷贝文件到hdfs的相应目录上。
	 * return:true表示成功，false表示失败
	 * 
	 */
	public static boolean dofiletohdfs(String localFile, String hdfspath)
			throws IOException {
           Path localpath = new Path(localFile);
	       Path dstPath = new Path(hdfspath+localpath.getName()) ;  
	        try{  
				Configuration conf = new Configuration();
				//liqin 2016-12-28 14:55
				//conf.set("dfs.datanode.max.xcievers", "8192");//解决HDFS同时打开文件数太小问题
				//conf.set("dfs.permissions", "false");//设置写入权限为false
	            FileSystem hdfs = FileSystem.get(URI.create(hdfspath), conf); 
	            //dstPath.getFileSystem(conf) ; 
	            hdfs.copyFromLocalFile(false, localpath, dstPath) ; 
	            System.out.println(localpath+"--"+dstPath);
	        }catch(IOException e){
				e.printStackTrace();
	            return false ; 
	        }  
	        return true ;  
	    } 
	/*
	 * funcation 从hdfs下载文件到本地目录
	 * param:src表示hdfs上的文件
	 *       dest是本地文件目录
	 * 
	 */
    public static boolean getFromHDFS(String src , String dst){  
        Path dstPath = new Path(dst) ;  
        try{  
			Configuration conf = new Configuration();
			//liqin 2016-12-28 14:55
			conf.set("dfs.datanode.max.xcievers", "8192");//解决HDFS同时打开文件数太小问题
            FileSystem dhfs = FileSystem.get(URI.create(src), conf);  
            dhfs.copyToLocalFile(false, new Path(src), dstPath) ;  
        }catch(IOException e){
			e.printStackTrace();
            return false ;  
        }  
        return true ;  
    }
	       
	/*
	 *funcation:写入16进制文本文件 
	 * 
	 */
	public static void writeToTxtFile(String filename, String str) {
		// 创建文件
		try {
			
			FileOutputStream fos = new FileOutputStream(filename, true);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "utf-8");
			osw.append(str);
			osw.append("\r\n");
			osw.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Map<String, String> setStringToHashMap(String str) {
		Map<String, String> out = new HashMap<String, String>();
		out.clear();
		String[] liststr = str.split(",");
		for (int i = 0; i < liststr.length; i++) {
			String[] key_value = liststr[i].split("=");
			if (key_value.length >= 2) {
				out.put(key_value[0], key_value[1]);
			} else {
				out.put(key_value[0], "");
			}
			key_value = null;
		}

		return out;
	}

	public void writeListFile(String filename, List<String> datal)
			throws IOException {
		FileWriter writefile = null;
		PrintWriter pwriter = null;
		try {
			writefile = new FileWriter(filename);
			pwriter = new PrintWriter(writefile, true);
			for (int i = 0; i < datal.size(); i++) {
				pwriter.println(datal.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pwriter != null)
				pwriter.close();
			if (writefile != null)
				writefile.close();
		}

	}
   public static String formateDataTime(String itime){
	   String d1 = "";
       if (!(itime == null)&&(!"".equals(itime))){
           try {
               d1 = "20" + itime.substring(0, 2) + "-" + itime.substring(2, 4) + "-" + itime.substring(4, 6) + " " + itime.substring(6, 8)
                       + ":" + itime.substring(8, 10) + ":" + itime.substring(10);
           } catch (Exception e) {
			   e.printStackTrace();
           }
	   
        }
       return d1;
   }
   
	public static Boolean get24HTime(String icurtime, String btime) {
		// 离线在线计算公式
		Boolean mouturn = false;
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date d2;
			Date d1 = df.parse(icurtime);
			if (btime.indexOf("-") > -1)
				d2 = df.parse(btime);
			else {
				SimpleDateFormat bdf = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				d2 = bdf.parse(btime);
			}
			long diff = d1.getTime() - d2.getTime();// 这样得到的差值是微秒级别
			long days = diff / (1000 * 60 * 60 * 24);
			long hours = (diff - days * (1000 * 60 * 60 * 24))
					/ (1000 * 60 * 60);
			long minutes = (diff - days * (1000 * 60 * 60 * 24) - hours
					* (1000 * 60 * 60))
					/ (1000 * 60);
			// 小于60分钟--进行延迟计算。业务规则为10分钟，由于铁科会延迟15分钟。因此调整成20分钟（王川提供）
			if (minutes < 1440) {
				mouturn = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return mouturn;
		}

	}

	/**
	* 将异常堆栈转换为字符串
	* @param aThrowable 异常
	* @return String
	*/
	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}
}
