package org.phm.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

import javax.json.JsonArray;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by siglex.li 2015-02-4.
 *
 */
@SuppressWarnings("deprecation")
public class HbaseUtilNew {
//    private final static String classname = "com.mysql.jdbc.Driver"; //mysql5.0
    private final static String classname = "com.mysql.cj.jdbc.Driver"; //mysql8.0
    private final static String classname_phoenix = "org.apache.phoenix.jdbc.PhoenixDriver";
//    private final static String url_196 = "jdbc:mysql://208.73.232.196:3306/dash?user=hadoop&password=l3tMe.In&useUnicode=true&characterEncoding=UTF8";
//    private final static String url_209 = "jdbc:mysql://208.73.232.209:3306/dash?user=hadoop&password=l3tMe.In&useUnicode=true&characterEncoding=UTF8";
    private final static String url_196 = "jdbc:mysql://208.73.232.196:3306/dash?useSSL=false&serverTimezone=UTC&user=hadoop&password=l3tMe.In&useUnicode=true&characterEncoding=UTF8";
    private final static String url_209 = "jdbc:mysql://208.73.232.209:3306/dash?useSSL=false&serverTimezone=UTC&user=hadoop&password=l3tMe.In&useUnicode=true&characterEncoding=UTF8";
//    private final static String url_phoenix = "jdbc:phoenix:master,worker1,worker2";
    private final static String url_phoenix = "jdbc:phoenix:master,worker1,worker2:2181";//error
    private final static String url_mysql_local = "jdbc:mysql://127.0.0.1:3306/bigdata?useSSL=false&serverTimezone=UTC&user=bigdata&password=RkHado@p202!!&useUnicode=true&characterEncoding=UTF8";


    private static Configuration configuration;
    //maps for relation tables
    private static  Map<String, String> map_opnumber;
    private static  Map<String, String> map_phonebook;
    private static  Map<String, String> map_customer;
    private static int iCallLogNo = 1000000;
//    private static  Map<String, String> map_opnumber;

    static {
        if (configuration == null) {
            configuration = HBaseConfiguration.create();
            // configuration.set("hbase.zookeeper.quorum", quorum);
            configuration.set("hbase.zookeeper.quorum",
                    "master,worker1,worker2");
//            configuration.set("zookeeper.znode.parent", "/hbase-unsecure");
            configuration.set("zookeeper.znode.parent", "/hbase");
            configuration.set("hbase.zookeeper.property.clientPort", "2181");
            //configuration.set("hbase.master", "192.168.137.143:60000");
            configuration.set("hbase.master",
                    "master:60000");
        }

        //load opnumber and phonebook
        LoadMaps();
    }

    @SuppressWarnings("deprecation")
    public HbaseUtilNew() {
    }

    private  java.sql.Connection phoenixConn;

    private static void LoadMaps() {
        System.out.println("LoadMaps");
        map_opnumber = new HashMap<String, String>();
        map_phonebook = new HashMap<String, String>();
        map_customer = new HashMap<String, String>();

    }
    @SuppressWarnings({"deprecation", "resource", "finally"})
    public static Table getTable(String tableName) {
        Table mHTable = null;
        try {
            System.getProperties().setProperty("HADOOP_USER_NAME", "hbase");
            org.apache.hadoop.hbase.client.Connection hConnection = ConnectionFactory.createConnection(configuration);

            //得到连接hash对象里面的hbase表，如果没有就打开连接，得到表
            mHTable = hConnection.getTable(TableName.valueOf(tableName));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return mHTable;
        }
    }

    /*
     * function:get one record of the table; in:table name;rowid out:maphash
     */

    @SuppressWarnings("finally")
    public Map<String, String> getOneFamilyRecord(String tableName,
                                                  String rowKey) {
        Table table = getTable(tableName);

        // get a table's record
        Map<String, String> recordmap = new HashMap<String, String>();
        recordmap.clear();
        try {
            Get get = new Get(rowKey.getBytes());
            Result rs = table.get(get);

            CellScanner cellScanner = rs.cellScanner();
            while (cellScanner.advance()) {
                Cell cell = cellScanner.current();
                recordmap.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))),
                        new String(Bytes.toString(CellUtil.cloneValue(cell))));
            }
//                204                 System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                205                 System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                206                 System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                207                 System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

//            for (KeyValue kv : rs.raw()) {
//                // three key
//                recordmap.put(new String(kv.getFamily()),
//                        new String(kv.getValue()));
//            } // end for
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return recordmap;
        }
    }

    /*
     * function:get all Qualifier record of the table; in:table name;rowid
     * out:rowid key value
     */

    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> getAllFamilyRecord(String tableName) {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tableName);
        Scan s = new Scan();
        // It is highly recommended to specify a startKey and endKey.
        ResultScanner scanner = null;
        String rowid = "";
        try {
            scanner = table.getScanner(s);

            Iterator<Result> iterator = scanner.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            scanner.close();
            return iresult;
        }
    }

    /*
     * function:get all Qualifier record of the table; in:table name;rowid
     * out:rowid key value
     */

    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> getAllQualifierRecord(
            String tableName) {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tableName);
        Scan s = new Scan();
        // It is highly recommended to specify a startKey and endKey.
        // s.setStartRow(Bytes.toBytes("01001686138100001"));
        // s.setStopRow(Bytes.toBytes("01001686138100002"));
        // s.setBatch(1000);
        // s.setCaching(1000);
        ResultScanner scanner = null;
        String rowid = "";
        try {
            scanner = table.getScanner(s);
            Iterator<Result> iterator = scanner.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            scanner.close();
            return iresult;
        }
    }

    /*
     * function:get table count
     * out:count
     */

    @SuppressWarnings("finally")
    public Integer getTableCountByTime(
            String tableName, Date startTime, Date endTime) throws Exception {
        Table table = getTable(tableName);
        Scan s = new Scan();
        s.setTimeRange(startTime.getTime(), endTime.getTime());
        ResultScanner scanner = null;
        int count = 0;
        try {
            scanner = table.getScanner(s);
            // while row leap
            for (Result rr = scanner.next(); rr != null; rr = scanner.next()) {
                count++;
            }
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            scanner.close();
            return count;
        }
    }

    /*
     * function:get one record of the table; in:table name;rowid out:maphash
     */

    public Map<String, String> getOneQualifierRecord(String tableName,
                                                     String rowKey) {
        Table table = getTable(tableName);
        // get a table's record
        Map<String, String> recordmap = new TreeMap<String, String>();
        recordmap.clear();
        try {
            Get get = new Get(rowKey.getBytes());
            Result rs = table.get(get);

            CellScanner cellScanner = rs.cellScanner();
            while (cellScanner.advance()) {
                Cell cell = cellScanner.current();
                recordmap.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                        new String(Bytes.toString(CellUtil.cloneValue(cell))));
            }
//                204                 System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                205                 System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                206                 System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                207                 System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return recordmap;
        }
    }
    /*
     * function:get one record of the table; in:table name;rowid out:maphash
     */

    public Map<String, String> getOneTimeAndQualifierRecord(String tableName,
                                                            String rowKey) {
        Table table = getTable(tableName);
        // get a table's record
        Map<String, String> recordmap = new TreeMap<String, String>();
        recordmap.clear();
        try {
            Get get = new Get(rowKey.getBytes());
            Result rs = table.get(get);

            CellScanner cellScanner = rs.cellScanner();
            while (cellScanner.advance()) {
                Cell cell = cellScanner.current();
                recordmap.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                        new String(String.valueOf(cell.getTimestamp()) + "+"
                                + Bytes.toString(CellUtil.cloneValue(cell))));
            }
//                204                 System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                205                 System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                206                 System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                207                 System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return recordmap;
        }
    }
    /*
     * function:get count of the table; in:table name out:maphash
     */

    public int getTableCount(String tableName) {
        Table table = getTable(tableName);
        Scan s = new Scan();
        ResultScanner scanner = null;
        int count = 0;
        try {
            scanner = table.getScanner(s);
            // while row leap
            for (Result rr = scanner.next(); rr != null; rr = scanner.next()) {
                count++;
            }
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            scanner.close();
            return count;
        }
    }

    /*
     * function:insert one record to the tables in:tablename filed:fieldname and
     * values out:0---insert succend;1---insert error;2--already has the
     * record;3---system error
     */

    public int insertOneQualifierRecord(String itablename, String rowKey,
                                        String family, Map<String, String> iRecord) {
        Table table = getTable(itablename);

//        table.setAutoFlush(false, false);
//        table.setWriteBufferSize(3*1024*1024);
        String qualifier, value;
        int outb = 1;
        qualifier = "";
        try {
            // insert operate
            Put put = new Put(Bytes.toBytes(rowKey));
            for (Map.Entry<String, String> entry : iRecord.entrySet()) {
                qualifier = entry.getKey();
                value = entry.getValue();
                // String turn to bytes;
                put.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier),
                        Bytes.toBytes(value));
                // System.out.println(UtilSys.getCurrentTime()+":"+family+"---"+qualifier+"--"+value);
                // System.out.println(UtilSys.getCurrentTime()+":"+"---insert recored "
                // + rowKey + " to family:" +
                // family+"to qualifier:"+qualifier+"to value:"+value +" ok.");
            }

            table.put(put);
            outb = 0;
            // System.out.println(UtilSys.getCurrentTime()+":"+"---insert recored "
            // + rowKey + " to table " + itablename +" ok.");
            // ///////////////////////////////////////////////////////////////////////////////////////
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return outb;
        }
    }


    public int insertQualifierPut(String itablename, List<Put> iRecord) {
        if ((iRecord == null) || (iRecord.size() == 0))
            return -1;
        Table table = getTable(itablename);
        int outb = 1;
        try {
            table.put(iRecord);
            outb = 0;
            // System.out.println(UtilSys.getCurrentTime()+":"+"---insert recored "
            // + rowKey + " to table " + itablename +" ok.");
            // ///////////////////////////////////////////////////////////////////////////////////////
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return outb;
        }
    }

    @SuppressWarnings("finally")
    public Put hashQualifierToPut(String rowKey, String family,
                                  Map<String, String> iRecord) {
        Put out = new Put(Bytes.toBytes(rowKey));
        if ((iRecord == null) || (iRecord.size() == 0)) {
            return out;
        }
        String qualifier, value;
        try {
            // insert operate
            for (Map.Entry<String, String> entry : iRecord.entrySet()) {
                qualifier = entry.getKey();
                value = entry.getValue();
                if (value == null) continue;
                // String turn to bytes;
                out.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier),
                        Bytes.toBytes(value));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return out;
        }
    }

    @SuppressWarnings("finally")
    public Put hashOneQualifierToPut(String rowKey, String family,
                                     String qname, String keyvalue) {
        Put out = new Put(Bytes.toBytes(rowKey));
        String qualifier, value;
        qualifier = "";
        try {
            // insert operate
            // String turn to bytes;
            out.addColumn(Bytes.toBytes(family), Bytes.toBytes(qname),
                    Bytes.toBytes(keyvalue));

            // System.out.println(UtilSys.getCurrentTime()+":"+"---insert recored "
            // + rowKey + " to table " + itablename +" ok.");
            // ///////////////////////////////////////////////////////////////////////////////////////
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return out;
        }
    }

    /*
     * function:insert one record to the tables in:tablename filed:fieldname and
     * values out:0---insert succend;1---insert error;2--already has the
     * record;3---system error
     */
    public int insertOneFamilyRecord(String itablename, String rowKey,
                                     Map<String, String> iRecord) {
        int out = 1;
        Table table = getTable(itablename);
        String family, value;
        try {
            // insert operate
            Put put = new Put(Bytes.toBytes(rowKey));
            for (Map.Entry<String, String> entry : iRecord.entrySet()) {
                family = entry.getKey();
                value = entry.getValue();
                // String turn to bytes;
                put.addColumn(Bytes.toBytes(family), Bytes.toBytes(""),
                        Bytes.toBytes(value));
                // System.out.println(UtilSys.getCurrentTime()+":"+family+"---"+qualifier+"--"+value);
            }
            table.put(put);
            out = 0;
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return out;
        }
    }

    /*
     * function:selectByFamilyFilter in :String i_family,String i_qual,String
     * i_value i_value only need == out:hashmap row +family +i_value
     */
    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> selectFamilyFilter(
            String tablename, String i_family, String i_qual, String i_value) {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);
        FilterList filterList = new FilterList();
        Scan s1 = new Scan();

        filterList.addFilter(new SingleColumnValueFilter(Bytes
                .toBytes(i_family), Bytes.toBytes(i_qual),
                CompareFilter.CompareOp.EQUAL, Bytes.toBytes(i_value)));
        // 添加下面这一行后，则只返回指定的cell，同一行中的其他cell不返回
        // s1.addColumn(Bytes.toBytes(s[0]), Bytes.toBytes(s[1]));
        s1.setFilter(filterList);
        ResultScanner ResultScannerFilterList = null;

        try {
            ResultScannerFilterList = table.getScanner(s1);
            String rowid = "";
            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ResultScannerFilterList.close();
            return iresult;
        }
    }

    /*
     * create one table
     */
    public void closeHBaseAdmin(Admin iHBaseAdmin) {
        try {
            iHBaseAdmin.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int createTable(String tableName, String columnName) {
        // System.out.println("start create table ......"+tableName);
        Admin hBaseAdmin = null;
        try {
            org.apache.hadoop.hbase.client.Connection hConnection = ConnectionFactory.createConnection(configuration);
            hBaseAdmin = hConnection.getAdmin();
            if (hBaseAdmin.tableExists(TableName.valueOf(tableName))) {
                hBaseAdmin.disableTable(TableName.valueOf(tableName));
                hBaseAdmin.deleteTable(TableName.valueOf(tableName));
                // System.out.println(tableName + " is exist,detele....");
            }
            HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
            tableDescriptor.addFamily(new HColumnDescriptor(columnName));
            hBaseAdmin.createTable(tableDescriptor);
            // System.out.println(UtilSys.getCurrentTime()+"end create table ......"+tableName);
            return 0;
        } catch (MasterNotRunningException e) {
            e.printStackTrace();
            return -1;
        } catch (ZooKeeperConnectionException e) {
            e.printStackTrace();
            return -2;
        } catch (IOException e) {
            e.printStackTrace();
            return -3;
        } finally {
            closeHBaseAdmin(hBaseAdmin);
        }
    }

    /*
     * create one table
     */
    public int isExistsTable(String tableName) {
        // System.out.println("start create table ......"+tableName);
        Admin hBaseAdmin = null;
        try {
            org.apache.hadoop.hbase.client.Connection hConnection = ConnectionFactory.createConnection(configuration);
            hBaseAdmin = hConnection.getAdmin();
            if (hBaseAdmin.tableExists(TableName.valueOf(tableName))) {
                // System.out.println(tableName + " is exist,detele....");
                return 1;
            }
            return 0;
        } catch (MasterNotRunningException e) {
            e.printStackTrace();
            return -1;
        } catch (ZooKeeperConnectionException e) {
            e.printStackTrace();
            return -2;
        } catch (IOException e) {
            e.printStackTrace();
            return -3;
        } finally {
            closeHBaseAdmin(hBaseAdmin);
        }
    }

    /*
     * function:selectEqualManyFilter in :String i_family,String i_qual,String
     * i_value i_value only need == out:hashmap row +family +i_value
     */
    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> selectEqualManyFilter(
            String tablename, Map<String, String> i_fimill_yvalue, int icacheint) {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);
        FilterList filterList = new FilterList();
        Scan s1 = new Scan();
        // cache record number
        s1.setCaching(icacheint);
        s1.setCacheBlocks(false);
        String i_family, i_value;
        for (Map.Entry<String, String> entry : i_fimill_yvalue.entrySet()) {
            i_family = entry.getKey();
            i_value = entry.getValue();
            if (StringUtils.isNotBlank(i_value)) {
                filterList.addFilter(new SingleColumnValueFilter(Bytes
                        .toBytes(i_family), Bytes.toBytes(""),
                        CompareFilter.CompareOp.EQUAL, Bytes.toBytes(i_value)));
            }
            // System.out.println(UtilSys.getCurrentTime()+":selectFamilyManyFilter----"+i_family+"-----"+i_value);
        }
        // 添加下面这一行后，则只返回指定的cell，同一行中的其他cell不返回
        // s1.addColumn(Bytes.toBytes(s[0]), Bytes.toBytes(s[1]));
        if (filterList.hasFilterRow()) {
            s1.setFilter(filterList);
        }
        ResultScanner ResultScannerFilterList = null;
        try {
            ResultScannerFilterList = table.getScanner(s1);
            String rowid = "";
            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ResultScannerFilterList.close();
            return iresult;
        }
    }

    /*
     * function:selectLikeManyFilter in :String i_family,String i_qual,String
     * i_value i_value only need == out:hashmap row +family +i_value
     */
    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> selectLikeManyFilter(
            String tablename, Map<String, String> i_fimill_yvalue, int icacheint) {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);
        FilterList filterList = new FilterList();
        Scan s1 = new Scan();
        String i_family, i_value;
        for (Map.Entry<String, String> entry : i_fimill_yvalue.entrySet()) {
            i_family = entry.getKey();
            i_value = entry.getValue();
            if (StringUtils.isNotBlank(i_value)) {
                SubstringComparator comp = new SubstringComparator(i_value);
                filterList.addFilter(new SingleColumnValueFilter(Bytes
                        .toBytes(i_family), Bytes.toBytes(""),
                        CompareFilter.CompareOp.EQUAL, comp));
            }
            // System.out.println(UtilSys.getCurrentTime()+":selectFamilyManyFilter----"+i_family+"-----"+i_value);
        }
        // 添加下面这一行后，则只返回指定的cell，同一行中的其他cell不返回
        // s1.addColumn(Bytes.toBytes(s[0]), Bytes.toBytes(s[1]));
        if (filterList.hasFilterRow()) {
            s1.setFilter(filterList);
            // cache record number
            s1.setCaching(icacheint);
            s1.setCacheBlocks(false);
        }
        ResultScanner ResultScannerFilterList = null;

        try {
            ResultScannerFilterList = table.getScanner(s1);
            String rowid = "";
            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ResultScannerFilterList.close();
            return iresult;
        }
    }

    /*
     * function: selectLikeRowkeyFilter in :String tablename,String
     * irowkeystring,String i_value i_value only need == out:hashmap row +family
     * +i_value
     */
    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> selectLikeRowkeyFilter(
            String tablename, String irowkeystring) {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);

        Filter filter3 = new RowFilter(CompareFilter.CompareOp.EQUAL,
                new RegexStringComparator(irowkeystring));
        Scan scan = new Scan();
        scan.setFilter(filter3);
        // try {
        // scanner3 = table.getScanner(scan);
        // for (Result res : scanner3) {
        // res.value();
        // System.out.println(res);
        // }
        // scanner3.close();
        ResultScanner ResultScannerFilterList = null;
        try {
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";
            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));
                    System.out.println("get rowid:" + rowid + " value:" + new String(Bytes.toString(CellUtil.cloneValue(cell))));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
            return iresult;
        }

    }

    public Map<String, Object> selectLikeRowkeyFilter(
            String tablename, FilterList f1, int interval, String sDate) {
        Map<String, Object> iresult = new TreeMap<String, Object>();
        Map<String, Object> seriesMap = new TreeMap<String, Object>();
        Table table = getTable(tablename);

        Scan scan = new Scan();
        scan.setFilter(f1);
        scan.setLimit(interval);
        ResultScanner ResultScannerFilterList = null;
        try {
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";
            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                rowid = "";
//                System.out.println("while:");

                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();
                //only get caller_number and duration

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
//                    System.out.println("cellScanner.advance:");
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));
                    System.out.println("get rowid:" + rowid + " value:" + new String(Bytes.toString(CellUtil.cloneValue(cell))));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                seriesMap.put(rowid, irowvalue);
//                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            System.out.println(e.toString());
            e.printStackTrace();
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
            //xAxis
            Map<String, Object> xAxisMap = new TreeMap<String, Object>();
            String[] dateStrList = new String[1];
            dateStrList[0] = sDate;
            xAxisMap.put("categories", dateStrList);
            iresult.put("xAxis", xAxisMap);
            //series
            iresult.put("series", seriesMap);

            return iresult;
        }

    }


    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> getAllQualifierRecord(
            String tableName, String[] families) throws IOException {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();

        Table table = getTable(tableName);
        Scan s = new Scan();
        for (String family : families) {
            s.addFamily(Bytes.toBytes(family));
        }
        // It is highly recommended to specify a startKey and endKey.
        // s.setStartRow(Bytes.toBytes("01001686138100001"));
        // s.setStopRow(Bytes.toBytes("01001686138100002"));
        // s.setBatch(1000);
        // s.setCaching(1000);
        ResultScanner scanner = null;
        String rowid = "";
        try {
            scanner = table.getScanner(s);
            Iterator<Result> iterator = scanner.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (scanner != null)
                scanner.close();
            // System.out.println(new Date().toString() +"进入hbase");
            return iresult;
        }
    }

    /**
     * 根据 rowkey删除一条记录
     *
     * @param tablename
     * @param rowkey
     */
    public void deleteRow(String tablename, List<String> rowkey)
            throws IOException {
        Table table = getTable(tablename);
        try {
            List list = new ArrayList();
            for (int i = 0; i < rowkey.size(); i++) {
                Delete d1 = new Delete(rowkey.get(i).getBytes());
                list.add(d1);
            }
            table.delete(list);
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }

    /**
     * 根据 rowkey删除一条记录
     *
     * @param tablename
     * @param famillyname,
     * @param rowkeyvalue
     */
    public void deleteRowQColumn(String tablename, String famillyname,
                                 List<String> rowkeyvalue) throws IOException {
        Table table = getTable(tablename);
        try {
            List list = new ArrayList();
            for (int i = 0; i < rowkeyvalue.size(); i++) {
                String[] irk = rowkeyvalue.get(i).split(";");
                Delete d1 = new Delete(irk[0].getBytes());
                d1.addColumn(famillyname.getBytes(), irk[1].getBytes());
                list.add(d1);
            }
            table.delete(list);
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }

    /**
     * 多个条件过滤
     *
     * @param tablename    ---表名
     * @param ibeginrowkey --列号-注意位数控制，如果有车厢号，需要输入完全，加上车厢号
     * @param istart_time  ----开始日期精确到毫秒
     * @param iend_time    ----结束日期精确到毫秒YYYYMMDDHHMMSSsss
     * @return
     * @throws IOException
     */
    @SuppressWarnings("finally")
    public Map<String, Map<String, String>> selectLikeQRowkeyFilter(
            String tablename, String ibeginrowkey, String istart_time,
            String iend_time, String filterstr) throws IOException {
        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);

        // 过滤条件处理
        byte[] startRow = (ibeginrowkey + "_" + istart_time).getBytes();
        byte[] stopRow = (ibeginrowkey + "_" + iend_time).getBytes();
        Scan scan = new Scan(startRow, stopRow);

        ResultScanner mResultScanner = null;
        try {
            mResultScanner = table.getScanner(scan);
            String rowid = "";
            Iterator<Result> iterator = mResultScanner.iterator();
            while (iterator.hasNext()) {
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
                Result result = iterator.next();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

//                    302                     System.out.println("行键:" + Bytes.toString(CellUtil.copyRow(cell)));
//                    303                     System.out.println("列族:" + Bytes.toString(CellUtil.cloneFamily(cell)));
//                    304                     System.out.println("列名:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
//                    305                     System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));

            table.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (mResultScanner != null)
                mResultScanner.close();
            return (iresult);
        }
    }

    public String getMapValue2(Map<String, String> irowvalue, String sKey) {
        if (irowvalue == null || irowvalue.get(sKey) == null) {
            return "null";
        } else {
            return "'" + irowvalue.get(sKey) + "'";
        }

    }

    public String getDateForLinux(String sIp, Map<String, String> map) {
        String sDate = getHbaseDateStr(getMapValue(map, "created").replaceAll(":", "")
                .replaceAll("-", "")
                .replace("T", "")
                .replace("Z", "")
                .substring(0, 14));
//        System.out.println("sDate:"+getMapValue(map, "created").replaceAll(":", "")
//                .replaceAll("-", "")
//                .replace("T", "")
//                .replace("Z", "")
//                .substring(0, 14));
        String sKey = sIp + "_" + sDate + "_" + getMapValue(map, "id");
        return sKey;
    }

    @Test
    public void TestLinuxDate() {
        String sDate= "1615815615";
        System.out.println(getDateForLinux(sDate));
    }

    public String getDateForLinux(String sDate) {
        sDate += "000";
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String sd = sdf.format(new Date(Long.parseLong(sDate))); // 时间戳转换日期
        String sdt = sdf.format(d);
//        System.out.println("当前系统时间:" + sdt);
//        System.out.println("系统时间戳:" + d.getTime());
//        System.out.println("时间戳转换日期:" + sd);
        return sd;
    }

    @Test
    public void TestDtChange() {
        String sDate = "1615815615"+"000";
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String sd = sdf.format(new Date(Long.parseLong(sDate))); // 时间戳转换日期
        String sdt = sdf.format(d);
        System.out.println("当前系统时间:" + sdt);
        System.out.println("系统时间戳:" + d.getTime());
        System.out.println("时间戳转换日期:" + sd);
    }
    @Test
    public void TestCallLogKey() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", "4076104");
        map.put("setuptime", "0");
        map.put("created", "2021-03-15T13:23:18.000Z");
//        System.out.println(getCallLogKey("208.73.232.195", map));
    }

    public String getMapValue(Map<String, String> irowvalue, String sKey) {
        if (irowvalue == null || irowvalue.get(sKey) == null) {
            return "null";
        } else {
            return irowvalue.get(sKey);
        }

    }

    public String getMapValue(Map<String, String> irowvalue, String sKey, String sDefault) {
        if (irowvalue == null || irowvalue.get(sKey) == null) {
            return sDefault;
        } else {
            String sValue = irowvalue.get(sKey).replace("\r", "\\r");
            sValue = sValue.replace("\n", "\\n");
            sValue = sValue.replace("\t", "\\t");
            return sValue;
        }

    }

    public String getCall_Date(JSONObject json) {
        String sCreated = json.getString("created")
                .replace("-", "")
                .replace("T", "")
                .replace(":", "")
                .replace(" ", "")
                .substring(0, 10)
                ;
        return sCreated;
    }

    public String getFirstname(String sName) {
        String[] sList = sName.split("&");
        return sList[0];
    }

    public String getLastname(String sName) {
        String[] sList = sName.split("&");
        if (sList.length > 1) {
            return sList[1];
        } else {
            return "";
        }
    }

    public String getMapValue(Map<String, String> irowvalue, String sKey, boolean bAddColon) {
        if (irowvalue == null || irowvalue.get(sKey) == null) {
            return "null";
        } else {
            String sValue = irowvalue.get(sKey).replace("\r", "\\r");
            sValue = sValue.replace("\n", "\\n");
            sValue = sValue.replace("\t", "\\t");
            if (bAddColon) {
                return "\"" + sValue + "\"";
            } else {
                return sValue;
            }
        }

    }
    public String getDuration66(Map<String, String> irowvalue, String sKey) {
        //  6/6
        if (irowvalue == null || irowvalue.get(sKey) == null) {
            return "null";
        } else {
            String sValue = irowvalue.get(sKey).replace("\r", "\\r");
            sValue = sValue.replace("\n", "\\n");
            sValue = sValue.replace("\t", "\\t");
            double dDuration = Math.ceil(Integer.parseInt(sValue)/6.0)*6;
                return String.valueOf(dDuration);
        }
    }

    @Test
    public void test_duration() {
        double dDuration = Math.ceil(Integer.parseInt("17")/6.0)*6;
        System.out.println(dDuration);
    }
    public String getJsonAllValue(JSONObject jsonObj) {
        if (jsonObj == null) {
            return null;
        } else {
            String sValue = "";
            Iterator iter = jsonObj.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                sValue += entry.getKey().toString()+":"+entry.getValue().toString() + "||";
            }
//            System.out.println(sValue);
            return sValue;
        }
    }

    public String getJsonValueForSqlValue(JSONObject jsonObj, String sKey) {
        if (jsonObj == null || jsonObj.get(sKey) == null) {
            return null;
        } else {
            String sValue = "'"+jsonObj.getString(sKey)+"'";
            return sValue;
        }
    }

    public String getJsonValue(JSONObject jsonObj, String sKey) {
        if (jsonObj == null || jsonObj.get(sKey) == null) {
            return null;
        } else {
            String sValue = jsonObj.getString(sKey);
            return sValue;
        }
    }

    public String getJsonNumber(JSONObject jsonObj, String sKey) {
        if (jsonObj == null || jsonObj.get(sKey) == null) {
            return null;
        } else {
            String sValue = jsonObj.getString(sKey).replace(",","");
            return sValue;
        }
    }

    public String getJsonValue(JSONObject jsonObj, String sKey,String sDefault) {
        if (jsonObj == null || jsonObj.get(sKey) == null) {
            return sDefault;
        } else {
            String sValue = jsonObj.getString(sKey);
            return sValue;
        }
    }

    public String getCDRsTest(
            String sFilter, int iStartDate, int iEndDate, String sIp, int limit, int skip) {
        Date dtStart = new Date();
        String tablename = "cdr";
        String sRowKeyStart = "208.73.232.196_2021021805";
        String sRowKeyEnd = "208.73.232.196_2021021904";
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);

        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);

        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));
//        scan.setReversed(true);

        ResultScanner ResultScannerFilterList = null;
        try {
            int iNo = 0;
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";

            //way1
//            Iterator<Result> iterator = ResultScannerFilterList.iterator();
//            while (iterator.hasNext()) {
//                iNo++;
//                Result result = iterator.next();
//            }

            //way2
            for (Result rs = ResultScannerFilterList.next(); rs != null; rs = ResultScannerFilterList.next()) {
                iNo++;
            }

            table.close();
            System.out.println("hbase query end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + String.valueOf(iNo));
            return "";

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
        }

    }

    public int getCount(String tablename, String sRowKeyStart, String sRowKeyEnd, FilterList allFilters) {
        int iNo = 0;
        Date dtStart = new Date();
        Table table = getTable(tablename);
        Scan scan = new Scan();
        if (allFilters != null) {
//            allFilters.addFilter(new FirstKeyOnlyFilter());
            scan.setFilter(allFilters);
        } else {
            scan.setFilter(new FirstKeyOnlyFilter());
        }
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));
        scan.setCaching(100000);
        scan.setCacheBlocks(false);

        ResultScanner ResultScannerFilterList = null;
        try {
            ResultScannerFilterList = table.getScanner(scan);
            for (Result rs = ResultScannerFilterList.next(); rs != null; rs = ResultScannerFilterList.next()) {
                iNo++;
            }
            table.close();
            System.out.println("hbase query end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + String.valueOf(iNo));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
        }
        return iNo;
    }

    @Test
    public void testGetFilterList() {
        String sFilter = "7864994309";
        String sFields = "callerNumber";
        String sRowKeyStart = "208.73.232.196_76789870769577";
        String sRowKeyEnd = "208.73.232.196_80789870769577";
        String limit = "5";
        String skip = "0";

        String sSql  = "select * from \"call_log\" ";
        String sIp = "208.73.232.209";
        String sWhere = getFilterList(sIp,sFilter, sFields,"",sRowKeyStart,sRowKeyEnd);
        sSql = sSql + sWhere + " limit " + limit + " offset " + skip;
        System.out.println("sSql:" + sSql);
    }
    public String getCDRs(
            String sFilter, String sFields, String sStartDate, String sEndDate, String sIp
            ,String customerId, int limit, int skip) {
        Date dtStart = new Date();
        sStartDate = MysqlUtil.addSecond(sStartDate, -1);
        String sRowKeyStart = sIp + "_" + getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp + "_" + getHbaseDateStr(sStartDate);
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);

        try {
            Class.forName(classname_phoenix);
            java.sql.Connection conn = DriverManager.getConnection(url_phoenix);
            Statement  statement = conn.createStatement();
            String sSql  = "select * from \"call_log\" ";
            String sWhere = getFilterList(sIp,sFilter, sFields,customerId, sRowKeyStart,sRowKeyEnd);
            sSql = sSql + sWhere + " limit " + limit + " offset " + skip;
            long      time = System.currentTimeMillis();
            //test
            System.out.println("sSql:" + sSql);
            ResultSet rs   = statement.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
            int iNo = 0;
            while (rs.next()) {
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
//                System.out.println("newid" + rs.getString("newid"));
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    irowvalue.put(columnName,rs.getString(i));
//                    System.out.println(columnName+":" + rs.getString(i));
                }
                iresult.put(rs.getString("newid"), irowvalue);
                iNo++;
            }
            // 关闭连接
            rs.close();
            statement.close();
            conn.close();

            System.out.println("phoenix query end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("{\n");
            sb.append("\"total_count\":" + String.valueOf(iNo - skip) + ",\n");
            sb.append("\"data\":[\n");
            int ii = 0;
            ListIterator<Map.Entry<String, Map<String, String>>> li = new ArrayList<Map.Entry<String, Map<String, String>>>(iresult.entrySet()).listIterator();
            while (li.hasNext()) {
                Map.Entry<String, Map<String, String>> entry = li.next();
//                for (String rowkey : iresult.keySet()) {
//                Map<String, String> irowvalue = iresult.get(rowkey);
                Map<String, String> irowvalue = entry.getValue();
                ii++;
                sb.append("{\n");
                sb.append("\"id\": " + getMapValue(irowvalue, "id") + ",\n");
                sb.append("\"callId\": " + getMapValue(irowvalue, "call_id", true) + ",\n");
                sb.append("\"callDirection\": " + getMapValue(irowvalue, "call_direction", true) + ",\n");
                sb.append("\"callerNumber\": " + getMapValue(irowvalue, "caller_number", true) + ",\n");
                sb.append("\"sessionData\": " + getMapValue(irowvalue, "session_data", true) + ",\n");
                sb.append("\"metrics\": " + getMapValue(irowvalue, "metrics", true) + ",\n");
//                sb.append("\"routingid\": " + getMapValue(irowvalue, "info:routing_id", true) + ",\n");
                sb.append("\"flag\": " + getMapValue(irowvalue, "flag") + ",\n");
                sb.append("\"callTerminated\": " + getMapValue(irowvalue, "call_terminated") + ",\n");
                sb.append("\"callStatus\": " + getMapValue(irowvalue, "call_status", true) + ",\n");
                sb.append("\"callStatusMessage\": " + getMapValue(irowvalue, "call_status_message", true) + ",\n");
                sb.append("\"duration\": " + getDuration66(irowvalue, "duration") + ",\n");
                sb.append("\"msDuration\": " + getMapValue(irowvalue, "ms_duration") + ",\n");
                sb.append("\"setuptime\": " + getMapValue(irowvalue, "setuptime") + ",\n");
                sb.append("\"created\": " + getMapValue(irowvalue, "created", true) + ",\n");
                sb.append("\"callrecordingId\": " + getMapValue(irowvalue, "callRecording_id", true) + ",\n");
                sb.append("\"opnumberId\": " + getMapValue(irowvalue, "opNumber_id") + ",\n");
                sb.append("\"trackingNumber\": " + getMapValue(irowvalue, "trackingNumber"));
                //phonebook
                if (irowvalue.get("phonebook_id") != null) {
                    sb.append(",\n");
                    sb.append("\"Phonebook\": {\n");
                    sb.append("\"id\": " + getMapValue(irowvalue, "phonebook_id") + ",\n");
                    sb.append("\"name\": " + getMapValue(irowvalue, "phonebook_name", true) + ",\n");
                    sb.append("\"email\": " + getMapValue(irowvalue, "phonebook_email", true) + ",\n");
                    sb.append("\"street\": " + getMapValue(irowvalue, "phonebook_street", true) + ",\n");
                    sb.append("\"city\": " + getMapValue(irowvalue, "phonebook_city", true) + ",\n");
                    sb.append("\"state\": " + getMapValue(irowvalue, "phonebook_state", true) + ",\n");
                    sb.append("\"country\": " + getMapValue(irowvalue, "phonebook_country", true) + ",\n");
                    sb.append("\"postalCode\": " + getMapValue(irowvalue, "phonebook_postalCode", true) + ",\n");
                    sb.append("\"contact_number\": " + getMapValue(irowvalue, "phonebook_contactNumber", true) + ",\n");
                    sb.append("\"note\": " + getMapValue(irowvalue, "phonebook_note", true) + "\n");
                    sb.append("}");
                }
                //OpNumber
                if (irowvalue.get("opNumber_id") != null) {
                    sb.append(",\n");
                    sb.append("\"OpNumber\": {\n");
                    sb.append("\"id\": " + getMapValue(irowvalue, "opNumber_id") + ",\n");
                    sb.append("\"active\": " + getMapValue(irowvalue, "opNumber_active") + ",\n");
                    sb.append("\"tracking_number\": " + getMapValue(irowvalue, "opNumber_trackingNumber", true) + ",\n");
                    sb.append("\"notifications\": " + getMapValue(irowvalue, "opNumber_notifications") + ",\n");
                    sb.append("\"text_support\": " + getMapValue(irowvalue, "opNumber_textSupport") + ",\n");
                    sb.append("\"number_tags\": " + getMapValue(irowvalue, "opNumber_numberTags", true) + ",\n");
                    sb.append("\"failsafe_number\": " + getMapValue(irowvalue, "opNumber_failsafeNumber", true) + ",\n");
                    sb.append("\"renewal_date\": " + getMapValue(irowvalue, "opNumber_renewalDate", true) + ",\n");
                    sb.append("\"description\": " + getMapValue(irowvalue, "opNumber_description", true) + ",\n");
                    sb.append("\"customerId\": " + getMapValue(irowvalue, "customerId") + ",\n");
                    sb.append("\"receiving_numberId\": " + getMapValue(irowvalue, "receivingNumber_id") + ",\n");
                    sb.append("\"phonebook_id\": " + getMapValue(irowvalue, "phonebook_id") + ",\n");
                    sb.append("\"tracking_sourceId\": " + getMapValue(irowvalue, "trackingSource_id"));
                    //Customer
                    if (irowvalue.get("customerId") != null) {
                        sb.append(",\n");
                        sb.append("\"Customer\": {\n");
//                        sb.append("\"id\": " + getMapValue(irowvalue, "customerId") + ",\n");
                        sb.append("\"id\": " + getMapValue(irowvalue, "customerId") + "\n");
//                        sb.append("\"enabled\": " + getMapValue(irowvalue, "op_numberCustomerEnabled", true) + ",\n");
//                        sb.append("\"name\": " + getMapValue(irowvalue, "op_numberCustomerName", true) + ",\n");
//                        sb.append("\"vatnumber\": " + getMapValue(irowvalue, "op_numberCustomerVatnumber", true) + ",\n");
//                        sb.append("\"businessname\": " + getMapValue(irowvalue, "op_numberCustomerBusinessname", true) + ",\n");
//                        sb.append("\"email\": " + getMapValue(irowvalue, "op_numberCustomerEmail", true) + ",\n");
//                        sb.append("\"emailaccount\": " + getMapValue(irowvalue, "op_numberCustomerEmailaccount", true) + "\n");
                        sb.append("}");
                    }
                    //TrackingSources
                    if (irowvalue.get("trackingSource_id") != null) {
                        sb.append(",\n");
                        sb.append("\"TrackingSources\": {\n");
                        sb.append("\"id\": " + getMapValue(irowvalue, "trackingSource_id") + ",\n");
                        sb.append("\"name\": " + getMapValue(irowvalue, "trackingSource_name", true) + ",\n");
                        sb.append("\"type\": " + getMapValue(irowvalue, "trackingSource_type", true) + ",\n");
                        sb.append("\"position\": " + getMapValue(irowvalue, "trackingSource_position") + ",\n");
                        sb.append("\"lastTouch\": " + getMapValue(irowvalue, "trackingSource_lastTouch") + ",\n");
                        sb.append("\"updatedAt\": " + getMapValue(irowvalue, "trackingSource_updatedAt", true) + ",\n");
                        sb.append("\"description\": " + getMapValue(irowvalue, "trackingSource_description", true) + "\n");
                        sb.append("}");
                    }

                    sb.append(",\n");
                    //receivingNumber
                    sb.append("\"receivingNumber_id\": " + getMapValue(irowvalue, "receivingNumber_id", true) + ",\n");
                    sb.append("\"receivingNumber_number\": " + getMapValue(irowvalue, "receivingNumber_number", true) + ",\n");
                    sb.append("\"receivingNumber_description\": " + getMapValue(irowvalue, "receivingNumber_description", true) + ",\n");
                    //sipGateway
                    sb.append("\"routingAction\": " + getMapValue(irowvalue, "opNumber_routingAction", true) + ",\n");
                    sb.append("\"sipGateway_id\": " + getMapValue(irowvalue, "sipGateway_id", true) + ",\n");
                    sb.append("\"sipGateway_name\": " + getMapValue(irowvalue, "sipGateway_name", true) + ",\n");
                    sb.append("\"sipGateway_address\": " + getMapValue(irowvalue, "sipGateway_address", true) + ",\n");
                    sb.append("\"sipGateway_port\": " + getMapValue(irowvalue, "sipGateway_port") + ",\n");
                    sb.append("\"sipGateway_digitsStrip\": " + getMapValue(irowvalue, "sipGateway_digitsStrip") + ",\n");
                    sb.append("\"sipGateway_description\": " + getMapValue(irowvalue, "sipGateway_description", true) + "\n");

                    //end
                    sb.append("}\n");
                }
                if (ii >= iresult.size()) {
                    sb.append("}\n");
                } else {
                    sb.append("},\n");
                }
            }
            sb.append("]\n");
            sb.append("}\n");


            System.out.println("export result end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }
    public String getCDRs_bak(
            String sFilter, String sFields, String sStartDate, String sEndDate, String sIp, int limit, int skip) {
        Date dtStart = new Date();
        sStartDate = MysqlUtil.addSecond(sStartDate, -1);
        String tablename = "cdr";
        String sRowKeyStart = sIp + "_" + getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp + "_" + getHbaseDateStr(sStartDate);
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);

        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);
        Scan scan = new Scan();
        FilterList allFilters = getFilterList(sFilter, sFields);
        if (allFilters != null) {
            scan.setFilter(allFilters);
        } else {
            System.out.println("no filter:");
        }
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));

        ResultScanner ResultScannerFilterList = null;
        try {
            int iNo = 0;
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";
            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                iNo++;
                Result result = iterator.next();
//                if (iNo<100000) {
//                    for (Cell cell : result.listCells()) {
//                        String sLog = Bytes.toString(cell.getRowArray(), cell.getRowOffset(), cell.getRowLength());
//                        System.out.println(String.valueOf(iNo) + " get rowids:" + sLog);
//                        break;
//                    }
//                }
                if (iNo <= skip) {
                    continue;
                }
                if (iNo <= (limit + skip)) {
                    rowid = "";
                    Map<String, String> irowvalue = new TreeMap<String, String>();
                    irowvalue.clear();

                    CellScanner cellScanner = result.cellScanner();
                    while (cellScanner.advance()) {
                        Cell cell = cellScanner.current();

                        if (rowid == "")
                            rowid = Bytes.toString(CellUtil.copyRow(cell));
//                    System.out.println("get rowid:"+ rowid +
//                            " key:"+new String(Bytes.toString(CellUtil.cloneFamily(cell))) +
//                            " value:"+new String(Bytes.toString(CellUtil.cloneValue(cell)))
//                    );

                        irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))) + ":" +
                                        new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                                new String(Bytes.toString(CellUtil.cloneValue(cell))));
                    }
                    iresult.put(rowid, irowvalue);
                } else {
                    break;
                }
            }

            table.close();
            System.out.println("hbase query end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("{\n");
            sb.append("\"total_count\":" + String.valueOf(iNo - skip) + ",\n");
            sb.append("\"data\":[\n");
            int ii = 0;
            ListIterator<Map.Entry<String, Map<String, String>>> li = new ArrayList<Map.Entry<String, Map<String, String>>>(iresult.entrySet()).listIterator();
            while (li.hasNext()) {
                Map.Entry<String, Map<String, String>> entry = li.next();
//                for (String rowkey : iresult.keySet()) {
//                Map<String, String> irowvalue = iresult.get(rowkey);
                Map<String, String> irowvalue = entry.getValue();
                ii++;
                sb.append("{\n");
                sb.append("\"id\": " + getMapValue(irowvalue, "info:id") + ",\n");
                sb.append("\"callId\": " + getMapValue(irowvalue, "info:call_id", true) + ",\n");
                sb.append("\"callDirection\": " + getMapValue(irowvalue, "info:call_direction", true) + ",\n");
                sb.append("\"callerNumber\": " + getMapValue(irowvalue, "info:caller_number", true) + ",\n");
                sb.append("\"sessionData\": " + getMapValue(irowvalue, "info:session_data", true) + ",\n");
                sb.append("\"metrics\": " + getMapValue(irowvalue, "info:metrics", true) + ",\n");
                sb.append("\"routingid\": " + getMapValue(irowvalue, "info:routing_id", true) + ",\n");
                sb.append("\"flag\": " + getMapValue(irowvalue, "info:flag") + ",\n");
                sb.append("\"callTerminated\": " + getMapValue(irowvalue, "info:call_terminated") + ",\n");
                sb.append("\"callStatus\": " + getMapValue(irowvalue, "info:call_status", true) + ",\n");
                sb.append("\"callStatusMessage\": " + getMapValue(irowvalue, "info:call_status_message", true) + ",\n");
                sb.append("\"duration\": " + getMapValue(irowvalue, "info:duration") + ",\n");
                sb.append("\"msDuration\": " + getMapValue(irowvalue, "info:ms_duration") + ",\n");
                sb.append("\"setuptime\": " + getMapValue(irowvalue, "info:setuptime") + ",\n");
                sb.append("\"created\": " + getMapValue(irowvalue, "info:created", true) + ",\n");
                sb.append("\"callrecordingId\": " + getMapValue(irowvalue, "info:callrecordingId", true) + ",\n");
                sb.append("\"opnumberId\": " + getMapValue(irowvalue, "info:op_numberId") + ",\n");
                sb.append("\"caller_contactId\": " + getMapValue(irowvalue, "info:caller_contactId")+ ",\n");
                sb.append("\"trackingNumber\": " + getMapValue(irowvalue, "info:trackingNumber"));
                //phonebook
                if (irowvalue.get("info:phonebookId") != null) {
                    sb.append(",\n");
                    sb.append("\"Phonebook\": {\n");
                    sb.append("\"id\": " + getMapValue(irowvalue, "info:phonebookId") + ",\n");
                    sb.append("\"name\": " + getMapValue(irowvalue, "info:phonebookName", true) + ",\n");
                    sb.append("\"email\": " + getMapValue(irowvalue, "info:phonebookEmail", true) + ",\n");
                    sb.append("\"street\": " + getMapValue(irowvalue, "info:phonebookStreet", true) + ",\n");
                    sb.append("\"city\": " + getMapValue(irowvalue, "info:phonebookCity", true) + ",\n");
                    sb.append("\"state\": " + getMapValue(irowvalue, "info:phonebookState", true) + ",\n");
                    sb.append("\"country\": " + getMapValue(irowvalue, "info:phonebookCountry", true) + ",\n");
                    sb.append("\"postalCode\": " + getMapValue(irowvalue, "info:phonebookPostalCode", true) + ",\n");
                    sb.append("\"contact_number\": " + getMapValue(irowvalue, "info:phonebookContact_number", true) + ",\n");
                    sb.append("\"note\": " + getMapValue(irowvalue, "info:phonebookNote", true) + "\n");
                    sb.append("}");
                }
                //OpNumber
                if (irowvalue.get("info:op_numberId") != null) {
                    sb.append(",\n");
                    sb.append("\"OpNumber\": {\n");
                    sb.append("\"id\": " + getMapValue(irowvalue, "info:op_numberId") + ",\n");
                    sb.append("\"active\": " + getMapValue(irowvalue, "info:op_numberActive") + ",\n");
                    sb.append("\"tracking_number\": " + getMapValue(irowvalue, "info:op_numberTracking_number", true) + ",\n");
                    sb.append("\"notifications\": " + getMapValue(irowvalue, "info:op_numberNotifications") + ",\n");
                    sb.append("\"text_support\": " + getMapValue(irowvalue, "info:op_numberText_support") + ",\n");
                    sb.append("\"number_tags\": " + getMapValue(irowvalue, "info:op_numberNumber_tags", true) + ",\n");
                    sb.append("\"failsafe_number\": " + getMapValue(irowvalue, "info:op_numberFailsafe_number", true) + ",\n");
                    sb.append("\"renewal_date\": " + getMapValue(irowvalue, "info:op_numberRenewal_date", true) + ",\n");
                    sb.append("\"description\": " + getMapValue(irowvalue, "info:op_numberDescription", true) + ",\n");
                    sb.append("\"customerId\": " + getMapValue(irowvalue, "info:op_numberCustomerId") + ",\n");
                    sb.append("\"receiving_numberId\": " + getMapValue(irowvalue, "info:op_numberReceiving_numberId") + ",\n");
                    sb.append("\"phonebook_id\": " + getMapValue(irowvalue, "info:op_numberPhonebook_id") + ",\n");
                    sb.append("\"tracking_sourceId\": " + getMapValue(irowvalue, "info:op_numberTracking_sourceId"));
                    //Customer
                    if (irowvalue.get("info:op_numberCustomerId") != null) {
                        sb.append(",\n");
                        sb.append("\"Customer\": {\n");
                        sb.append("\"id\": " + getMapValue(irowvalue, "info:op_numberCustomerId") + ",\n");
                        sb.append("\"enabled\": " + getMapValue(irowvalue, "info:op_numberCustomerEnabled", true) + ",\n");
                        sb.append("\"name\": " + getMapValue(irowvalue, "info:op_numberCustomerName", true) + ",\n");
                        sb.append("\"vatnumber\": " + getMapValue(irowvalue, "info:op_numberCustomerVatnumber", true) + ",\n");
                        sb.append("\"businessname\": " + getMapValue(irowvalue, "info:op_numberCustomerBusinessname", true) + ",\n");
                        sb.append("\"email\": " + getMapValue(irowvalue, "info:op_numberCustomerEmail", true) + ",\n");
                        sb.append("\"emailaccount\": " + getMapValue(irowvalue, "info:op_numberCustomerEmailaccount", true) + "\n");
                        sb.append("}");
                    }
                    //TrackingSources
                    if (irowvalue.get("info:op_numberTracking_sourceId") != null) {
                        sb.append(",\n");
                        sb.append("\"TrackingSources\": {\n");
                        sb.append("\"id\": " + getMapValue(irowvalue, "info:op_numberTracking_sourceId") + ",\n");
                        sb.append("\"name\": " + getMapValue(irowvalue, "info:op_numberTracking_sourcesName", true) + ",\n");
                        sb.append("\"type\": " + getMapValue(irowvalue, "info:op_numberTracking_sourcesType", true) + ",\n");
                        sb.append("\"position\": " + getMapValue(irowvalue, "info:op_numberTracking_sourcesPosition") + ",\n");
                        sb.append("\"lastTouch\": " + getMapValue(irowvalue, "info:op_numberTracking_sourcesLast_touch") + ",\n");
                        sb.append("\"updatedAt\": " + getMapValue(irowvalue, "info:op_numberTracking_sourcesUpdated_at", true) + ",\n");
                        sb.append("\"description\": " + getMapValue(irowvalue, "info:op_numberTracking_sourcesDescription", true) + "\n");
                        sb.append("}");
                    }
                    sb.append("}\n");
                }
                if (ii >= iresult.size()) {
                    sb.append("}\n");
                } else {
                    sb.append("},\n");
                }
            }
            sb.append("]\n");
            sb.append("}\n");


            System.out.println("export result end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            return sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
        }

    }

    public String getCallLogKey(String sIp,JSONObject jsonObj) {
        //208.73.232.196_79789898948689_1949385
//        System.out.println("------------------------getCallLogKey created:"+map.get("created"));
//        System.out.println("------------------------getCallLogKey id:"+map.get("id"));
        String sCreated = jsonObj.getString("created")
                .replace("-", "")
                .replace("T", "")
                .replace(":", "")
                .replace(" ", "")
                .substring(0, 14)
                ;
        String rowKey = sIp + "_" + getHbaseDateStr(sCreated) +"_"+ jsonObj.getString("id");
        return rowKey;
    }
    public String getCallLogKey2(String sIp,Map<String, String> map) {
        //208.73.232.196_79789898948689_1949385
//        System.out.println("------------------------getCallLogKey created:"+map.get("created"));
//        System.out.println("------------------------getCallLogKey id:"+map.get("id"));
        String sCreated = map.get("created")
                .replace("-", "")
                .replace("T", "")
                .replace(":", "")
                .replace(" ", "")
                .substring(0, 14)
                ;
        String rowKey = sIp + "_" + getHbaseDateStr(sCreated) +"_"+ map.get("id");
        return rowKey;
    }
    public String addCDRs(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //update call log
        //write to hbase
        //add it to sum table
        String rowKey = "";
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            rowKey = getCallLogKey(sIp, jsonObj);
            //add to phoenix
            addCDRsToPhoenix(rowKey,sIp,jsonObj);
//            System.out.println("added call log to call log table:" + rowKey
//                    + " takes :" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("{\n");
            sb.append("\"id\":" + rowKey + "\n");
            sb.append("}\n");
            return sb.toString();
        } catch (Exception e) {
            System.out.println("get error:"+e.toString());
            return "error";
        }
    }
    @Test
    public void testOpnumberList() {
        String ss = "{" +
                "customerId: 1," +
                "opNumberIdList: [1,2,3]" +
                "}";
        JSONObject jsonObj = JSONObject.parseObject(ss);
        JSONArray jsonArray = jsonObj.getJSONArray("opNumberIdList");
        String sList = "('" + jsonArray.getString(0)+"'";
        for (int ii = 1;ii<jsonArray.size();ii++) {
            sList = sList+ ",'" + jsonArray.getString(ii) + "'";
        }
        sList += ")";
        System.out.println(sList);
    }
    public String update_opNumber_test(String sIp, JSONObject jsonObj) {
        System.out.println("update_opNumber_test:"+getJsonAllValue(jsonObj));
        System.out.println("  from :"+sIp);
        return "";
    }
    public String getMysqlUrl(String sIp) {
        String url = "";
        if (sIp.equals("208.73.232.196")) {
            url = url_196;
        } else if (sIp.equals("208.73.232.209")) {
            url = url_209;
        } else {
            System.out.println("  getMysqlUrl empty :" + sIp);
            System.out.println(sIp.equals("208.73.232.196"));
            System.out.println(sIp.equals("208.73.232.209"));
        }
        return url;
    }
    public JSONObject getMysqlOpNumber(String sIp,String sId) {
        JSONObject jsonObj = new JSONObject();;
        Date dtStart = new Date();
        String sSql = "SELECT  *  FROM op_number where id =  " + sId
                ;

        java.sql.Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps_mysql = null;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(getMysqlUrl(sIp));

            ps_mysql=conn.prepareStatement(sSql);
//            ps_mysql.setInt(1,Integer.parseInt(sId));
            rs = ps_mysql.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
                }
            }
            System.out.println("getMysqlOpNumber:ok");
            if (rs!=null){
                rs.close();
            }
            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
    public JSONObject getMysqlCustomer(String sIp,String sId) {
        JSONObject jsonObj = new JSONObject();;
        Date dtStart = new Date();
        String sSql = "SELECT  *  FROM customer where id =  " + sId
                ;

        java.sql.Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps_mysql = null;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(getMysqlUrl(sIp));

            ps_mysql=conn.prepareStatement(sSql);
//            ps_mysql.setInt(1,Integer.parseInt(sId));
            rs = ps_mysql.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
                }
            }
            System.out.println("getMysqlCustomer:ok");
            if (rs!=null){
                rs.close();
            }
            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
    public JSONObject getMysqlSipGateway(String sIp,String sId) {
        JSONObject jsonObj = new JSONObject();;
        Date dtStart = new Date();
        String sSql = "SELECT  *  FROM sip_gateways where id =  " + sId
                ;

        java.sql.Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps_mysql = null;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(getMysqlUrl(sIp));

            ps_mysql=conn.prepareStatement(sSql);
            rs = ps_mysql.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
                }
            }
            System.out.println("getMysqlSipGateway:ok");
            if (rs!=null){
                rs.close();
            }
            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
    public JSONObject getMysqlPhonebook(String sIp,String sId) {
        JSONObject jsonObj = new JSONObject();;
        Date dtStart = new Date();
        String sSql = "SELECT  *  FROM phonebook where id =  " + sId
                ;

        java.sql.Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps_mysql = null;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(getMysqlUrl(sIp));

            ps_mysql=conn.prepareStatement(sSql);
//            ps_mysql.setInt(1,Integer.parseInt(sId));
            rs = ps_mysql.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
                }
            }
            System.out.println("getMysqlPhonebook:ok");
            if (rs!=null){
                rs.close();
            }
            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
    public JSONObject getMysqlTrackingSources(String sIp) {
        JSONObject jsonListObj = new JSONObject();
        Date dtStart = new Date();
        String sSql = "SELECT  *  FROM tracking_sources "
                ;

        java.sql.Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps_mysql = null;
        int iNo = 0;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(getMysqlUrl(sIp));

            ps_mysql=conn.prepareStatement(sSql);
//            ps_mysql.setInt(1,Integer.parseInt(sId));
            rs = ps_mysql.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                JSONObject jsonObj = new JSONObject();
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
                }
                jsonListObj.put(String.valueOf(iNo),jsonObj);
                iNo++;
            }
            System.out.println("getMysqlTrackingSources:ok");
            if (rs!=null){
                rs.close();
            }
            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonListObj;
    }
    public JSONObject getMysqlTrackingSource(String sIp,String sId) {
        JSONObject jsonObj = new JSONObject();;
        Date dtStart = new Date();
        String sSql = "SELECT  *  FROM tracking_sources where id =  " + sId
                ;

        java.sql.Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps_mysql = null;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(getMysqlUrl(sIp));

            ps_mysql=conn.prepareStatement(sSql);
//            ps_mysql.setInt(1,Integer.parseInt(sId));
            rs = ps_mysql.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
                }
            }
            System.out.println("getMysqlTrackingSource:ok");
            if (rs!=null){
                rs.close();
            }
            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
    public JSONObject getMysqlReceivingNumber(String sIp,String sId) {
        JSONObject jsonObj = new JSONObject();;
        Date dtStart = new Date();
        String sSql = "SELECT  *  FROM receiving_number where id =  " + sId
                ;

        java.sql.Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps_mysql = null;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(getMysqlUrl(sIp));

            ps_mysql=conn.prepareStatement(sSql);
//            ps_mysql.setInt(1,Integer.parseInt(sId));
            rs = ps_mysql.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
                }
            }
            System.out.println("getMysqlReceivingNumber:ok");
            if (rs!=null){
                rs.close();
            }
            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
    public void updateHadoopOpNumber(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "upsert into \"op_number\" ( " +
                "\"id\"  ," +
                "   \"info\".\"hostname\" ," +
                "   \"info\".\"customerId\"    ," +
                "   \"info\".\"receiving_numberId\" ," +
                "   \"info\".\"tracking_number\" ," +
                "   \"info\".\"tracking_sourceId\" ," +
                "   \"info\".\"sip_gatewayId\" ," +
                "   \"info\".\"routing_action\" ," +
                "   \"info\".\"phonebook_id\" ," +
                "   \"info\".\"target_numbers\"  ," +
                "   \"info\".\"call_settings\" ," +
                "   \"info\".\"schedule_id\" ," +
                "   \"info\".\"notifications\" ," +
                "   \"info\".\"text_support\" ," +
                "   \"info\".\"number_tags\" ," +
                "   \"info\".\"failsafe_number\" ," +
                "   \"info\".\"renewal_date\" ," +
                "   \"info\".\"active\" ," +
                "   \"info\".\"type_id\" ," +
                "   \"info\".\"description\" " +
                "   ) values(" +
                "?,?,?,?,?,?,?,?,?,?," +
                "?,?,?,?,?,?,?,?,?,?" +
                ")";//20


        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            ps=phoenixConn.prepareStatement(sSql);
            int iField = 1;
            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
            ps.setString(iField,rowKey);
            iField++;ps.setString(iField,sIp);
            iField++;ps.setString(iField,getJsonValue(jsonObj,"customerId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"receiving_numberId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"tracking_number"));//5
            iField++;ps.setString(iField,getJsonValue(jsonObj,"tracking_sourceId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"sip_gatewayId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"routing_action"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"target_numbers"));//10
            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_settings"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"schedule_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"notifications"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"text_support"));//15
            iField++;ps.setString(iField,getJsonValue(jsonObj,"number_tags"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"failsafe_number"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"renewal_date"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"active"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"type_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"description"));

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("inserted into hadoop opNumber:"+rowKey);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateHadoopCustomer(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "upsert into \"customers\" ( " +
                "\"id\"  ," +
                "   \"info\".\"hostname\" ," +
                "   \"info\".\"enabled\" ," +
                "   \"info\".\"firstName\"    ," +
                "   \"info\".\"lastName\" ," +
                "   \"info\".\"vatNumber\" ," +
                "   \"info\".\"companyName\" ," +
                "   \"info\".\"companyId\" ," +
                "   \"info\".\"customerId\" ," +
                "   \"info\".\"billingEmail\" ," +
                "   \"info\".\"contactEmail\"  ," +
                "   \"info\".\"address\" ," +
                "   \"info\".\"city\" ," +
                "   \"info\".\"state\" ," +
                "   \"info\".\"zip\" ," +
                "   \"info\".\"phone\"  " +
                "   ) values(" +
                "?,?,?,?,?,?,?,?,?,?," +
                "?,?,?,?,?,?" +
                ")";//16


        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            ps=phoenixConn.prepareStatement(sSql);
            int iField = 1;
            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
            ps.setString(iField,rowKey);
            iField++;ps.setString(iField,sIp);
            iField++;ps.setString(iField,getJsonValue(jsonObj,"enabled"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"firstName"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"lastName"));//5
            iField++;ps.setString(iField,getJsonValue(jsonObj,"vatNumber"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"companyName"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"companyId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"customerId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"billingEmail"));//10
            iField++;ps.setString(iField,getJsonValue(jsonObj,"contactEmail"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"address"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"city"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"state"));//15
            iField++;ps.setString(iField,getJsonValue(jsonObj,"zip"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phone"));

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("inserted into hadoop customer:"+rowKey);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateHadoopTrackingSource(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "upsert into \"tracking_sources\" ( " +
                "\"id\"  ," +
                "   \"info\".\"hostname\" ," +
                "   \"info\".\"name\" ," +
                "   \"info\".\"type\" ," +
                "   \"info\".\"position\"    ," +
                "   \"info\".\"last_touch\" ," +
                "   \"info\".\"global_unique\" ," +
                "   \"info\".\"customerId\" ," +
                "   \"info\".\"updated_at\" ," +
                "   \"info\".\"description\" " + //10
                "   ) values(" +
                "?,?,?,?,?,?,?,?,?,? " +
                ")";//16
        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            ps=phoenixConn.prepareStatement(sSql);
            int iField = 1;
            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
            ps.setString(iField,rowKey);
            iField++;ps.setString(iField,sIp);
            iField++;ps.setString(iField,getJsonValue(jsonObj,"name"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"type"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"position"));//5
            iField++;ps.setString(iField,getJsonValue(jsonObj,"last_touch"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"global_unique"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"customerId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"updated_at"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"description"));//10

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("inserted into hadoop tracking_sources:"+rowKey);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateRpt_call_log_activityTrackingSource(String sIp,JSONObject jsonObj) {
        updateRpt_call_log_activityTrackingSource1(sIp,jsonObj);
        updateRpt_call_log_activityTrackingSource2(sIp,jsonObj);
    }
    public void updateRpt_call_log_activityTrackingSource1(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "update rpt_call_log_activity1 set `name`='"+getJsonValue(jsonObj,"name")
                +"'  where `hostname`='"+sIp+"' and `cid`='"+getJsonValue(jsonObj,"id")+"'  " ;
        PreparedStatement ps;
        try {
            Class.forName(classname);
            java.sql.Connection conn_mysql = DriverManager.getConnection(url_mysql_local);
            ps=conn_mysql.prepareStatement(sSql);

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("update Rpt_call_log_activityTrackingSource1:"+sSql);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateRpt_call_log_activityTrackingSource2(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "update rpt_call_log_activity2 set `name`='"+getJsonValue(jsonObj,"name")
                +"'  where `hostname`='"+sIp+"' and `cid`='"+getJsonValue(jsonObj,"id")+"'  " ;
        PreparedStatement ps;
        try {
            Class.forName(classname);
            java.sql.Connection conn_mysql = DriverManager.getConnection(url_mysql_local);
            ps=conn_mysql.prepareStatement(sSql);

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("update Rpt_call_log_activityTrackingSource1:"+sSql);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateHadoopReceivingNumber(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "upsert into \"receiving_numbers\" ( " +
                "\"id\"  ," +
                "   \"info\".\"hostname\" ," +
                "   \"info\".\"number\" ," +
                "   \"info\".\"description\" " + //4
                "   ) values(" +
                "?,?,?,? " +
                ")";//4
        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            ps=phoenixConn.prepareStatement(sSql);
            int iField = 1;
            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
            ps.setString(iField,rowKey);
            iField++;ps.setString(iField,sIp);
            iField++;ps.setString(iField,getJsonValue(jsonObj,"number"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"description"));//10

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("inserted into hadoop receiving_numbers:"+rowKey);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateHadoopSipGateway(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "upsert into \"sip_gateways\" ( " +
                "\"id\"  ," +
                "   \"info\".\"hostname\" ," +
                "   \"info\".\"name\" ," +
                "   \"info\".\"address\" ," +
                "   \"info\".\"port\" ," +
                "   \"info\".\"digits_strip\" ," +
                "   \"info\".\"description\" " + //4
                "   ) values(" +
                "?,?,?,?,?,?,? " +
                ")";//7
        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            ps=phoenixConn.prepareStatement(sSql);
            int iField = 1;
            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
            ps.setString(iField,rowKey);
            iField++;ps.setString(iField,sIp);
            iField++;ps.setString(iField,getJsonValue(jsonObj,"name"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"address"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"port"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"digits_strip"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"description"));//10

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("inserted into hadoop sip_gateways:"+rowKey);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateHadoopPhonebook(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        String sSql = "upsert into \"phonebooks\" ( " +
                "\"id\"  ," +
                "   \"info\".\"hostname\" ," +
                "   \"info\".\"name\" ," +
                "   \"info\".\"email\" ," +
                "   \"info\".\"street\" ," +
                "   \"info\".\"city\" ," +
                "   \"info\".\"state\" ," +
                "   \"info\".\"country\" ," +
                "   \"info\".\"postal_code\" ," +
                "   \"info\".\"contact_number\" ," +
                "   \"info\".\"note\" " + //4
                "   ) values(" +
                "?,?,?,?,?,?,?,?,?,?, " +
                "? " +
                ")";//11
        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            ps=phoenixConn.prepareStatement(sSql);
            int iField = 1;
            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
            ps.setString(iField,rowKey);
            iField++;ps.setString(iField,sIp);
            iField++;ps.setString(iField,getJsonValue(jsonObj,"name"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"email"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"street"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"city"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"state"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"country"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"postal_code"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"contact_number"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"note"));

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("inserted into hadoop updateHadoopPhonebook:"+rowKey);
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getCallLogMaxId(String sIp) {
        //update hadoop call log's opnumber information
        //get max id at first
//        String sSql = "select max(\"newid\")";//20
//
//
//        PreparedStatement ps;
//        try {
//            Class.forName(classname_phoenix);
//            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
//            ps=phoenixConn.prepareStatement(sSql);
//            int iField = 1;
//            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
//            ps.setString(iField,rowKey);
//            iField++;ps.setString(iField,sIp);
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"customerId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"receiving_numberId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"tracking_number"));//5
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"tracking_sourceId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"sip_gatewayId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"routing_action"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_id"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"target_numbers"));//10
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_settings"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"schedule_id"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"notifications"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"text_support"));//15
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"number_tags"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"failsafe_number"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"renewal_date"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"active"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"type_id"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"description"));
//
//            int i=ps.executeUpdate();
//            //commit
//            phoenixConn.commit();
//            if (i>0){
//                System.out.println("inserted into hadoop opNumber:"+rowKey);
//            }
//
//            if (ps!=null){
//                ps.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return "";
    }
    public void updateHadoopCallLogForTrackingSourceId(String sIp,JSONObject jsonObj,JSONObject jsonData) {
        String  update_call_logs = getJsonValue(jsonObj,"update_call_logs");
        System.out.println("start updateHadoopCallLogForTrackingSourceId update_call_logs:"+update_call_logs);
        System.out.println("start updateHadoopCallLogForTrackingSourceId jsonObj:"+jsonObj.toString());

        if (update_call_logs == null || !update_call_logs.equals("true"))
            return;
        String  tracking_sourceId = getJsonValue(jsonData,"tracking_sourceId");
        String  tracking_number = getJsonValue(jsonData,"tracking_number");
        System.out.println("start updateHadoopCallLogForTrackingSourceId tracking_sourceId:"+tracking_sourceId
                +"  tracking_number:"+tracking_number);

        Date dtStart = new Date();
        String sSql = "upsert into \"call_log\" (\"newid\",\"info\".\"trackingSource_id\")  " +
                "select \"newid\",'" + tracking_sourceId + "' " +
                "from \"call_log\" " +
                "where \"hostname\" = '"+ sIp +"'  " +
                "and \"tracking_number\"='"+ tracking_number +"'  " +
                "and \"trackingSource_id\"<>'"+ tracking_sourceId +"'  " +
                " ";


        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
            ps=phoenixConn.prepareStatement(sSql);

            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
                System.out.println("update call log tracking_sourceId finished.  take "
                        + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            }

            if (ps!=null){
                ps.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("update call log tracking_sourceId exception:"+e.toString());
        }
        System.out.println("update call log tracking_sourceId end.  take "
                + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
    }
    public void updateHadoopCallLogOfOpNumberInfo(String sIp,JSONObject jsonObj) {
        //update hadoop call log's opnumber information
        //get max id at first
//        String sMaxId = getCallLogMaxId(sIp);
//        Date dtStart = new Date();
//        String sSql = "upsert into \"op_number\" ( " +
//                "\"id\"  ," +
//                "   \"info\".\"hostname\" ," +
//                "   \"info\".\"customerId\"    ," +
//                "   \"info\".\"receiving_numberId\" ," +
//                "   \"info\".\"tracking_number\" ," +
//                "   \"info\".\"tracking_sourceId\" ," +
//                "   \"info\".\"sip_gatewayId\" ," +
//                "   \"info\".\"routing_action\" ," +
//                "   \"info\".\"phonebook_id\" ," +
//                "   \"info\".\"target_numbers\"  ," +
//                "   \"info\".\"call_settings\" ," +
//                "   \"info\".\"schedule_id\" ," +
//                "   \"info\".\"notifications\" ," +
//                "   \"info\".\"text_support\" ," +
//                "   \"info\".\"number_tags\" ," +
//                "   \"info\".\"failsafe_number\" ," +
//                "   \"info\".\"renewal_date\" ," +
//                "   \"info\".\"active\" ," +
//                "   \"info\".\"type_id\" ," +
//                "   \"info\".\"description\" " +
//                "   ) values(" +
//                "?,?,?,?,?,?,?,?,?,?," +
//                "?,?,?,?,?,?,?,?,?,?" +
//                ")";//20
//
//
//        PreparedStatement ps;
//        try {
//            Class.forName(classname_phoenix);
//            phoenixConn = DriverManager.getConnection(url_phoenix);            // row key 208.73.232.196_79789898948689_1949385
//            ps=phoenixConn.prepareStatement(sSql);
//            int iField = 1;
//            String rowKey = sIp + "_" + getJsonValue(jsonObj,"id");
//            ps.setString(iField,rowKey);
//            iField++;ps.setString(iField,sIp);
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"customerId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"receiving_numberId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"tracking_number"));//5
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"tracking_sourceId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"sip_gatewayId"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"routing_action"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_id"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"target_numbers"));//10
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_settings"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"schedule_id"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"notifications"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"text_support"));//15
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"number_tags"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"failsafe_number"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"renewal_date"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"active"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"type_id"));
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"description"));
//
//            int i=ps.executeUpdate();
//            //commit
//            phoenixConn.commit();
//            if (i>0){
//                System.out.println("inserted into hadoop opNumber:"+rowKey);
//            }
//
//            if (ps!=null){
//                ps.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
    public String update_opNumber_trackingSource(String sIp,JSONObject jsonObj) {
        Date dtStart = new Date();
        //1 get mysql data
        String sId = getJsonValue(jsonObj,"id");
        System.out.println("update_opNumber_trackingSource:" + sId);
        JSONObject jsonData = getMysqlOpNumber(sIp,sId);
        if (jsonData==null)
            return "";
        //2 update opnumber
        updateHadoopOpNumber(sIp,jsonData);
        //3 update to call log
        updateHadoopCallLogForTrackingSourceId(sIp,jsonObj,jsonData);
        //4 re statics the activity and overview report

        return "";
        //add it to sum table
//        java.sql.Connection conn = null;
//        ResultSet rs = null;
//        Statement stmt = null;
//        try {
//            Class.forName(classname_phoenix);
//            phoenixConn = DriverManager.getConnection(url_phoenix);
//            Statement  statement = phoenixConn.createStatement();
//
//            Class.forName(classname);
//            String url = getMysqlUrl(sIp);
//            conn = DriverManager.getConnection(url);
//            stmt = conn.createStatement();
//            String sSql = "select * from op_number where id=? ";
//
//            PreparedStatement ps=conn.prepareStatement(sSql);
//            ps.setString(1,getJsonValue(jsonObj,"opNumber_id"));
//            rs = stmt.executeQuery(sSql);
//
//
//            //update opNumber table at first
//            String sSqlCount  = "select count(*) as ccc from \"call_log\" where \"hostname\"='"+ sIp + "'"+
//                    " and \"opNumber_id\"="+ getJsonValueForSqlValue(jsonObj,"opNumber_id") +
//                    " and \"opNumber_trackingNumber\"<>"+ getJsonValueForSqlValue(jsonObj,"opNumber_trackingNumber") +
//                    " ";
//
//            //add to phoenix
//            String sql="upsert into \"call_log\" ( " +
//                    "\"newid\"  ," +
//                    "   \"info\".\"opNumber_trackingNumber\" ," +
//                    "   \"info\".\"opNumber_routingAction\" ," +
//                    "   \"info\".\"opNumber_notifications\" ," +
//                    "   \"info\".\"opNumber_textSupport\" ," +
//                    "   \"info\".\"opNumber_numberTags\" ," +
//                    "   \"info\".\"opNumber_failsafeNumber\" ," +
//                    "   \"info\".\"opNumber_renewalDate\" ," +
//                    "   \"info\".\"opNumber_active\" ," +
//                    "   \"info\".\"opNumber_description\" ," +
//
//                    "   \"info\".\"trackingSource_id\" ," +
//                    "   \"info\".\"trackingSource_name\" ," +
//                    "   \"info\".\"trackingSource_type\" ," +
//                    "   \"info\".\"trackingSource_position\" ," +
//                    "   \"info\".\"trackingSource_description\" ," +
//
//                    "   \"info\".\"receivingNumber_id\" ," +
//                    "   \"info\".\"receivingNumber_number\" ," +
//                    "   \"info\".\"receivingNumber_description\" ," +
//
//                    "   \"info\".\"sipGateway_id\" ," +
//                    "   \"info\".\"sipGateway_name\" ," +
//                    "   \"info\".\"sipGateway_address\" ," +
//                    "   \"info\".\"sipGateway_port\" ," +
//                    "   \"info\".\"sipGateway_digitsStrip\" ," +
//                    "   \"info\".\"sipGateway_description\"," +
//
//                    "   \"info\".\"customerId\" " +
//                    "  ) " +
//
//                    " select " +
//                    "\"newid\"  ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_trackingNumber") +  " as \"opNumber_trackingNumber\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_routingAction") +  " as \"opNumber_routingAction\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_notifications") +  " as \"opNumber_notifications\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_textSupport") +  " as \"opNumber_textSupport\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_numberTags") +  " as \"opNumber_numberTags\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_failsafeNumber") +  " as \"opNumber_failsafeNumber\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_renewalDate") +  " as \"opNumber_renewalDate\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_active") +  " as \"opNumber_active\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_description") +  " as \"opNumber_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_id") +  " as \"trackingSource_id\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_name") +  " as \"trackingSource_name\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_type") +  " as \"trackingSource_type\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_position") +  " as \"trackingSource_position\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_description") +  " as \"trackingSource_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"receivingNumber_id") +  " as \"receivingNumber_id\" ," +
//                    getJsonValueForSqlValue(jsonObj,"receivingNumber_number") +  " as \"receivingNumber_number\" ," +
//                    getJsonValueForSqlValue(jsonObj,"receivingNumber_description") +  " as \"receivingNumber_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_id") +  " as \"sipGateway_id\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_name") +  " as \"sipGateway_name\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_address") +  " as \"sipGateway_address\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_port") +  " as \"sipGateway_port\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_digitsStrip") +  " as \"sipGateway_digitsStrip\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_description") +  " as \"sipGateway_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"customerId") +  " as \"customerId\" " +
//
//                    " from \"call_log\" " +
//                    " where \"hostname\"='"+ sIp + "'"+
//                    " and \"opNumber_id\"="+ getJsonValueForSqlValue(jsonObj,"opNumber_id") +
//                    " and \"opNumber_trackingNumber\"<>"+ getJsonValueForSqlValue(jsonObj,"opNumber_trackingNumber") +
//                    " limit 10000 "
//                    ;
//            System.out.println("sSqlCount:"+sSqlCount);
//            System.out.println("sql:"+sql);
//
//            ResultSet rs = null;
//            int iNo = 1;
//            while (iNo>0) {
//                PreparedStatement ps=phoenixConn.prepareStatement(sql);
//                ps.executeUpdate();
//                phoenixConn.commit();
//                if (ps!=null){
//                    ps.close();
//                }
//                System.out.println("update opnumber left count:"+ String.valueOf(iNo) + "  take " + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
//
//                rs = statement.executeQuery(sSqlCount);
//                while (rs.next()) {
//                    iNo = rs.getInt(1);
//                }
//            }
//
//            // 关闭连接
//            rs.close();
//            statement.close();
//            phoenixConn.close();
//            System.out.println("update opnumber end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
//
//            return "";
//        } catch (Exception e) {
//            System.out.println("get error:"+e.toString());
//            return "error";
//        }
    }
    public String update_opNumber(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //1 get mysql data
        String sId = getJsonValue(jsonObj,"opNumber_id");
        System.out.println("update_opNumber:" + sId);
        JSONObject jsonData = getMysqlOpNumber(sIp,sId);
        if (jsonData==null)
            return "";
        //2 update opnumber
        updateHadoopOpNumber(sIp,jsonData);
        //3 update to call log
        updateHadoopCallLogOfOpNumberInfo(sIp,jsonData);
        return "";
        //add it to sum table
//        java.sql.Connection conn = null;
//        ResultSet rs = null;
//        Statement stmt = null;
//        try {
//            Class.forName(classname_phoenix);
//            phoenixConn = DriverManager.getConnection(url_phoenix);
//            Statement  statement = phoenixConn.createStatement();
//
//            Class.forName(classname);
//            String url = getMysqlUrl(sIp);
//            conn = DriverManager.getConnection(url);
//            stmt = conn.createStatement();
//            String sSql = "select * from op_number where id=? ";
//
//            PreparedStatement ps=conn.prepareStatement(sSql);
//            ps.setString(1,getJsonValue(jsonObj,"opNumber_id"));
//            rs = stmt.executeQuery(sSql);
//
//
//            //update opNumber table at first
//            String sSqlCount  = "select count(*) as ccc from \"call_log\" where \"hostname\"='"+ sIp + "'"+
//                    " and \"opNumber_id\"="+ getJsonValueForSqlValue(jsonObj,"opNumber_id") +
//                    " and \"opNumber_trackingNumber\"<>"+ getJsonValueForSqlValue(jsonObj,"opNumber_trackingNumber") +
//                    " ";
//
//            //add to phoenix
//            String sql="upsert into \"call_log\" ( " +
//                    "\"newid\"  ," +
//                    "   \"info\".\"opNumber_trackingNumber\" ," +
//                    "   \"info\".\"opNumber_routingAction\" ," +
//                    "   \"info\".\"opNumber_notifications\" ," +
//                    "   \"info\".\"opNumber_textSupport\" ," +
//                    "   \"info\".\"opNumber_numberTags\" ," +
//                    "   \"info\".\"opNumber_failsafeNumber\" ," +
//                    "   \"info\".\"opNumber_renewalDate\" ," +
//                    "   \"info\".\"opNumber_active\" ," +
//                    "   \"info\".\"opNumber_description\" ," +
//
//                    "   \"info\".\"trackingSource_id\" ," +
//                    "   \"info\".\"trackingSource_name\" ," +
//                    "   \"info\".\"trackingSource_type\" ," +
//                    "   \"info\".\"trackingSource_position\" ," +
//                    "   \"info\".\"trackingSource_description\" ," +
//
//                    "   \"info\".\"receivingNumber_id\" ," +
//                    "   \"info\".\"receivingNumber_number\" ," +
//                    "   \"info\".\"receivingNumber_description\" ," +
//
//                    "   \"info\".\"sipGateway_id\" ," +
//                    "   \"info\".\"sipGateway_name\" ," +
//                    "   \"info\".\"sipGateway_address\" ," +
//                    "   \"info\".\"sipGateway_port\" ," +
//                    "   \"info\".\"sipGateway_digitsStrip\" ," +
//                    "   \"info\".\"sipGateway_description\"," +
//
//                    "   \"info\".\"customerId\" " +
//                    "  ) " +
//
//                    " select " +
//                    "\"newid\"  ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_trackingNumber") +  " as \"opNumber_trackingNumber\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_routingAction") +  " as \"opNumber_routingAction\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_notifications") +  " as \"opNumber_notifications\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_textSupport") +  " as \"opNumber_textSupport\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_numberTags") +  " as \"opNumber_numberTags\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_failsafeNumber") +  " as \"opNumber_failsafeNumber\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_renewalDate") +  " as \"opNumber_renewalDate\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_active") +  " as \"opNumber_active\" ," +
//                    getJsonValueForSqlValue(jsonObj,"opNumber_description") +  " as \"opNumber_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_id") +  " as \"trackingSource_id\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_name") +  " as \"trackingSource_name\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_type") +  " as \"trackingSource_type\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_position") +  " as \"trackingSource_position\" ," +
//                    getJsonValueForSqlValue(jsonObj,"trackingSource_description") +  " as \"trackingSource_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"receivingNumber_id") +  " as \"receivingNumber_id\" ," +
//                    getJsonValueForSqlValue(jsonObj,"receivingNumber_number") +  " as \"receivingNumber_number\" ," +
//                    getJsonValueForSqlValue(jsonObj,"receivingNumber_description") +  " as \"receivingNumber_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_id") +  " as \"sipGateway_id\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_name") +  " as \"sipGateway_name\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_address") +  " as \"sipGateway_address\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_port") +  " as \"sipGateway_port\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_digitsStrip") +  " as \"sipGateway_digitsStrip\" ," +
//                    getJsonValueForSqlValue(jsonObj,"sipGateway_description") +  " as \"sipGateway_description\" ," +
//
//                    getJsonValueForSqlValue(jsonObj,"customerId") +  " as \"customerId\" " +
//
//                    " from \"call_log\" " +
//                    " where \"hostname\"='"+ sIp + "'"+
//                    " and \"opNumber_id\"="+ getJsonValueForSqlValue(jsonObj,"opNumber_id") +
//                    " and \"opNumber_trackingNumber\"<>"+ getJsonValueForSqlValue(jsonObj,"opNumber_trackingNumber") +
//                    " limit 10000 "
//                    ;
//            System.out.println("sSqlCount:"+sSqlCount);
//            System.out.println("sql:"+sql);
//
//            ResultSet rs = null;
//            int iNo = 1;
//            while (iNo>0) {
//                PreparedStatement ps=phoenixConn.prepareStatement(sql);
//                ps.executeUpdate();
//                phoenixConn.commit();
//                if (ps!=null){
//                    ps.close();
//                }
//                System.out.println("update opnumber left count:"+ String.valueOf(iNo) + "  take " + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
//
//                rs = statement.executeQuery(sSqlCount);
//                while (rs.next()) {
//                    iNo = rs.getInt(1);
//                }
//            }
//
//            // 关闭连接
//            rs.close();
//            statement.close();
//            phoenixConn.close();
//            System.out.println("update opnumber end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
//
//            return "";
//        } catch (Exception e) {
//            System.out.println("get error:"+e.toString());
//            return "error";
//        }
    }
    public String delete_opNumber(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //delete opNumber table
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);
            String sql = "delete from \"op_number\" where \"id\"='" + sIp
                    + "_" + getJsonValue(jsonObj, "opNumber_id") + "'" +
                    " ";
            System.out.println("sql:" + sql);

            PreparedStatement ps = phoenixConn.prepareStatement(sql);
            ps.executeUpdate();
            phoenixConn.commit();

            if (ps != null) {
                ps.close();
            }

            // 关闭连接
            phoenixConn.close();
            System.out.println("delete opnumber end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");

            return "";
        } catch (Exception e) {
            System.out.println("get error:" + e.toString());
            return "error";
        }
        //redo static summary table
        //do it late
    }
    public String delete_customer(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //delete opNumber table
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);
            String sql = "delete from \"customers\" where \"id\"='" + sIp
                    + "_" + getJsonValue(jsonObj, "id") + "'" +
                    " ";
            System.out.println("sql:" + sql);

            PreparedStatement ps = phoenixConn.prepareStatement(sql);
            ps.executeUpdate();
            phoenixConn.commit();

            if (ps != null) {
                ps.close();
            }

            // 关闭连接
            phoenixConn.close();
            System.out.println("delete customer end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");

            return "";
        } catch (Exception e) {
            System.out.println("get error:" + e.toString());
            return "error";
        }
        //redo static summary table
        //do it late
    }
    public String delete_receivingNumber(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //delete opNumber table
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);
            String sql = "delete from \"receiving_numbers\" where \"id\"='" + sIp
                    + "_" + getJsonValue(jsonObj, "id") + "'" +
                    " ";
            System.out.println("sql:" + sql);

            PreparedStatement ps = phoenixConn.prepareStatement(sql);
            ps.executeUpdate();
            phoenixConn.commit();

            if (ps != null) {
                ps.close();
            }

            // 关闭连接
            phoenixConn.close();
            System.out.println("delete receiving_numbers end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");

            return "";
        } catch (Exception e) {
            System.out.println("get error:" + e.toString());
            return "error";
        }
        //redo static summary table
        //do it late
    }

    public String update_customer(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //1 get mysql data
        String sId = getJsonValue(jsonObj,"id");
        System.out.println("update_customer:" + sId);
        JSONObject jsonData = getMysqlCustomer(sIp,sId);
        if (jsonData==null)
            return "";
        //2 update customer
        updateHadoopCustomer(sIp,jsonData);
        //3 update to call log
        return "";
    }
    public String update_trackingSources(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //1 get mysql data
        String sId = getJsonValue(jsonObj,"id");
        System.out.println("update_trackingSources:" + sId);
        JSONObject jsonData = getMysqlTrackingSource(sIp,sId);
        if (jsonData==null)
            return "";
        //2 update trackingSources
        updateHadoopTrackingSource(sIp,jsonData);
        //3 update to rpt_call_log_activity1 and rpt_call_log_activity2
        updateRpt_call_log_activityTrackingSource(sIp,jsonData);
        return "";
    }
    public String sync_trackingSources(String sIp) {
        Date dtStart = new Date();
        //1 get mysql data
        JSONObject jsonDataList = getMysqlTrackingSources(sIp);
        for (Map.Entry entry : jsonDataList.entrySet()) {
            JSONObject jsonData = (JSONObject)entry.getValue();
            if (jsonData==null)
                continue;
            updateHadoopTrackingSource(sIp,jsonData);
        }
        //3 update to call log
        return "";
    }
    public String delete_trackingSources(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //delete tracking_sources table
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);
            String sql = "delete from \"tracking_sources\" where \"id\"='" + sIp
                    + "_" + getJsonValue(jsonObj, "id") + "'" +
                    " ";
            System.out.println("sql:" + sql);

            PreparedStatement ps = phoenixConn.prepareStatement(sql);
            ps.executeUpdate();
            phoenixConn.commit();

            if (ps != null) {
                ps.close();
            }

            // 关闭连接
            phoenixConn.close();
            System.out.println("delete customer end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");

            return "";
        } catch (Exception e) {
            System.out.println("get error:" + e.toString());
            return "error";
        }
        //redo static summary table
        //do it late
    }
    public String update_receivingNumber(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //1 get mysql data
        String sId = getJsonValue(jsonObj,"id");
        System.out.println("update_receivingNumbers:" + sId);
        JSONObject jsonData = getMysqlReceivingNumber(sIp,sId);
        if (jsonData==null)
            return "";
        //2 update customer
        updateHadoopReceivingNumber(sIp,jsonData);
        //3 update to call log
        return "";
    }
    public String update_sipGateways(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //1 get mysql data
        String sId = getJsonValue(jsonObj,"id");
        System.out.println("update_sipGateways:" + sId);
        JSONObject jsonData = getMysqlSipGateway(sIp,sId);
        if (jsonData==null)
            return "";
        //2 update opnumber
        updateHadoopSipGateway(sIp,jsonData);
        return "";
    }
    public String delete_sipGateways(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //delete tracking_sources table
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);
            String sql = "delete from \"sip_gateways\" where \"id\"='" + sIp
                    + "_" + getJsonValue(jsonObj, "id") + "'" +
                    " ";
            System.out.println("sql:" + sql);

            PreparedStatement ps = phoenixConn.prepareStatement(sql);
            ps.executeUpdate();
            phoenixConn.commit();

            if (ps != null) {
                ps.close();
            }

            // 关闭连接
            phoenixConn.close();
            System.out.println("delete sip_gateways end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");

            return "";
        } catch (Exception e) {
            System.out.println("get error:" + e.toString());
            return "error";
        }
        //redo static summary table
        //do it late
    }
    public String update_phonebooks(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //1 get mysql data
        String sId = getJsonValue(jsonObj,"id");
        System.out.println("update_phonebooks:" + sId);
        JSONObject jsonData = getMysqlPhonebook(sIp,sId);
        if (jsonData==null)
            return "";
        //2 update opnumber
        updateHadoopPhonebook(sIp,jsonData);
        return "";
    }
    public String delete_phonebooks(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        //delete tracking_sources table
        try {
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);
            String sql = "delete from \"phonebooks\" where \"id\"='" + sIp
                    + "_" + getJsonValue(jsonObj, "id") + "'" +
                    " ";
            System.out.println("sql:" + sql);

            PreparedStatement ps = phoenixConn.prepareStatement(sql);
            ps.executeUpdate();
            phoenixConn.commit();

            if (ps != null) {
                ps.close();
            }

            // 关闭连接
            phoenixConn.close();
            System.out.println("delete phonebooks end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");

            return "";
        } catch (Exception e) {
            System.out.println("get error:" + e.toString());
            return "error";
        }
        //redo static summary table
        //do it late
    }

    @Test
    public void testGetCall_date() {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("created","2021-03-24T10:51:34.000Z");
        System.out.println(getCall_Date(jsonObj));
    }
    public void addCDRsToPhoenix(String rowKey,String sIp,JSONObject jsonObj) {
        PreparedStatement ps;
//        ResultSet resultSet;
        try {
            String sql="upsert into \"call_log\" ( " +
                    "\"newid\"  ," +
                    "   \"info\".\"hostname\" ," +
                    "   \"info\".\"id\"    ," +
                    "   \"info\".\"call_id\" ," +
                    "   \"info\".\"call_direction\" ," +
                    "   \"info\".\"caller_number\" ," +
                    "   \"info\".\"tracking_number\" ," +
                    "   \"info\".\"customerId\" ," +
                    "   \"info\".\"session_data\" ," +
                    "   \"info\".\"metrics\" ," +
                    "   \"info\".\"flag\"  ," +
                    "   \"info\".\"call_terminated\" ," +
                    "   \"info\".\"call_status\" ," +
                    "   \"info\".\"call_status_message\" ," +
                    "   \"info\".\"duration\" ," +
                    "   \"info\".\"ms_duration\" ," +
                    "   \"info\".\"setuptime\" ," +
                    "   \"info\".\"created\" ," +
                    "   \"info\".\"phonebook_id\" ," +
                    "   \"info\".\"phonebook_name\" ," +
                    "   \"info\".\"phonebook_email\" ," +
                    "   \"info\".\"phonebook_street\" ," +
                    "   \"info\".\"phonebook_city\" ," +
                    "   \"info\".\"phonebook_state\" ," +
                    "   \"info\".\"phonebook_country\" ," +
                    "   \"info\".\"phonebook_postalCode\" ," +
                    "   \"info\".\"phonebook_contactNumber\" ," +
                    "   \"info\".\"phonebook_note\" ," +
                    "   \"info\".\"callRecording_id\" ," +
                    "   \"info\".\"callRecording_name\" ," +
                    "   \"info\".\"callRecording_timestamp\" ," +
                    "   \"info\".\"callRecording_content\" ," +
                    "   \"info\".\"callRecording_duration\" ," +
                    "   \"info\".\"callRecording_tages\" ," +
                    "   \"info\".\"callRecording_visible\" ," +
                    "   \"info\".\"opNumber_id\" ," +
                    "   \"info\".\"opNumber_trackingNumber\" ," +
                    "   \"info\".\"opNumber_routingAction\" ," +
                    "   \"info\".\"opNumber_notifications\" ," +
                    "   \"info\".\"opNumber_textSupport\" ," +
                    "   \"info\".\"opNumber_numberTags\" ," +
                    "   \"info\".\"opNumber_failsafeNumber\" ," +
                    "   \"info\".\"opNumber_renewalDate\" ," +
                    "   \"info\".\"opNumber_active\" ," +
                    "   \"info\".\"opNumber_description\" ," +
                    "   \"info\".\"trackingSource_id\" ," +
                    "   \"info\".\"trackingSource_name\" ," +
                    "   \"info\".\"trackingSource_type\" ," +
                    "   \"info\".\"trackingSource_position\" ," +
                    "   \"info\".\"trackingSource_lastTouch\" ," +
                    "   \"info\".\"trackingSource_globalUnique\" ," +
                    "   \"info\".\"trackingSource_updatedAt\" ," +
                    "   \"info\".\"trackingSource_description\" ," +
                    "   \"info\".\"receivingNumber_id\" ," +
                    "   \"info\".\"receivingNumber_number\" ," +
                    "   \"info\".\"receivingNumber_description\" ," +
                    "   \"info\".\"sipGateway_id\" ," +
                    "   \"info\".\"sipGateway_name\" ," +
                    "   \"info\".\"sipGateway_address\" ," +
                    "   \"info\".\"sipGateway_port\" ," +
                    "   \"info\".\"sipGateway_digitsStrip\" ," +
                    "   \"info\".\"sipGateway_description\"" +
                    "   ) values(" +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?" +
                    ")";//62
            ps=phoenixConn.prepareStatement(sql);
            int iField = 1;
            ps.setString(iField,rowKey);
            //change to ip
//            iField++;ps.setString(iField,getJsonValue(jsonObj,"hostname"));
            iField++;ps.setString(iField,sIp);
            iField++;ps.setString(iField,getJsonValue(jsonObj,"id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_direction"));//5
            iField++;ps.setString(iField,getJsonValue(jsonObj,"caller_number"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"tracking_number"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"customerId"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"session_data"));//9
            iField++;ps.setString(iField,getJsonValue(jsonObj,"metrics"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"flag"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_terminated"));//12
            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_status"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"call_status_message"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"duration"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"ms_duration"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"setuptime"));//17
            iField++;ps.setString(iField,getJsonValue(jsonObj,"created"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_name"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_email"));//21
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_street"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_city"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_state"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_country"));//25
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_postalCode"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_contactNumber"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"phonebook_note"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"callRecording_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"callRecording_name"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"callRecording_timestamp"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"callRecording_content"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"callRecording_duration"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"callRecording_tages"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"callRecording_visible"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_trackingNumber"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_routingAction"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_notifications"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_textSupport"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_numberTags"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_failsafeNumber"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_renewalDate"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_active"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"opNumber_description"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_name"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_type"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_position"));//49
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_lastTouch"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_globalUnique"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_updatedAt"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"trackingSource_description"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"receivingNumber_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"receivingNumber_number"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"receivingNumber_description"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"sipGateway_id"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"sipGateway_name"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"sipGateway_address"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"sipGateway_port"));
            iField++;ps.setString(iField,getJsonValue(jsonObj,"sipGateway_digitsStrip"));//61
            iField++;ps.setString(iField,getJsonValue(jsonObj,"sipGateway_description"));
            int i=ps.executeUpdate();
            //commit
            phoenixConn.commit();
            if (i>0){
//                if (sIp.equals("208.73.232.209")) {
//                    System.out.println("inserted into call log:" + rowKey);
//                }
                System.out.println("inserted into call log:" + rowKey);
            }
            //add to mysql table
            java.sql.Connection conn_mysql = null;
            Class.forName(classname);
            conn_mysql = DriverManager.getConnection(url_mysql_local);
            PreparedStatement ps_mysql;
            //add rpt_call_log_activity1
            sql="insert into rpt_call_log_activity1 ( " +
                    "id  ," +
                    "   call_date ," +
                    "   hostname    ," +
                    "   customerid ," +
                    "   cid ," +
                    "   name ," +
                    "   total_calls ," +
                    "   total_time " +

                    "   ) values(" +
                    "?,?,?,?,?,?,1," + getJsonValue(jsonObj,"duration") +
                    ")  ON DUPLICATE KEY UPDATE total_calls = total_calls + 1, " +
                    " total_time = total_time + " + getJsonValue(jsonObj,"duration")
            ;  //8
            ps_mysql=conn_mysql.prepareStatement(sql);
            iField = 1;
            String sValue = sIp
                    + "**" + getCall_Date(jsonObj)  //change created to 2021031205
                    + "**" + getJsonValue(jsonObj,"trackingSource_id")
                    + "**" + getJsonValue(jsonObj,"customerId")
                    ;
//            ,b.customerid,'**',date_format(a.created, '%Y%m%d%H'),'**',c.name ,'**',c.id,
//                    '**',d.`contact_number`;
            ps_mysql.setString(iField,sValue);
            iField++;ps_mysql.setString(iField,getCall_Date(jsonObj));
//            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"hostname"));
            iField++;ps_mysql.setString(iField,sIp);
            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"customerId","0"));//5
            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_id"));
            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_name"));
            i=ps_mysql.executeUpdate();
            //add rpt_call_log_activity2
            sql="insert into rpt_call_log_activity2 ( " +
                    "id  ," +
                    "   call_date ," +
                    "   hostname    ," +
                    "   customerid ," +
                    "   cid ," +
                    "   name ," +
                    "   contact_number ," +
                    "   total_calls ," +
                    "   total_time " +

                    "   ) values(" +
                    "?,?,?,?,?,?,?,1," + getJsonValue(jsonObj,"duration") +
                    ")  ON DUPLICATE KEY UPDATE total_calls = total_calls + 1, " +
                    " total_time = total_time + " + getJsonValue(jsonObj,"duration")
            ;  //9
            ps_mysql=conn_mysql.prepareStatement(sql);
            iField = 1;
            sValue = sIp
                    + "**" + getJsonValue(jsonObj,"customerId")
                    + "**" + getCall_Date(jsonObj)  //change created to 2021031205
                    + "**" + getJsonValue(jsonObj,"trackingSource_name")
                    + "**" + getJsonValue(jsonObj,"trackingSource_id")
                    + "**" + getJsonValue(jsonObj,"tracking_number")
            ;
//            ,b.customerid,'**',date_format(a.created, '%Y%m%d%H'),'**',c.name ,'**',c.id,
//                    '**',d.`contact_number`;
            ps_mysql.setString(iField,sValue);
            iField++;ps_mysql.setString(iField,getCall_Date(jsonObj));
//            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"hostname"));
            iField++;ps_mysql.setString(iField,sIp);
            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"customerId","0"));
            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_id"));
            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_name"));
            iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"tracking_number"));
            i=ps_mysql.executeUpdate();

            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn_mysql!=null){
                conn_mysql.close();
            }
            if (ps!=null){
                ps.close();
            }
        }
        catch (Exception e) {
            System.out.println("Exception:"+e.toString());
        }
    }
    public String addCDRs_bak2(String sIp, Map<String, String> map) {
        Date dtStart = new Date();
        //update call log
        //write to hbase
        //add it to sum table
        //add relation table
        String tablename = "cdr";

        Table table = getTable(tablename);

        String qualifier, value;
        String rowKey = "";
        try {
            // row key 208.73.232.196_79789898948689_1949385
//            rowKey = getCallLogKey(sIp, map);
            String sFamily = "info";
            Put put = new Put(Bytes.toBytes(rowKey));
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getValue() != null) {
                    qualifier = entry.getKey();
                    value = entry.getValue();
                    put.addColumn(Bytes.toBytes(sFamily), Bytes.toBytes(qualifier), Bytes.toBytes(value));
                }
            }
            table.put(put);
            table.close();
            System.out.println("added call log:" + rowKey
                    + " takes :" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("{\n");
            sb.append("\"id\":" + rowKey + "\n");
            sb.append("}\n");
            return sb.toString();
        } catch (IOException e) {
            System.out.println("get error:");
//            e.printStackTrace();
            System.out.println(e.toString());
            System.out.println("get error end:");
            return "error";
        }
    }

    public String addCDRs_bak(String sIp, JSONObject jsonObj) {
        Date dtStart = new Date();
        Map<String, String> tmp_cdr = new HashMap<String, String>();
        if(jsonObj.getInteger("prosbc_drct")==1) {
            String reason = jsonObj.getString("term_cause");
            int code = 0;
            if (reason.matches("^NORMAL")) {
                code = 200;
            } else if (reason.matches("^\\d+_.*")) {
                String[] matches = reason.split("_");
                code = Integer.parseInt(matches[0]);
                reason = matches[1];
                for (int i = 2; i < matches.length; i++) {
                    reason += "_" + matches[i];
                }
            } else {
                code = 500;
            }
            System.out.println("term_cause:" + jsonObj.getString("term_cause") + "code:" + code + "  reason:" + reason);
            tmp_cdr.put("prosbc_drct", "1");
            tmp_cdr.put("id", String.valueOf(iCallLogNo));
            tmp_cdr.put("is_direct","1"); //create cdr flag
            iCallLogNo++;
            tmp_cdr.put("reason", reason);
            tmp_cdr.put("ms_duration", String.valueOf(jsonObj.getInteger("duration") * 1000));
            tmp_cdr.put("duration", String.valueOf(jsonObj.getInteger("duration")));
            tmp_cdr.put("call_id", jsonObj.getString("session_id").replaceAll(" ", ""));
            tmp_cdr.put("caller_number", jsonObj.getString("calling"));
            tmp_cdr.put("called_user", jsonObj.getString("called"));
            tmp_cdr.put("code", String.valueOf(code));
            tmp_cdr.put("setuptime", "0");
            tmp_cdr.put("method", "INVITE");
            tmp_cdr.put("duration", jsonObj.getString("duration"));
            tmp_cdr.put("customerId", "1");
            tmp_cdr.put("opnumberId", "1");
            BigInteger connc_time = new BigInteger(jsonObj.getString("connc_time"));
            BigInteger zero = new BigInteger("0");
            String call_start_time = jsonObj.getString("start_time");
            if (connc_time.compareTo(zero) > 0) {
                call_start_time = jsonObj.getString("connc_time");
            }
            tmp_cdr.put("call_start_time", call_start_time);
        }
        //setup recordid, danilo said need not setup it
        //setup opnumer
        String opnumberId = map_opnumber.get(tmp_cdr.get("called_user"));
        if (opnumberId!=null) {
            tmp_cdr.put("opnumberId", opnumberId);
            String customerId = map_customer.get(opnumberId);
            if (customerId!=null) {
                tmp_cdr.put("customerId", customerId);
            }
        }
        //setup Phonebook, query or create
        String phonebookId = map_phonebook.get(tmp_cdr.get("caller_number"));
        if (phonebookId!=null) {
            tmp_cdr.put("phonebookId", phonebookId);
        }
        String call_status = "No Answer";
        if(tmp_cdr.get("reason").equals("OK")) {
            call_status = "Answered";
        }
        if(tmp_cdr.get("prosbc_drct").equals("1")) {
            if(tmp_cdr.get("code").equals("200")) {
                call_status = "Answered";
            } else {
                call_status = "No Answer";
            }

            call_status += "\n(" + tmp_cdr.get("reason") + ")";
        }
        Date dd=new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startime = df.format(dd);
        if (tmp_cdr.get("call_start_time") != null) {
            startime  = getDateForLinux(tmp_cdr.get("call_start_time"));
        }
        tmp_cdr.put("call_direction","inbound");
        tmp_cdr.put("caller_contactId",phonebookId);
        tmp_cdr.put("phonebookId",opnumberId);
        String metrics = startime + '\n' + call_status;
        tmp_cdr.put("metrics",metrics);
        tmp_cdr.put("flag","1");
        tmp_cdr.put("call_terminated","1");
        tmp_cdr.put("call_status",tmp_cdr.get("code"));
        tmp_cdr.put("call_status_message",tmp_cdr.get("reason"));
        if(tmp_cdr.get("created")==null) {
            tmp_cdr.put("created",startime);
        }
        //update call log
        //write to hbase
        //add it to sum table
        //add relation table
        String tablename = "cdr";

        Table table = getTable(tablename);

        String qualifier, value;
        int outb = 1;
        String rowKey = "";
        try {
            // row key 208.73.232.196_79789898948689_1949385
//            rowKey = getCallLogKey(sIp, tmp_cdr);
            String sFamily = "info";
            Put put = new Put(Bytes.toBytes(rowKey));
            for (Map.Entry<String, String> entry : tmp_cdr.entrySet()) {
                if (entry.getValue()!=null) {
                    qualifier = entry.getKey();
                    value = entry.getValue();
                    put.addColumn(Bytes.toBytes(sFamily), Bytes.toBytes(qualifier), Bytes.toBytes(value));
                }
            }
            table.put(put);
            table.close();
            System.out.println("added call log:"+rowKey
                    +" takes :"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("{\n");
            sb.append("\"id\":"+rowKey+"\n");
            sb.append("}\n");
            return sb.toString();
        } catch (IOException e) {
            System.out.println("get error:");
//            e.printStackTrace();
            System.out.println(e.toString());
            System.out.println("get error end:");
            return "error";
        }
    }

    public String getHbaseDateStr(String sDate) {
        BigInteger biMax =  new BigInteger("100000000000000");
        BigInteger biDate =  new BigInteger(sDate);
        BigInteger bb =  biMax.subtract(biDate);
        return bb.toString();
    }
    @Test
    public void TestgetHbaseDateStr() {
        String reason = "503_SERVICE_UNAVAIL";
        int code = 0;
        if(reason.matches("^NORMAL")) {
            code = 200;
        } else if(reason.matches("^\\d+_.*")) {
            String[] matches = reason.split("_");
            code = Integer.parseInt(matches[0]);
            reason = matches[1];
            for (int i=2;i<matches.length;i++) {
                reason += "_" + matches[i];
            }
        } else {
            code = 500;
        }
        System.out.println("term_cause:"+"code:"+code + "  reason:"+reason);


        System.out.println(getHbaseDateStr("20210108042022"));
    }
    @Test
    public void testHourValid() {
        String s ="208.73.232.196_79798793956982_1196991";
        String sHour = "04";
        sHour = "95";
        int iStart = 8 + "208.73.232.196_".length();
        String regex ="208.73.232.196_\\d{8}95\\d{4}_\\d+";
        System.out.println(s.matches(regex));
    }

    private FilterList getFilterList(String sFilter,String sFields) {
        if (!sFilter.equals("")) {
            FilterList allFilters = new FilterList(FilterList.Operator.MUST_PASS_ONE);
            if (sFields.equals("")) {
                SingleColumnValueFilter filter_op_numberTracking_sourcesName = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("op_numberTracking_sourcesName"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                filter_op_numberTracking_sourcesName.setFilterIfMissing(true);
                allFilters.addFilter(filter_op_numberTracking_sourcesName);
            } else {
                String[] sList = sFields.split(",");
                for (String sField : sList) {
                    if (sField.equals("OpNumber.TrackingSources.name")) {
                        SingleColumnValueFilter filter_op_numberTracking_sourcesName = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("op_numberTracking_sourcesName"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                        filter_op_numberTracking_sourcesName.setFilterIfMissing(true);
                        allFilters.addFilter(filter_op_numberTracking_sourcesName);
                    } else if (sField.equals("Phonebook.name")) {
                        SingleColumnValueFilter filter_phonebookName = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("phonebookName"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                        filter_phonebookName.setFilterIfMissing(true);
                        allFilters.addFilter(filter_phonebookName);
                    } else if (sField.equals("Phonebook.city")) {
                        SingleColumnValueFilter filter_phonebookCity = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("phonebookCity"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                        filter_phonebookCity.setFilterIfMissing(true);
                        allFilters.addFilter(filter_phonebookCity);
                    } else if (sField.equals("Phonebook.state")) {
                        SingleColumnValueFilter filter_phonebookState = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("phonebookState"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                        filter_phonebookState.setFilterIfMissing(true);
                        allFilters.addFilter(filter_phonebookState);
                    } else if (sField.equals("Phonebook.country")) {
                        SingleColumnValueFilter filter_phonebookCountry = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("phonebookCountry"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                        filter_phonebookCountry.setFilterIfMissing(true);
                        allFilters.addFilter(filter_phonebookCountry);
                    } else if (sField.equals("OpNumber.tracking_number")) {
                        SingleColumnValueFilter filter_op_numberTracking_number = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("op_numberTracking_number"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                        filter_op_numberTracking_number.setFilterIfMissing(true);
                        allFilters.addFilter(filter_op_numberTracking_number);
                    } else if (sField.equals("callerNumber")) {
                        SingleColumnValueFilter filter_caller_number = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("caller_number"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
                        filter_caller_number.setFilterIfMissing(true);
                        allFilters.addFilter(filter_caller_number);
                    }
                }
            }
            return allFilters;
        } else {
            return null;
        }
    }
    private String getFilterList(String sIp,String sFilter,String sFields,String customerId, String startId,String endId) {
        String sWhere = "";
        if (!sFilter.equals("")) {
            if (sFields.equals("")) {
                sWhere = " where \"newid\" in (";
                sWhere += " select \"newid\" from \"call_log\"" +
                        " where \"newid\"<='"+endId+"'" +
                        " and \"newid\">='"+startId+"'" +
                        " and (\"info\".\"trackingSource_name\"='"+sFilter+"' " +
                        " or \"info\".\"tracking_number\"='"+sFilter+"' " +
                        " )"
                ;
                //default search op_numberTracking_sourcesName
                sWhere += ")";
            } else {
                String[] sList = sFields.split(",");
                sWhere = " where \"newid\" in (";
                sWhere += " select \"newid\" from \"call_log\"" +
                        " where \"newid\"<='"+endId+"'" +
                        " and \"newid\">='"+startId+"'";
                for (String sField : sList) {
                    if (sField.equals("OpNumber.TrackingSources.name")) {
                        if (sFilter.equals("OTHERS"))
                            sWhere += " and \"info\".\"trackingSource_id\" is null ";
                        else {
                            String trackingSource_id = getTrackingSource_id(sIp,sFilter);
                            sWhere += " and \"info\".\"trackingSource_id\"='" + trackingSource_id + "' ";
                        }
                    } else if (sField.equals("Phonebook.name")) {
                        sWhere += " and \"info\".\"phonebook_name\"='"+sFilter+"' ";
                    } else if (sField.equals("Phonebook.city")) {
                        sWhere += " and \"info\".\"phonebook_city\"='"+sFilter+"' ";
                    } else if (sField.equals("Phonebook.state")) {
                        sWhere += " and \"info\".\"phonebook_state\"='"+sFilter+"' ";
                    } else if (sField.equals("Phonebook.country")) {
                        sWhere += " and \"info\".\"phonebook_country\"='"+sFilter+"' ";
                    } else if (sField.equals("OpNumber.tracking_number")) {
                        sWhere += " and \"info\".\"tracking_number\"='"+sFilter+"' ";
                    } else if (sField.equals("callerNumber")) {
                        sWhere += " and \"info\".\"caller_number\"='"+sFilter+"' ";
                    }
                }
                sWhere += ")";
            }
        } else {
            //no filter, then only search date range
            sWhere = " where \"newid\"<='"+endId+"'" +
                    " and \"newid\">='"+startId+"'"
                    ;
        }
        if (customerId!=null && !customerId.equals("")) {
            sWhere += " and \"info\".\"customerId\"='"+customerId+"' ";;//customerId
        }
        return sWhere;
    }
    public String getTrackingSource_id(String  sIp,String sFilter) {
        String sId = "";
        try {
            Class.forName(classname_phoenix);
            java.sql.Connection conn = DriverManager.getConnection(url_phoenix);
            Statement  statement = conn.createStatement();
            String sSql  = "select \"id\" from \"tracking_sources\" where \"name\"='"+sFilter+"' ";

            ResultSet rs   = statement.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
            int iNo = 0;
            while (rs.next()) {
                sId = rs.getString("id");
                break;
            }
            // 关闭连接
            rs.close();
            statement.close();
            conn.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] sList = sId.split("_");
        if (sList.length>1)
            sId = sList[1];
        return sId;

    }

    public String getCDRsExport(
            String sFilter,String sFields,String customerId, String sStartDate,String sEndDate,String sIp) {
        Date dtStart = new Date();
        sStartDate = MysqlUtil.addSecond(sStartDate,-1);
        String sRowKeyStart = sIp+"_"+getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp+"_"+getHbaseDateStr(sStartDate);
        System.out.println("sRowKeyStart:"+sRowKeyStart);
        System.out.println("sRowKeyEnd:"+sRowKeyEnd);

        try {
            Class.forName(classname_phoenix);
            java.sql.Connection conn = DriverManager.getConnection(url_phoenix);
            Statement  statement = conn.createStatement();
            String sSql  = "select * from \"call_log\" ";
            String sWhere = getFilterList(sIp,sFilter, sFields,customerId,sRowKeyStart,sRowKeyEnd);
            sSql = sSql + sWhere
            //        + " limit " + limit + " skip " + skip
            ;
            ResultSet rs   = statement.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
            int iNo = 0;
            while (rs.next()) {
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();
//                System.out.println("newid" + rs.getString("newid"));
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    irowvalue.put(columnName,rs.getString(i));
//                    System.out.println(columnName+":" + rs.getString(i));
                }
                iresult.put(rs.getString("newid"), irowvalue);
                iNo++;
            }
            // 关闭连接
            rs.close();
            statement.close();
            conn.close();

            System.out.println("phoenix export query end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("Date,First Name,Last Name,City,State,Phone,Tracking Number,Tracking Source,Status,Duration\n");
            ListIterator<Map.Entry<String, Map<String, String>>> li = new ArrayList<Map.Entry<String, Map<String, String>>>(iresult.entrySet()).listIterator();
            while(li.hasNext()) {
                Map.Entry<String, Map<String, String>> entry = li.next();
                Map<String, String> irowvalue = entry.getValue();
                sb.append(getMapValue(irowvalue, "created", "") + ",");
                sb.append(getFirstname(getMapValue(irowvalue, "op_numberCustomerName", "")) + ",");
                sb.append(getLastname(getMapValue(irowvalue, "op_numberCustomerName", "")) + ",");
                sb.append(getMapValue(irowvalue, "phonebook_city", "") + ",");
                sb.append(getMapValue(irowvalue, "phonebook_state", "") + ",");
                sb.append(getMapValue(irowvalue, "caller_number", "") + ",");
                sb.append(getMapValue(irowvalue, "tracking_number", "") + ",");
                sb.append(getMapValue(irowvalue, "trackingSource_name", "") + ",");
                sb.append(getMapValue(irowvalue, "call_status_message", "") + ",");
                sb.append(getDuration66(irowvalue, "duration") + "");

                sb.append("\n");
            }

            System.out.println("export result end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }
    public String getCDRsExport_bak(
            String sFilter,String sFields, String sStartDate,String sEndDate,String sIp) {
        Date dtStart = new Date();
        sStartDate = MysqlUtil.addSecond(sStartDate,-1);
        String tablename = "cdr";
//        tablename = "cdr_test";
        String sRowKeyStart = sIp+"_"+getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp+"_"+getHbaseDateStr(sStartDate);
        System.out.println("sRowKeyStart:"+sRowKeyStart);
        System.out.println("sRowKeyEnd:"+sRowKeyEnd);

        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);

        Scan scan = new Scan();
        FilterList allFilters = getFilterList(sFilter,sFields);
        if (allFilters!=null) {
            scan.setFilter(allFilters);
        }
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));
//        scan.setReversed(true);

        ResultScanner ResultScannerFilterList = null;
        try {
            int iNo = 0;
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";

            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                iNo++;
                Result result = iterator.next();
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));
//                    System.out.println("get rowid:"+ rowid +
//                            " key:"+new String(Bytes.toString(CellUtil.cloneFamily(cell))) +
//                            " value:"+new String(Bytes.toString(CellUtil.cloneValue(cell)))
//                    );

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))) + ":" +
                                    new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

            table.close();
            System.out.println("hbase query end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("Date,First Name,Last Name,City,State,Phone,Tracking Number,Tracking Source,Status,Duration\n");
            ListIterator<Map.Entry<String, Map<String, String>>> li = new ArrayList<Map.Entry<String, Map<String, String>>>(iresult.entrySet()).listIterator();
            while(li.hasNext()) {
                Map.Entry<String, Map<String, String>> entry = li.next();
                Map<String, String> irowvalue = entry.getValue();
                sb.append(getMapValue(irowvalue, "info:created", "") + ",");
                sb.append(getFirstname(getMapValue(irowvalue, "info:op_numberCustomerName", "")) + ",");
                sb.append(getLastname(getMapValue(irowvalue, "info:op_numberCustomerName", "")) + ",");
                sb.append(getMapValue(irowvalue, "info:phonebookCity", "") + ",");
                sb.append(getMapValue(irowvalue, "info:phonebookState", "") + ",");
                sb.append(getMapValue(irowvalue, "info:caller_number", "") + ",");
                sb.append(getMapValue(irowvalue, "info:op_numberTracking_number", "") + ",");
                sb.append(getMapValue(irowvalue, "info:op_numberTracking_sourcesName", "") + ",");
                sb.append(getMapValue(irowvalue, "info:call_status_message", "") + ",");
                sb.append(getMapValue(irowvalue, "info:duration", "") + "");

                sb.append("\n");
            }

            System.out.println("export result end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
            return sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
        }

    }

    public void recoveryCallLog2(String sIp) {
        //get total minute and calls for dip_cus_ejt for last week
        Date dtStart = new Date();
        String tablename = "cdr";
        //208.73.232.196_20210308000000
        //208.73.232.196_79789691954100
        String sRowKeyStart = "208.73.232.196_79789691954100";
        //208.73.232.196_20210301000000
        String sRowKeyEnd = "208.73.232.196_79789698954041";
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);

        Table table = getTable(tablename);

        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));
        String sFilter = "DIP_CUS_EJT";
        FilterList allFilters = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        SingleColumnValueFilter filter_op_numberTracking_sourcesName = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("op_numberTracking_sourcesName"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
        filter_op_numberTracking_sourcesName.setFilterIfMissing(true);
        allFilters.addFilter(filter_op_numberTracking_sourcesName);
        scan.setFilter(allFilters);


        ResultScanner ResultScannerFilterList = null;
//        Connection conn = null;
//        ResultSet rs = null;
//        Statement stmt = null;
        try {

//            Class.forName(classname);
//            conn = DriverManager.getConnection(url_196);
//            stmt = conn.createStatement();

            int iCount = 0;
            int iSeconds = 0;
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";

            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                iCount++;
                Result result = iterator.next();
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))) + ":" +
                                    new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iSeconds += Integer.valueOf(getMapValue2(irowvalue, "info:duration"));
                //save to mysql
//                if (irowvalue.get("info:op_numberCustomerId2") != null) {
//                    //save to customer
//                    String sCustomer = "insert into dash.customer (id,enabled ,name " +
//                            ",vatNumber ,businessName " +
//                            ",email ,emailAccount ) " +
//                            " values ( " +
//                            getMapValue2(irowvalue, "info:op_numberCustomerId2") + "," +
//                            getMapValue2(irowvalue, "info:op_numberCustomerEnabled") + "," +
//                            getMapValue2(irowvalue, "info:op_numberCustomerName") + "," +
//                            getMapValue2(irowvalue, "info:op_numberCustomerVatNumber") + "," +
//                            getMapValue2(irowvalue, "info:op_numberCustomerBusinessName") + "," +
//                            getMapValue2(irowvalue, "info:op_numberCustomerEmail") + "," +
//                            getMapValue2(irowvalue, "info:op_numberCustomerEmailAccount") + "" +
//                            ")";
//                    try {
//                        stmt.executeUpdate(sCustomer);
//                        System.out.println("add customer"+getMapValue2(irowvalue, "info:op_numberCustomerName"));
//                    } catch (SQLException e) {
//                        System.out.println("sCustomer:"+sCustomer);
//                        e.printStackTrace();
//                    }
//                }
//                if (irowvalue.get("info:op_numberId") != null) {
//                    //save to op_number
//                    String sOpNumber = "insert into dash.op_number (id,receiving_numberId" +
//                            ",tracking_number ,tracking_sourceId " +
//                            ",phonebook_id ,notifications " +
//                            ",text_support ,number_tags ,failsafe_number " +
//                            ",renewal_date ,active ,description " +
//                            ",customerId ) " +
//                            " values ( " +
//                            getMapValue2(irowvalue, "info:op_numberId") + "," +
//                            getMapValue2(irowvalue, "info:op_numberReceiving_numberId") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_number") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourceId") + "," +
//                            getMapValue2(irowvalue, "info:op_numberPhonebook_id") + "," +
//                            getMapValue2(irowvalue, "info:op_numberNotifications") + "," +
//                            getMapValue2(irowvalue, "info:op_numberText_support") + "," +
//                            getMapValue2(irowvalue, "info:op_numberNumber_tags") + "," +
//                            getMapValue2(irowvalue, "info:op_numberFailsafe_number") + "," +
//                            getMapValue2(irowvalue, "info:op_numberRenewal_date") + "," +
//                            getMapValue2(irowvalue, "info:op_numberActive") + "," +
//                            getMapValue2(irowvalue, "info:op_numberDescription") + "," +
//                            getMapValue2(irowvalue, "info:op_numberCustomerId") + " " +
//                            ")";
//                    try {
//                        stmt.executeUpdate(sOpNumber);
//                        System.out.println("add op_number"+getMapValue2(irowvalue, "info:op_numberTracking_number"));
//                    } catch (SQLException e) {
//                        System.out.println("sOpNumber:"+sOpNumber);
//                        e.printStackTrace();
//                    }
//
//                }
//
//                if (irowvalue.get("info:phonebookId") != null) {
//                    //save to phonebook
//                    String sPhonebook = "insert into dash.phonebook (id ,name ,email" +
//                            ",street ,city ,state " +
//                            ",country ,postal_code " +
//                            ",contact_number ,note ) " +
//                            " values ( " +
//                            getMapValue2(irowvalue, "info:phonebookId") + "," +
//                            getMapValue2(irowvalue, "info:phonebookName") + "," +
//                            getMapValue2(irowvalue, "info:phonebookEmail") + "," +
//                            getMapValue2(irowvalue, "info:phonebookStreet") + "," +
//                            getMapValue2(irowvalue, "info:phonebookCity") + "," +
//                            getMapValue2(irowvalue, "info:phonebookState") + "," +
//                            getMapValue2(irowvalue, "info:phonebookCountry") + "," +
//                            getMapValue2(irowvalue, "info:phonebookPostal_code") + "," +
//                            getMapValue2(irowvalue, "info:phonebookContact_number") + "," +
//                            getMapValue2(irowvalue, "info:phonebookNote") + "" +
//                            ")";
//                    try {
//                        stmt.executeUpdate(sPhonebook);
//                        System.out.println("add phonebook"+getMapValue2(irowvalue, "info:phonebookName"));
//                    } catch (SQLException e) {
//                        System.out.println("sPhonebook:"+sPhonebook);
//
//                        e.printStackTrace();
//                    }
//                }
//
//                if (irowvalue.get("info:op_numberTracking_sourcesId") != null) {
//                    //save to tracking_sources
//                    String sTracking_sources = "insert into dash.tracking_sources (id ,name ,type " +
//                            ",position ,last_touch " +
//                            ",global_unique ,updated_at " +
//                            ",description ) " +
//                            " values ( " +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesId") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesName") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesType") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesPosition") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesLast_touch") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesGlobal_unique") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesUpdated_at") + "," +
//                            getMapValue2(irowvalue, "info:op_numberTracking_sourcesDescription") + "" +
//                            ")";
//                    try {
//                        stmt.executeUpdate(sTracking_sources);
//                        System.out.println("add tracking_sources"+getMapValue2(irowvalue, "info:op_numberTracking_sourcesName"));
//                    } catch (SQLException e) {
//                        System.out.println("sTracking_sources:"+sTracking_sources);
//                        e.printStackTrace();
//                    }
//                }
//                //save to call log
//                String sCallLog = "insert into dash.call_log (id,call_id,call_direction,caller_number,caller_contactId,opnumberId ,session_data" +
//                        ",callrecordingId,metrics,routingId,flag,call_terminated,call_status,call_status_message" +
//                        ",duration ,ms_duration,setuptime ,created ) " +
//                        " values ( " +
//                        getMapValue2(irowvalue, "info:id") + "," +
//                        getMapValue2(irowvalue, "info:call_id") + "," +
//                        getMapValue2(irowvalue, "info:call_direction") + "," +
//                        getMapValue2(irowvalue, "info:caller_number") + "," +
//                        getMapValue2(irowvalue, "info:caller_contactId") + "," +
//                        getMapValue2(irowvalue, "info:opnumberId") + "," +
//                        getMapValue2(irowvalue, "info:session_data") + "," +
//                        getMapValue2(irowvalue, "info:callrecordingId") + "," +
//                        getMapValue2(irowvalue, "info:metrics") + "," +
//                        getMapValue2(irowvalue, "info:routingId") + "," +
//                        getMapValue2(irowvalue, "info:flag") + "," +
//                        getMapValue2(irowvalue, "info:call_terminated") + "," +
//                        getMapValue2(irowvalue, "info:call_status") + "," +
//                        getMapValue2(irowvalue, "info:call_status_message") + "," +
//                        getMapValue2(irowvalue, "info:duration") + "," +
//                        getMapValue2(irowvalue, "info:ms_duration") + "," +
//                        getMapValue2(irowvalue, "info:setuptime") + "," +
//                        getMapValue2(irowvalue, "info:created") + "" +
//                        ")";
//                try {
//                    stmt.executeUpdate(sCallLog);
//                    System.out.println("add call"+getMapValue2(irowvalue, "info:id"));
//                } catch (SQLException e) {
//                    System.out.println("sCallLog:"+sCallLog);
//                    e.printStackTrace();
//                }
//                //iresult.put(rowid, irowvalue);
//                //for test
////                if (iNo > 100) {
////                    break;
////                }
            }

            table.close();
            System.out.println("hbase query end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("export result end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
            System.out.println("minutes:" + String.valueOf(iSeconds/60.0));
//        } catch (SQLException e) {
//            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ResultScannerFilterList != null) {
                ResultScannerFilterList.close();
            }
//            try {
//                if (rs != null)
//                    rs.close();
//                if (conn != null)
//                    conn.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            } finally {
//
//            }
        }
    }
    public void recoveryCallLog(String sIp) {
        //get total minute and calls for dip_cus_ejt for last week
        Date dtStart = new Date();
        String tablename = "cdr";
        //208.73.232.196_20210308000000
        //208.73.232.196_79789691954100
        String sRowKeyStart = "208.73.232.196_79789691954100";
        //208.73.232.196_20210301000000
        String sRowKeyEnd = "208.73.232.196_79789698954041";
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);
        Table table = getTable(tablename);
        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));
        String sFilter = "DIP_CUS_ETI";
        FilterList allFilters = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        SingleColumnValueFilter filter_op_numberTracking_sourcesName = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("op_numberTracking_sourcesName"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
        filter_op_numberTracking_sourcesName.setFilterIfMissing(true);
        allFilters.addFilter(filter_op_numberTracking_sourcesName);
        scan.setFilter(allFilters);

        ResultScanner ResultScannerFilterList = null;
        try {
            int iCount = 0;
            int iSeconds = 0;
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";

            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                Result result = iterator.next();
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))) + ":" +
                                    new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iCount++;
                iSeconds += Integer.valueOf(getMapValue(irowvalue, "info:duration"));
            }

            table.close();
            System.out.println("hbase query end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("export result end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
            System.out.println("minutes:" + String.valueOf(iSeconds/60.0));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ResultScannerFilterList != null) {
                ResultScannerFilterList.close();
            }
        }
    }
    public void re_create_report(String sIp) {
        //activity1
        create_activity_report1(sIp);
        create_activity_report2(sIp);
    }

    public void create_activity_report(String sIp,String start_id,String end_id) {
        //activity1
        create_activity_report1(sIp,start_id,end_id);
        create_activity_report2(sIp,start_id,end_id);
    }
    public void create_activity_report1(String sIp) {
        //activity1
        Date dtStart = new Date();
        String sSql = " select count(\"newid\") as \"total_calls\",to_char(sum(to_number(\"duration\"))) as \"total_time\"," +
                " \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10) as \"created1\",\"trackingSource_id\",\"trackingSource_name\" " +
                " from \"call_log\" " +
                " where \"hostname\"='"+sIp+"' " +
                " group by \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10),\"trackingSource_id\",\"trackingSource_name\" "
                ;
        System.out.println("create_activity_report1:" + sSql);

        java.sql.Connection conn_mysql = null;
        ResultSet rs = null;
        Statement stmt = null;
        PreparedStatement ps_mysql=null;
        try {
            int iCount = 0;
            Properties props = new Properties();
            props.setProperty("phoenix.query.timeoutMs", "1200000");
            props.setProperty("hbase.rpc.timeout", "1200000");
            props.setProperty("hbase.client.scanner.timeout.period", "1200000");

            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix, props);

            Class.forName(classname);
            conn_mysql = DriverManager.getConnection(url_mysql_local);


            stmt = phoenixConn.createStatement();

            rs = stmt.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
//                JSONObject jsonObj = new JSONObject();
//                for (int i = 1; i <= data.getColumnCount(); i++) {
//                    String columnName = data.getColumnLabel(i);
//                    jsonObj.put(columnName,rs.getString(i));
////                    System.out.println(columnName+":" + rs.getString(i));
//                }
//                //add to activity1
//                //add rpt_call_log_activity1
//                String sql="insert into rpt_call_log_activity1 ( " +
//                        "id  ," +
//                        "   call_date ," +
//                        "   hostname    ," +
//                        "   customerid ," +
//                        "   cid ," +
//                        "   name ," +
//                        "   total_calls ," +
//                        "   total_time " +
//
//                        "   ) values(" +
//                        "?,?,?,?,?,?,"+getJsonNumber(jsonObj,"total_calls")+
//                        "," + getJsonNumber(jsonObj,"total_time") +
//                        ")  ON DUPLICATE KEY UPDATE total_calls = total_calls + "+getJsonNumber(jsonObj,"total_calls")+", " +
//                        " total_time = total_time + " + getJsonNumber(jsonObj,"total_time")
//                ;  //8
//                ps_mysql=conn_mysql.prepareStatement(sql);
//                int iField = 1;
//                String sValue = sIp
//                        + "**" + getJsonValue(jsonObj,"created1")
//                        + "**" + getJsonValue(jsonObj,"trackingSource_id")
//                        + "**" + getJsonValue(jsonObj,"customerId")
//                        ;
//                ps_mysql.setString(iField,sValue);
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"created1"));
//                iField++;ps_mysql.setString(iField,sIp);
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"customerId","0"));//5
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_id"));
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_name"));
//                int i=ps_mysql.executeUpdate();
//                iCount++;
            }

            System.out.println("create_activity_report1:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
            if (rs!=null){
                rs.close();
            }
            if (stmt!=null){
                stmt.close();
            }
            if (phoenixConn!=null){
                phoenixConn.close();
            }

            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn_mysql!=null){
                conn_mysql.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void create_activity_report2(String sIp) {
        //activity1
        Date dtStart = new Date();
        String sSql = " select count(\"newid\") as \"total_calls\",to_char(sum(to_number(\"duration\"))) as \"total_time\"," +
                " \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10) as \"created1\",\"trackingSource_id\",\"trackingSource_name\" ,\"tracking_number\"" +
                " from \"call_log\" " +
                " where \"hostname\"='"+sIp+"' " +
                " group by \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10),\"trackingSource_id\",\"trackingSource_name\" ,\"tracking_number\""
                ;
        System.out.println("create_activity_report2:" + sSql);

        java.sql.Connection conn_mysql = null;
        ResultSet rs = null;
        Statement stmt = null;
        PreparedStatement ps_mysql=null;
        try {
            int iCount = 0;
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);

            Class.forName(classname);
            conn_mysql = DriverManager.getConnection(url_mysql_local);


            stmt = phoenixConn.createStatement();

            rs = stmt.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
//                JSONObject jsonObj = new JSONObject();
//                for (int i = 1; i <= data.getColumnCount(); i++) {
//                    String columnName = data.getColumnLabel(i);
//                    jsonObj.put(columnName,rs.getString(i));
////                    System.out.println(columnName+":" + rs.getString(i));
//                }
//                //add to activity1
//                //add rpt_call_log_activity1
//                String sql="insert into rpt_call_log_activity2 ( " +
//                        "id  ," +
//                        "   call_date ," +
//                        "   hostname    ," +
//                        "   customerid ," +
//                        "   cid ," +
//                        "   name ," +
//                        "   contact_number ," +
//                        "   total_calls ," +
//                        "   total_time " +
//
//                        "   ) values(" +
//                        "?,?,?,?,?,?,?,"+getJsonNumber(jsonObj,"total_calls")+
//                        "," + getJsonNumber(jsonObj,"total_time") +
//                        ")  ON DUPLICATE KEY UPDATE total_calls = total_calls + "+getJsonNumber(jsonObj,"total_calls")+", " +
//                        " total_time = total_time + " + getJsonNumber(jsonObj,"total_time")
//                        ;  //8
//                ps_mysql=conn_mysql.prepareStatement(sql);
//                int iField = 1;
//                String sValue = sIp
//                        + "**" + getJsonValue(jsonObj,"customerId")
//                        + "**" + getJsonValue(jsonObj,"created1")
//                        + "**" + getJsonValue(jsonObj,"trackingSource_name")
//                        + "**" + getJsonValue(jsonObj,"trackingSource_id")
//                        + "**" + getJsonValue(jsonObj,"tracking_number")
//                        ;
//                ps_mysql.setString(iField,sValue);
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"created1","0"));//5
//                iField++;ps_mysql.setString(iField,sIp);
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"customerId","0"));//5
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_id"));
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_name"));
//                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"tracking_number"));
//                int i=ps_mysql.executeUpdate();
//                iCount++;
            }

            System.out.println("create_activity_report2:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
            if (rs!=null){
                rs.close();
            }
            if (stmt!=null){
                stmt.close();
            }
            if (phoenixConn!=null){
                phoenixConn.close();
            }

            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn_mysql!=null){
                conn_mysql.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void create_activity_report1(String sIp,String start_id,String end_id) {
        //activity1
        Date dtStart = new Date();
        String sSql = " select count(\"newid\") as \"total_calls\",to_char(sum(to_number(\"duration\"))) as \"total_time\"," +
                " \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10) as \"created1\",\"trackingSource_id\",\"trackingSource_name\" " +
                " from \"call_log\" " +
                " where \"newid\">='"+start_id+"' " +
                " and \"newid\"<='"+end_id+"' " +
                " group by \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10),\"trackingSource_id\",\"trackingSource_name\" "
                ;
        System.out.println("create_activity_report1:" + sSql);

        java.sql.Connection conn_mysql = null;
        ResultSet rs = null;
        Statement stmt = null;
        PreparedStatement ps_mysql=null;
        try {
            int iCount = 0;
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);

            Class.forName(classname);
            conn_mysql = DriverManager.getConnection(url_mysql_local);


            stmt = phoenixConn.createStatement();

            rs = stmt.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                JSONObject jsonObj = new JSONObject();
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
//                    System.out.println(columnName+":" + rs.getString(i));
                }
                //add to activity1
                //add rpt_call_log_activity1
                String sql="insert into rpt_call_log_activity1 ( " +
                        "id  ," +
                        "   call_date ," +
                        "   hostname    ," +
                        "   customerid ," +
                        "   cid ," +
                        "   name ," +
                        "   total_calls ," +
                        "   total_time " +

                        "   ) values(" +
                        "?,?,?,?,?,?,"+getJsonNumber(jsonObj,"total_calls")+
                        "," + getJsonNumber(jsonObj,"total_time") +
                        ")  ON DUPLICATE KEY UPDATE total_calls = total_calls + "+getJsonNumber(jsonObj,"total_calls")+", " +
                        " total_time = total_time + " + getJsonNumber(jsonObj,"total_time")
                        ;  //8
                ps_mysql=conn_mysql.prepareStatement(sql);
                int iField = 1;
                String sValue = sIp
                        + "**" + getJsonValue(jsonObj,"created1")
                        + "**" + getJsonValue(jsonObj,"trackingSource_id")
                        + "**" + getJsonValue(jsonObj,"customerId")
                        ;
                ps_mysql.setString(iField,sValue);
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"created1"));
                iField++;ps_mysql.setString(iField,sIp);
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"customerId","0"));//5
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_id"));
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_name"));
                int i=ps_mysql.executeUpdate();
                iCount++;
            }

            System.out.println("create_activity_report1:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
            if (rs!=null){
                rs.close();
            }
            if (stmt!=null){
                stmt.close();
            }
            if (phoenixConn!=null){
                phoenixConn.close();
            }

            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn_mysql!=null){
                conn_mysql.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void create_activity_report2(String sIp,String start_id,String end_id) {
        //activity1
        Date dtStart = new Date();
        String sSql = " select count(\"newid\") as \"total_calls\",to_char(sum(to_number(\"duration\"))) as \"total_time\"," +
                " \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10) as \"created1\",\"trackingSource_id\",\"trackingSource_name\" ,\"tracking_number\"" +
                " from \"call_log\" " +
                " where \"newid\">='"+start_id+"' " +
                " and \"newid\"<='"+end_id+"' " +
                " group by \"hostname\",\"customerId\",substr(REGEXP_REPLACE(\"created\", '[-,T,:]', ''),0,10),\"trackingSource_id\",\"trackingSource_name\" ,\"tracking_number\""
                ;
        System.out.println("create_activity_report2:" + sSql);

        java.sql.Connection conn_mysql = null;
        ResultSet rs = null;
        Statement stmt = null;
        PreparedStatement ps_mysql=null;
        try {
            int iCount = 0;
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);

            Class.forName(classname);
            conn_mysql = DriverManager.getConnection(url_mysql_local);


            stmt = phoenixConn.createStatement();

            rs = stmt.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                JSONObject jsonObj = new JSONObject();
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
//                    System.out.println(columnName+":" + rs.getString(i));
                }
                //add to activity1
                //add rpt_call_log_activity1
                String sql="insert into rpt_call_log_activity2 ( " +
                        "id  ," +
                        "   call_date ," +
                        "   hostname    ," +
                        "   customerid ," +
                        "   cid ," +
                        "   name ," +
                        "   contact_number ," +
                        "   total_calls ," +
                        "   total_time " +

                        "   ) values(" +
                        "?,?,?,?,?,?,?,"+getJsonNumber(jsonObj,"total_calls")+
                        "," + getJsonNumber(jsonObj,"total_time") +
                        ")  ON DUPLICATE KEY UPDATE total_calls = total_calls + "+getJsonNumber(jsonObj,"total_calls")+", " +
                        " total_time = total_time + " + getJsonNumber(jsonObj,"total_time")
                        ;  //8
                ps_mysql=conn_mysql.prepareStatement(sql);
                int iField = 1;
                String sValue = sIp
                        + "**" + getJsonValue(jsonObj,"customerId")
                        + "**" + getJsonValue(jsonObj,"created1")
                        + "**" + getJsonValue(jsonObj,"trackingSource_name")
                        + "**" + getJsonValue(jsonObj,"trackingSource_id")
                        + "**" + getJsonValue(jsonObj,"tracking_number")
                        ;
                ps_mysql.setString(iField,sValue);
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"created1","0"));//5
                iField++;ps_mysql.setString(iField,sIp);
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"customerId","0"));//5
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_id"));
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"trackingSource_name"));
                iField++;ps_mysql.setString(iField,getJsonValue(jsonObj,"tracking_number"));
                int i=ps_mysql.executeUpdate();
                iCount++;
            }

            System.out.println("create_activity_report2:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
            if (rs!=null){
                rs.close();
            }
            if (stmt!=null){
                stmt.close();
            }
            if (phoenixConn!=null){
                phoenixConn.close();
            }

            if (ps_mysql!=null){
                ps_mysql.close();
            }
            if (conn_mysql!=null){
                conn_mysql.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void call_addFromMysql(String sIp,String start_id,String end_id) {
        //get total minute and calls for dip_cus_ejt for last week
        //just add by hand

        Date dtStart = new Date();
        String sSql = "SELECT  CONCAT(  " +
                "        '208.73.232.196',  " +
                "        '_',  " +
                "        100000000000000 - CONVERT(DATE_FORMAT(a.created, '%Y%m%d%H%i%s'), SIGNED),  " +
                "        '_',  " +
                "        a.id  " +
                "       ) AS `newid`,  " +
                "       '208.73.232.196' AS hostname,  " +
                "       a.id,  " +
                "       a.call_id,  " +
                "       a.call_direction,  " +
                "       a.caller_number,  " +
                "       a.tracking_number,  " +
                "       a.customerId,  " +
                "       a.session_data,  " +
                "       a.metrics,  " +
                "       a.flag,  " +
                "       a.call_terminated,  " +
                "       a.call_status,  " +
                "       a.call_status_message,  " +
                "       a.duration,  " +
                "       a.ms_duration,  " +
                "       a.setuptime,  " +
                "       a.created,  " +
                "       b.id AS phonebook_id,  " +
                "       b.name AS phonebook_name,  " +
                "       b.email AS phonebook_email,  " +
                "       b.street AS phonebook_street,  " +
                "       b.city AS phonebook_city,  " +
                "       b.state AS phonebook_state,  " +
                "       b.country AS phonebook_country,  " +
                "       b.postal_code AS phonebook_postalCode,  " +
                "       b.contact_number AS phonebook_contactNumber,  " +
                "       b.note AS phonebook_note,  " +
                "       c.id AS callRecording_id,  " +
                "       c.name AS callRecording_name,  " +
                "       c.timestamp AS callRecording_timestamp,  " +
                "       c.content AS callRecording_content,  " +
                "       c.duration AS callRecording_duration,  " +
                "       c.tags AS callRecording_tages,  " +
                "       c.visible AS callRecording_visible,  " +
                "       d.id AS opNumber_id,  " +
                "       d.tracking_number AS opNumber_trackingNumber,  " +
                "       d.routing_action AS opNumber_routingAction,  " +
                "       d.notifications AS opNumber_notifications,  " +
                "       d.text_support AS opNumber_textSupport,  " +
                "       d.number_tags AS opNumber_numberTags,  " +
                "       d.failsafe_number AS opNumber_failsafeNumber,  " +
                "       d.renewal_date AS opNumber_renewalDate,  " +
                "       d.active AS opNumber_active,  " +
                "       d.description AS opNumber_description,  " +
                "       e.id AS trackingSource_id,  " +
                "       e.name AS trackingSource_name,  " +
                "       e.type AS trackingSource_type,  " +
                "       e.position AS trackingSource_position,  " +
                "       e.last_touch AS trackingSource_lastTouch,  " +
                "       e.global_unique AS trackingSource_globalUnique,  " +
                "       e.updated_at AS trackingSource_updatedAt,  " +
                "       e.description AS trackingSource_description,  " +
                "       f.id AS receivingNumber_id,  " +
                "       f.number AS receivingNumber_number,  " +
                "       f.description AS receivingNumber_description,  " +
                "       g.id AS sipGateway_id,  " +
                "       g.name AS sipGateway_name,  " +
                "       g.address AS sipGateway_address,  " +
                "       g.port AS sipGateway_port,  " +
                "       g.digits_strip AS sipGateway_digitsStrip,  " +
                "       g.description AS sipGateway_description  " +
                "     FROM dash.call_log a  " +
                "     LEFT JOIN dash.phonebook b ON a.caller_contactId = b.id  " +
                "     LEFT JOIN dash.call_recording c ON a.callrecordingId = c.id  " +
                "     LEFT JOIN dash.op_number d ON a.opnumberId = d.id  " +
                "     LEFT JOIN dash.tracking_sources e ON d.tracking_sourceId = e.id  " +
                "     LEFT JOIN dash.receiving_number f ON d.receiving_numberId = f.id  " +
                "     LEFT JOIN dash.sip_gateways g ON d.sip_gatewayId = g.id  " +
                " where a.id>= " + start_id + " and a.id<=" + end_id +
                " order by a.id desc  "
                ;

                java.sql.Connection conn = null;
        ResultSet rs = null;
        Statement stmt = null;
        try {
            int iCount = 0;
            Class.forName(classname_phoenix);
            phoenixConn = DriverManager.getConnection(url_phoenix);

            Class.forName(classname);

//            conn = DriverManager.getConnection(getMysqlUrl(sIp));
            conn = DriverManager.getConnection(url_196);
            stmt = conn.createStatement();

            rs = stmt.executeQuery(sSql);
            ResultSetMetaData data = rs.getMetaData();
            while(rs.next()) {
                JSONObject jsonObj = new JSONObject();
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    String columnName = data.getColumnLabel(i);
                    jsonObj.put(columnName,rs.getString(i));
//                    System.out.println(columnName+":" + rs.getString(i));
                }
                String rowKey = getCallLogKey(sIp, jsonObj);
                String hostname = getJsonValue(jsonObj,"hostname");
                addCDRsToPhoenix(rowKey,hostname,jsonObj);
                iCount++;
                System.out.println("call_addFromMysql:" + rowKey
                        + " iCount :" + String.valueOf(iCount));
            }

            System.out.println("call_addFromMysql:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
            if (rs!=null){
                rs.close();
            }
            if (stmt!=null){
                stmt.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String testPhoenix(String sIp) {
        java.sql.Connection connection;
        PreparedStatement ps;
        try {
            Class.forName(classname_phoenix);
            connection = DriverManager.getConnection(url_phoenix);
            String sql="select count(*) as cc from \"call_log\"";
            ps=connection.prepareStatement(sql);

            ResultSet resultSet = ps.executeQuery();
            String sss = "";
            while (resultSet.next()){
                sss = "count："+resultSet.getInt("cc");
                System.out.print(sss);
            }

            if (resultSet!=null){
                resultSet.close();
            }
            if (ps!=null){
                ps.close();
            }
            if (connection!=null){
                connection.close();
            }
            return sss;
        }
        catch (Exception e) {
            System.out.println("Exception:"+e.toString());
            return "";
        }
    }
    public void update_caller_number() {
        //update caller number to 10 digits
        Date dtStart = new Date();
        try {
            Class.forName(classname_phoenix);
            java.sql.Connection conn = DriverManager.getConnection(url_phoenix);
            Statement  statement = conn.createStatement();
            String sSqlCount  = "select count(*) as ccc from \"call_log\" where length(\"caller_number\")>10";

            String sSql = "upsert into \"call_log\" (\"newid\",\"caller_number\") " +
                    "select \"newid\",SUBSTR(\"caller_number\",length(\"caller_number\")-10,10) " +
                    "from \"call_log\" " +
                    "where length(\"caller_number\")>10  " +
                    "limit 10000 "
                    ;
            ResultSet rs = null;
            int iNo = 1;
            while (iNo>0) {
                PreparedStatement ps=conn.prepareStatement(sSql);
                ps.executeUpdate();
                conn.commit();
                if (ps!=null){
                    ps.close();
                }
                System.out.println("update_caller_number left count:"+ String.valueOf(iNo) + "  take " + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");

                rs = statement.executeQuery(sSqlCount);
                while (rs.next()) {
                    iNo = rs.getInt(1);
                }
            }

            // 关闭连接
            rs.close();
            statement.close();
            conn.close();
            System.out.println("update_caller_number end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCallLogs20210319() {
        //get total minute and calls for dip_cus_ejt for last week
        Date dtStart = new Date();
        String tablename = "cdr";
        //sRowKeyStart
        //208.73.232.196_20250308000000
        //208.73.232.196_79749692000000
        String sRowKeyStart = "208.73.232.196_79749692000000";
        //sRowKeyEnd
        //208.73.232.196_20000301000000
        //208.73.232.196_79999699000000
        String sRowKeyEnd = "208.73.232.196_79999699000000";
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);
        Table table = getTable(tablename);
        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));

        ResultScanner ResultScannerFilterList = null;
        try {
            int iCount = 0;
            int iSeconds = 0;
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";

            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                Result result = iterator.next();
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();

                CellScanner cellScanner = result.cellScanner();
                boolean bAdd = true;
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();
                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));
                    String sFamily = new String(Bytes.toString(CellUtil.cloneFamily(cell)));
                    String sQualifier = new String(Bytes.toString(CellUtil.cloneQualifier(cell)));
                    String sValue = new String(Bytes.toString(CellUtil.cloneValue(cell)));
                    if (sFamily.equals("info") && sQualifier.equals("callId") && (sValue!= null)) {
                        bAdd = false;
                        break;
                    }
//                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))) + ":" +
//                                    new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
//                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                if (bAdd) {
                    Put put = new Put(Bytes.toBytes(rowid));
                    if (irowvalue.get("call_direction")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("callDirection"),
                                Bytes.toBytes(irowvalue.get("call_direction")));
//                        System.out.println("call_direction:" + irowvalue.get("call_direction") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("call_id")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("callId"),
                                Bytes.toBytes(irowvalue.get("call_id")));
//                        System.out.println("call_id:" + irowvalue.get("call_id") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("call_status")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("callStatus"),
                                Bytes.toBytes(irowvalue.get("call_status")));
//                        System.out.println("call_status:" + irowvalue.get("call_status") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("call_status_message")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("callStatusMessage"),
                                Bytes.toBytes(irowvalue.get("call_status_message")));
//                        System.out.println("call_status_message:" + irowvalue.get("call_status_message") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("call_terminated")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("callTerminated"),
                                Bytes.toBytes(irowvalue.get("call_terminated")));
//                        System.out.println("call_terminated:" + irowvalue.get("call_terminated") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("caller_number")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("callerNumber"),
                                Bytes.toBytes(irowvalue.get("caller_number")));
//                        System.out.println("caller_number:" + irowvalue.get("caller_number") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("op_numberCustomerId")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("customerId"),
                                Bytes.toBytes(irowvalue.get("op_numberCustomerId")));
//                        System.out.println("op_numberCustomerId:" + irowvalue.get("op_numberCustomerId") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("ms_duration")!=null) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("msDuration"),
                                Bytes.toBytes(irowvalue.get("ms_duration")));
//                        System.out.println("ms_duration:" + irowvalue.get("ms_duration") + " cdr key:" + rowid);
                    }
                    if (irowvalue.get("op_numberTracking_number")!=null && irowvalue.get("trackingNumber")==null ) {
                        put.addColumn(Bytes.toBytes("info"),
                                Bytes.toBytes("trackingNumber"),
                                Bytes.toBytes(irowvalue.get("op_numberTracking_number")));
//                        System.out.println("ms_duration:" + irowvalue.get("ms_duration") + " cdr key:" + rowid);
                    }
                    iCount++;
                    System.out.println("updating call log:" + String.valueOf(iCount) + " cdr key:" + rowid);
                    table.put(put);
                }
            }

            table.close();
            System.out.println("update call log 20210319 end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            System.out.println("count:" + iCount);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ResultScannerFilterList != null) {
                ResultScannerFilterList.close();
            }
        }
    }

    public String getCDRsExportTest(
            int hour,String sFilter,String sFields,String sInterval, int offset, String sStartDate,String sEndDate,String sIp) {
        Date dtStart = new Date();
        sStartDate = MysqlUtil.addSecond(sStartDate,-60);
        String tablename = "cdr";
//        tablename = "cdr_test";
        String sRowKeyStart = sIp + "_" + getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp + "_" + getHbaseDateStr(sStartDate);
        String sHour = String.valueOf (100-hour-1);
        String sFilterHour = sIp + "_\\d{8}"+ sHour +"\\d{4}_\\d+";
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);
        System.out.println("sFilterHour:" + sFilterHour);
        System.out.println("sFilter:" + sFilter);

        Map<String, Map<String, String>> iresult = new TreeMap<String, Map<String, String>>();
        Table table = getTable(tablename);

        Scan scan = new Scan();
        FilterList allFilters = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        if (sInterval.equals("1")) {
            System.out.println("sInterval:" + sInterval);
            //hour filter
            RowFilter filter_hour = new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(sFilterHour));
            allFilters.addFilter(filter_hour);
            //Tracking_sourcesName filter
            SingleColumnValueFilter filter_op_numberTracking_sourcesName = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("op_numberTracking_sourcesName"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
            filter_op_numberTracking_sourcesName.setFilterIfMissing(true);
            allFilters.addFilter(filter_op_numberTracking_sourcesName);
        }
        if (allFilters!=null) {
            scan.setFilter(allFilters);
        }
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));
//        scan.setReversed(true);

        ResultScanner ResultScannerFilterList = null;
        try {
            int iNo = 0;
            ResultScannerFilterList = table.getScanner(scan);
            String rowid = "";

            Iterator<Result> iterator = ResultScannerFilterList.iterator();
            while (iterator.hasNext()) {
                iNo++;
                Result result = iterator.next();
                rowid = "";
                Map<String, String> irowvalue = new TreeMap<String, String>();
                irowvalue.clear();

                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell cell = cellScanner.current();

                    if (rowid == "")
                        rowid = Bytes.toString(CellUtil.copyRow(cell));
//                    System.out.println("get rowid:"+ rowid +
//                            " key:"+new String(Bytes.toString(CellUtil.cloneFamily(cell))) +
//                            " value:"+new String(Bytes.toString(CellUtil.cloneValue(cell)))
//                    );

                    irowvalue.put(new String(Bytes.toString(CellUtil.cloneFamily(cell))) + ":" +
                                    new String(Bytes.toString(CellUtil.cloneQualifier(cell))),
                            new String(Bytes.toString(CellUtil.cloneValue(cell))));
                }
                iresult.put(rowid, irowvalue);
            }

            table.close();
            System.out.println("hbase query end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("Date,Tracking Source,Status,Duration\n");
            ListIterator<Map.Entry<String, Map<String, String>>> li = new ArrayList<Map.Entry<String, Map<String, String>>>(iresult.entrySet()).listIterator();
            while(li.hasNext()) {
                Map.Entry<String, Map<String, String>> entry = li.next();
                Map<String, String> irowvalue = entry.getValue();
                sb.append(getMapValue(irowvalue, "info:created", "") + ",");
                sb.append(getMapValue(irowvalue, "info:op_numberTracking_sourcesName", "") + ",");
                sb.append(getMapValue(irowvalue, "info:call_status_message", "") + ",");
                sb.append(getMapValue(irowvalue, "info:duration", "") + "");

                sb.append("\n");
            }

            System.out.println("export result end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
            return sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
        }

    }

    public String getCDRsCount(
            String sFilter,String sFields,String customerId, String sStartDate,String sEndDate,String sIp) {
        Date dtStart = new Date();
        sStartDate = MysqlUtil.addSecond(sStartDate, -1);
        String sRowKeyStart = sIp + "_" + getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp + "_" + getHbaseDateStr(sStartDate);
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);

        try {
            Class.forName(classname_phoenix);
            java.sql.Connection conn = DriverManager.getConnection(url_phoenix);
            Statement  statement = conn.createStatement();
            String sSql  = "select count(*) as ccc from \"call_log\" ";
            String sWhere = getFilterList(sIp,sFilter, sFields,customerId,sRowKeyStart,sRowKeyEnd);
            sSql = sSql + sWhere;
            ResultSet rs   = statement.executeQuery(sSql);
            int iNo = 0;
            while (rs.next()) {
                iNo = rs.getInt(1);
            }
            // 关闭连接
            rs.close();
            statement.close();
            conn.close();
            System.out.println("hbase query count end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
            //export
            StringBuilder sb = new StringBuilder();
            //output format
            sb.append("{\n");
            sb.append("\"total_count\":"+String.valueOf(iNo)+"\n");
            sb.append("}\n");
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    public String getCDRsCount_bak(
            String sFilter,String sFields, String sStartDate,String sEndDate,String sIp) {
        Date dtStart = new Date();
        sStartDate = MysqlUtil.addSecond(sStartDate, -1);
        String tablename = "cdr";
//        tablename = "cdr_test";
        String sRowKeyStart = sIp + "_" + getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp + "_" + getHbaseDateStr(sStartDate);
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);

        FilterList allFilters = getFilterList(sFilter,sFields);
        int iCount = getCount(tablename, sRowKeyStart, sRowKeyEnd, allFilters);
        System.out.println("hbase query count end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
        //export
        StringBuilder sb = new StringBuilder();
        //output format
        sb.append("{\n");
        sb.append("\"total_count\":"+String.valueOf(iCount)+"\n");
        sb.append("}\n");
        return sb.toString();
    }
    public String getCDRsCountTest(
            int hour,String sFilter,String sFields,String sInterval, int offset, String sStartDate,String sEndDate,String sIp) {
        System.out.println("enter getCDRsCountTest");
        Date dtStart = new Date();
//        sStartDate = MysqlUtil.addSecond(sStartDate, -1);
        sStartDate = MysqlUtil.addSecond(sStartDate, -60);
        String tablename = "cdr";
//        tablename = "cdr_test";
        String sRowKeyStart = sIp + "_" + getHbaseDateStr(sEndDate);
        String sRowKeyEnd = sIp + "_" + getHbaseDateStr(sStartDate);
        String sHour = String.valueOf (100-hour-1);
        String sFilterHour = sIp + "_\\d{8}"+ sHour +"\\d{4}_\\d+";
        System.out.println("sRowKeyStart:" + sRowKeyStart);
        System.out.println("sRowKeyEnd:" + sRowKeyEnd);
        System.out.println("sFilterHour:" + sFilterHour);
        System.out.println("sFilter:" + sFilter);

        FilterList allFilters = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        if (sInterval.equals("1")) {
            System.out.println("sInterval:" + sInterval);
            //hour filter
            if (hour!=-1) {
                RowFilter filter_hour = new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator(sFilterHour));
                allFilters.addFilter(filter_hour);
            }
            //Tracking_sourcesName filter
            SingleColumnValueFilter filter_op_numberTracking_sourcesName = new SingleColumnValueFilter(Bytes.toBytes("info"), Bytes.toBytes("op_numberTracking_sourcesName"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(sFilter));
            filter_op_numberTracking_sourcesName.setFilterIfMissing(true);
            allFilters.addFilter(filter_op_numberTracking_sourcesName);
        }

        int iNo = 0;
        Table table = getTable(tablename);
        Scan scan = new Scan();
        if (allFilters!=null) {
//            allFilters.addFilter(new FirstKeyOnlyFilter());
            scan.setFilter(allFilters);
        } else {
            scan.setFilter(new FirstKeyOnlyFilter());
        }
        scan.setStartRow(Bytes.toBytes(sRowKeyStart));
        scan.setStopRow(Bytes.toBytes(sRowKeyEnd));
        scan.setCaching(100000);
        scan.setCacheBlocks(false);

        ResultScanner ResultScannerFilterList = null;
        try {
            System.out.println("start filter");
            ResultScannerFilterList = table.getScanner(scan);
            for (Result rs = ResultScannerFilterList.next(); rs != null; rs = ResultScannerFilterList.next()) {
                iNo++;
            }
            table.close();
            System.out.println("hbase query end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
            System.out.println("count:"+String.valueOf(iNo));

        } catch (Exception e) {
            System.out.println("IOException");
            e.printStackTrace();
        } finally {
            if (ResultScannerFilterList != null)
                ResultScannerFilterList.close();
        }
        int iCount = iNo;
        System.out.println("hbase query count end:" + String.valueOf((int) ((new Date()).getTime() - dtStart.getTime())) + " mil seconds");
        //export
        StringBuilder sb = new StringBuilder();
        //output format
        sb.append("{\n");
        sb.append("\"total_count\":"+String.valueOf(iCount)+"\n");
        sb.append("}\n");
        System.out.println("return");
        return sb.toString();
    }

    private String getCallLogActivityReport(int starttime, int endtime, int offset, int interval, String view_by, String ipAddress) {
        int starttimeOrig = starttime;
        int endtimeOrig = endtime;
        MysqlUtil mysqlUtil = new MysqlUtil();
        if (offset != 0) {
            starttime = MysqlUtil.addHour(starttime, offset);
            endtime = MysqlUtil.addHour(endtime, offset);
        }
        List<String> dates = new ArrayList<String>();
        if (view_by.equals("1")) {

        } else if (interval == 1) {
            //hour
            for (int i = 0; i < 24; i++) {
                dates.add(MysqlUtil.pad(i, 2));
            }
        } else if (interval == 2) {
            //week
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;

            while (currtime <= e1) {
                String mondayStr = MysqlUtil.getMondayStr(currtime, 0);
                dates.add(mondayStr);
                currtime = MysqlUtil.addDays(currtime, 7);
                if (currtime>e1) {
                    String mondayStr2 = MysqlUtil.getMondayStr(currtime,0);
                    if (!mondayStr.equals(mondayStr2)) {
                        dates.add(mondayStr2);
                    }
                }
            }
        } else if (interval == 3) {
            //month
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;
            while (currtime <= e1) {
                String monthStr = MysqlUtil.getMonthStr(currtime, 0);
                dates.add(monthStr);
                currtime = MysqlUtil.addMonths(currtime, 1);
//                if (currtime>e1) {
//                    monthStr = getMonthStr(currtime,0);
//                    dates.add(monthStr);
//                }
            }
        } else {
            //days report
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;
            while (currtime <= e1) {
                String dayStr = MysqlUtil.getDayStr(currtime, 0);
                dates.add(dayStr);
                currtime = MysqlUtil.addDays(currtime, 1);
            }
        }
        //init series_data
        Map<String, Integer> series_data = new TreeMap<String, Integer>();
        Map<String, Map<String, Integer>> series_data_name = new TreeMap<String, Map<String, Integer>>();
        Map<String, Map<String, String>> series_name = new TreeMap<String, Map<String, String>>();
        Map<String, Map<String, Integer>> contacts_name = new TreeMap<String, Map<String, Integer>>();

        Map<String, Map<String, String>> view_by_1_list = new TreeMap<String, Map<String, String>>();


        for (int ii = 0; ii < dates.size(); ii++) {
            series_data.put(dates.get(ii), 0);
        }

//        System.out.println("xAxis:"+xAxis);
        //write series
        int iDiscarded = 0;
        String sSqlActivity1 = "select \"name\",\"call_date\",sum(\"total_calls\") as \"total_calls\",sum(\"total_time\") as \"total_time\" " +
                "from \"rpt_call_log_activity1\"  " +
                "where \"call_date\">='" + String.valueOf(starttimeOrig) + "' and \"call_date\"<'" + String.valueOf(endtimeOrig) + "'  " +
                mysqlUtil.getHostnameQueryForPhoenix(ipAddress) +
//                " and hostname='"+ipAddress+"' " +
                " group by \"name\",\"call_date\"";
        String sSqlActivity2 = "select \"name\",\"contact_number\",sum(\"total_calls\") as \"total_calls\" " +
                "from \"rpt_call_log_activity2\"  " +
                "where \"call_date\">='" + String.valueOf(starttimeOrig) + "' and \"call_date\"<'" + String.valueOf(endtimeOrig) + "'  " +
                mysqlUtil.getHostnameQueryForPhoenix(ipAddress) +
//                " and hostname='"+ipAddress+"' " +
                " group by \"name\",\"contact_number\"";
        System.out.println("sSqlActivity1:" + sSqlActivity1);
        System.out.println("sSqlActivity2:" + sSqlActivity2);

        java.sql.Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        List<Object> series = new ArrayList<Object>();

        series.clear();
        int total_calls = 0;
        int total_time = 0;
        try {
            Class.forName(classname_phoenix);
            conn = DriverManager.getConnection(url_phoenix);
            stmt = conn.createStatement();

            rs = stmt.executeQuery(sSqlActivity1);
            String rowkey = "";
            int iDateNo = 0;
            while (rs.next()) {
                rowkey = rs.getString("name");
                total_calls += rs.getInt("total_calls");
                total_time += rs.getInt("total_time");
                String sDateNo = "";
                if (view_by.equals("1")) {
                    sDateNo = MysqlUtil.getViewBy1Str(rs.getInt("call_date"));
                    if (view_by_1_list.get(sDateNo) == null) {
                        Map<String, String> tempMap = new HashMap<String, String>();
                        tempMap.put("total_calls", "0");
                        tempMap.put("total_time", "0");
                        tempMap.put("color", MysqlUtil.createRandomColor());
                        view_by_1_list.put(sDateNo, tempMap);
                    }
                    Map<String, String> dataMap2 = view_by_1_list.get(sDateNo);
                    dataMap2.put("total_calls", String.valueOf(Integer.parseInt(dataMap2.get("total_calls")) + rs.getInt("total_calls")));
                    dataMap2.put("total_time", String.valueOf(Integer.parseInt(dataMap2.get("total_time")) + rs.getInt("total_time")));
                    view_by_1_list.put(sDateNo, dataMap2);

                } else if (interval == 1) {
                    iDateNo = MysqlUtil.getHour(rs.getInt("call_date"), offset);
                    sDateNo = dates.get(iDateNo);
                } else if (interval == 2) {
                    //week
                    String mondayStr = MysqlUtil.getMondayStr(rs.getInt("call_date"), offset);
                    sDateNo = mondayStr;
                } else if (interval == 3) {
                    //month
                    String monthStr = MysqlUtil.getMonthStr(rs.getInt("call_date"), offset);
                    sDateNo = monthStr;
                } else {
                    //day
                    String dayStr = MysqlUtil.getDayStr(rs.getInt("call_date"), offset);
                    sDateNo = dayStr;
                }

//                System.out.println("call_date:"+rs.getString("call_date"));
//                System.out.println("sDateNo:"+sDateNo);
//                System.out.println("series_data:"+series_data);
//                System.out.println("db total_calls:"+rs.getString("total_calls"));
                if (!view_by.equals("1")) {

                    series_data.put(sDateNo, series_data.get(sDateNo) + rs.getInt("total_calls"));
                    if (series_data_name.get(rowkey) == null) {
                        Map<String, Integer> tempMap = new HashMap<String, Integer>();
                        for (int ii = 0; ii < dates.size(); ii++) {
                            tempMap.put(dates.get(ii), 0);
                        }
                        series_data_name.put(rowkey, tempMap);
                    }
                    if (series_name.get(rowkey) == null) {
                        Map<String, String> tempMap = new HashMap<String, String>();
                        tempMap.put("total_calls", "0");
                        tempMap.put("total_time", "0");
                        tempMap.put("name", rowkey);
                        tempMap.put("color", MysqlUtil.createRandomColor());
                        series_name.put(rowkey, tempMap);
                    }
                    Map<String, Integer> dataMap = series_data_name.get(rowkey);
                    dataMap.put(sDateNo, dataMap.get(sDateNo) + rs.getInt("total_calls"));
                    series_data_name.put(rowkey, dataMap);

                    Map<String, String> dataMap2 = series_name.get(rowkey);
                    dataMap2.put("total_calls", String.valueOf(Integer.parseInt(dataMap2.get("total_calls")) + rs.getInt("total_calls")));
                    dataMap2.put("total_time", String.valueOf(Integer.parseInt(dataMap2.get("total_time")) + rs.getInt("total_time")));
                    series_name.put(rowkey, dataMap2);
                }
            }

            if (!view_by.equals("1")) {
                rs = stmt.executeQuery(sSqlActivity2);
                while (rs.next()) {
                    rowkey = rs.getString("name");
                    if (contacts_name.get(rowkey) == null) {
                        Map<String, Integer> tempMap = new HashMap<String, Integer>();
                        contacts_name.put(rowkey, tempMap);
                    }
                    Map<String, Integer> dataMap = contacts_name.get(rowkey);
                    dataMap.put(rs.getString("contact_number"), rs.getInt("total_calls"));
                    contacts_name.put(rowkey, dataMap);

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {

            }
        }
//        activeReport.put("series",series);
//        System.out.println(series_data.toString());
//        System.out.println(series_data_name.toString());
//        System.out.println(series_name.toString());
        StringBuilder sb = new StringBuilder();
        if (view_by.equals("1")) {
            sb.append("{\n");
            for (String sKey : view_by_1_list.keySet()) {
                Map<String, String> dataMap2 = view_by_1_list.get(sKey);
                sb.append("\"" + sKey + "\": {");
                sb.append("\"total_calls\": " + dataMap2.get("total_calls") + ",");
                sb.append("\"total_time\": " + dataMap2.get("total_time") + ",");
                sb.append("\"color\": \"" + dataMap2.get("color") + "\"");
                sb.append("}\n");
            }
            sb.append("}\n");
        } else {
            //output format
            sb.append("{\n");
            sb.append("\"xAxis\": {\n");
            sb.append("\"categories\": [\n");
            for (int ii = 0; ii < dates.size(); ii++) {
                if ((ii + 1) == dates.size())
                    sb.append("\"" + dates.get(ii) + "\"");
                else
                    sb.append("\"" + dates.get(ii) + "\",");
            }
            sb.append("]\n");
            sb.append("},\n");
            sb.append("\"series\": [\n");
            int iFirst = 0;
            for (String sKey : series_name.keySet()) {
                Map<String, String> dataMap2 = series_name.get(sKey);
                if (iFirst == 0) {
                    iFirst = 1;
                } else {
                    sb.append("},\n");
                }
                sb.append("{\n");
                sb.append("\"total_calls\": " + dataMap2.get("total_calls") + ",\n");
                sb.append("\"total_time\": " + dataMap2.get("total_time") + ",\n");
                sb.append("\"name\": \"" + dataMap2.get("name") + "\",\n");
                sb.append("\"period_contacts\": {\n");
                Map<String, Integer> dataMap4 = contacts_name.get(sKey);
                if (dataMap4 != null) {
                    int i = 0;
                    for (String sKey2 : dataMap4.keySet()) {
                        if (i == 0) {
                            i = 1;
                        } else {
                            sb.append(",\n");
                        }
                        sb.append("\"" + sKey2 + "\": " + dataMap4.get(sKey2));
                    }
                    sb.append("\n");
                }
                sb.append("},\n");
                sb.append("\"color\": \"" + dataMap2.get("color") + "\",\n");
                Map<String, Integer> dataMap3 = series_data_name.get(sKey);
                sb.append("\"series_data\": [\n");
                for (int ii = 0; ii < dates.size(); ii++) {
                    if ((ii + 1) == dates.size())
                        sb.append(String.valueOf(dataMap3.get(dates.get(ii))) + "\n");
                    else
                        sb.append(String.valueOf(dataMap3.get(dates.get(ii))) + ",\n");

                }
                sb.append("]\n");

            }
            sb.append("}\n");
            sb.append("],\n");
            //write tabular
            sb.append("\"tabular\": {\n");
            sb.append("\"globals\": {\n");
            sb.append("\"total_calls\": " + String.valueOf(total_calls) + ",\n");
            sb.append("\"total_time\": " + String.valueOf(total_time) + ",\n");
            sb.append("\"series_data\": [\n");
            for (int ii = 0; ii < dates.size(); ii++) {
                if ((ii + 1) == dates.size())
                    sb.append(String.valueOf(series_data.get(dates.get(ii))) + "\n");
                else
                    sb.append(String.valueOf(series_data.get(dates.get(ii))) + ",\n");
            }
            sb.append("]\n");
            sb.append("}\n");
            sb.append("}\n");
            sb.append("}\n");
        }


        return sb.toString();
    }

    private String getOverViewReport(int starttime, int endtime, int offset, int interval, String ipAddress) {
        return "";
        //need function when search over report, then can't phoenix, still use mysqll
//        Date dtStart = new Date();
//        int starttimeOrig = starttime;
//        int endtimeOrig = endtime;
//        if (offset != 0) {
//            starttime = MysqlUtil.addHour(starttime, offset);
//            endtime = MysqlUtil.addHour(endtime, offset);
//        }
//        List<String> dates = new ArrayList<String>();
//        if (interval == 1) {
//            //hour
//            int s1 = starttime ;
//            int e1 = endtime ;
//            int currtime = s1;
//            while (currtime <= e1) {
//                String dayStr = MysqlUtil.getDayHourStr(currtime, 0);
//                dates.add(dayStr);
//                currtime = MysqlUtil.addHour(currtime, 1);
////                if (currtime > e1) {
////                    dayStr = getDayStr(currtime);
////                    dates.add(dayStr);
////                }
//            }
//        } else if (interval == 2) {
//            //week
//            int s1 = starttime / 100 * 100;
//            int e1 = endtime / 100 * 100;
//            int currtime = s1;
//
//            while (currtime <= e1) {
//                String mondayStr = MysqlUtil.getMondayStr(currtime, 0);
//                System.out.println(mondayStr);
//                dates.add(mondayStr);
//                currtime = MysqlUtil.addDays(currtime, 7);
//                if (currtime>e1) {
//                    String mondayStr2 = MysqlUtil.getMondayStr(currtime,0);
//                    if (!mondayStr.equals(mondayStr2)) {
//                        dates.add(mondayStr2);
//                    }
//                }
//            }
//        } else if (interval == 3) {
//            //month
//            int s1 = starttime / 100 * 100;
//            int e1 = endtime / 100 * 100;
//            int currtime = s1;
//            while (currtime <= e1) {
//                String monthStr = MysqlUtil.getMonthStr(currtime, 0);
//                dates.add(monthStr);
//                currtime = MysqlUtil.addMonths(currtime, 1);
////                if (currtime>e1) {
////                    monthStr = getMonthStr(currtime,0);
////                    dates.add(monthStr);
////                }
//            }
//        } else {
//            //days report
//            int s1 = starttime / 100 * 100;
//            int e1 = endtime / 100 * 100;
//            int currtime = s1;
//            while (currtime <= e1) {
//                String dayStr = MysqlUtil.getDayStr(currtime, 0);
//                dates.add(dayStr);
//                currtime = MysqlUtil.addDays(currtime, 1);
////                if (currtime > e1) {
////                    dayStr = getDayStr(currtime);
////                    dates.add(dayStr);
////                }
//            }
//        }
//        //init series_data
//        Map<String, Integer> activity_data = new TreeMap<String, Integer>();
//        Map<String, Integer> contacts_data = new TreeMap<String, Integer>();
//        Map<String, Integer> hour_steps = new TreeMap<String, Integer>();
//        Map<String, Integer> day_steps = new TreeMap<String, Integer>();
//        Map<String, Integer> contacts_global = new TreeMap<String, Integer>();
//        Map<String, Map<String, Integer>> contacts_name = new TreeMap<String, Map<String, Integer>>();
//        Map<String, Map<String, Integer>> tsources_name = new TreeMap<String, Map<String, Integer>>();
//        Map<String, Map<String, Integer>> contacts_step = new TreeMap<String, Map<String, Integer>>();
//
//
//
//        for (int ii = 0; ii < dates.size(); ii++) {
//            activity_data.put(dates.get(ii), 0);
//            contacts_data.put(dates.get(ii), 0);
//        }
//
////        System.out.println("xAxis:"+xAxis);
//        //write series
//        int iDiscarded = 0;
//        String sSqlActivity1 = "select \"call_date\",sum(\"total_calls\") as \"total_calls\",sum(\"total_time\") as \"total_time\" " +
//                "from \"rpt_call_log_activity1\"  " +
//                "where \"call_date\">='" + String.valueOf(starttimeOrig) + "' and \"call_date\"<'" + String.valueOf(endtimeOrig) + "'  " +
//                MysqlUtil.getHostnameQueryForPhoenix(ipAddress) +
//                " group by \"call_date\"";
//        String sSqlActivity2 = "select \"name\",\"contact_number\",sum(\"total_calls\") as \"total_calls\",sum(\"total_time\") as \"total_time\" " +
//                "from \"rpt_call_log_activity2\"  " +
//                "where \"call_date\">='" + String.valueOf(starttimeOrig) + "' and \"call_date\"<'" + String.valueOf(endtimeOrig) + "'  " +
//                MysqlUtil.getHostnameQueryForPhoenix(ipAddress) +
//                " group by \"name\",\"contact_number\"";
//
//        String sSqlContactsData = "select \"call_date\",\"contact_number\",sum(\"total_calls\") as \"total_calls\" from (" +
//                "  select convert(fn_intdate(call_date,"+ String.valueOf(offset) +")/100,UNSIGNED INTEGER) as call_date,contact_number,total_calls " +
//                "  from rpt_call_log_activity2  " +
//                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  " +
//                getHostnameQuery(ipAddress) +
//                "  ) c " +
//                "  group by call_date,contact_number  " +
//                "  order by call_date"
//                ;
//        //only for interval 0
//        if (interval == 1) {
//            sSqlContactsData = "select call_date,contact_number,sum(total_calls) as total_calls from (" +
//                    "  select fn_intdate(call_date,"+ String.valueOf(offset) +") as call_date,contact_number,total_calls " +
//                    "  from rpt_call_log_activity2  " +
//                    "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  " +
//                    getHostnameQuery(ipAddress) +
//                    "  ) c " +
//                    "  group by call_date,contact_number  " +
//                    "  order by call_date"
//            ;
//        } else if (interval == 2) {
//            sSqlContactsData = "select call_date,contact_number,sum(total_calls) as total_calls from (" +
//                    "  select fn_getintmonday(call_date,"+ String.valueOf(offset) +") as call_date,contact_number,total_calls " +
//                    "  from rpt_call_log_activity2  " +
//                    "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  " +
//                    getHostnameQuery(ipAddress) +
//                    "  ) c " +
//                    "  group by call_date,contact_number  " +
//                    "  order by call_date"
//            ;
//        } else if (interval == 3) {
//            sSqlContactsData = "select call_date,contact_number,sum(total_calls) as total_calls from (" +
//                    "  select convert(fn_intdate(call_date,"+ String.valueOf(offset) +")/10000,UNSIGNED INTEGER) as call_date,contact_number,total_calls " +
//                    "  from rpt_call_log_activity2  " +
//                    "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  " +
//                    getHostnameQuery(ipAddress) +
//                    "  ) c " +
//                    "  group by call_date,contact_number " +
//                    "  order by call_date"
//            ;
//        }
//
//        String sSqlHour_steps = " select call_date,sum(total_calls) as total_calls ,sum(total_time) as total_time" +
//                " from (  select fn_gethour(call_date,2) call_date,total_calls ,total_time  " +
//                " from rpt_call_log_activity1" +
//                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  " +
//                getHostnameQuery(ipAddress) +
//                "  ) c " +
//                " group by call_date    " +
//                " order by call_date";
//
//        String sSqlDay_steps = " select call_date,sum(total_calls) as total_calls ,sum(total_time) as total_time" +
//                " from (  select fn_getday(call_date,2) call_date,total_calls ,total_time  " +
//                " from rpt_call_log_activity1" +
//                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  " +
//                getHostnameQuery(ipAddress) +
//                "  ) c " +
//                " group by call_date    " +
//                " order by call_date";
//
//        String sSqlContacts_global = "select contact_number,sum(total_calls) as total_calls " +
//                "  from rpt_call_log_activity2  " +
//                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  " +
//                getHostnameQuery(ipAddress) +
//                "  group by contact_number  "
//                ;
//
//        String sSqlData4 = "select name,sum(total_calls) as total_calls,sum(total_time) as total_time "
//                + " from rpt_call_log_activity1  "
//                + " where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "  "
//                + getHostnameQuery(ipAddress)
//                + "  group by name "
//                ;
//
//        String sSqlTotalContacts = "select count(*) as cc from (" +
//                "  select contact_number,sum(total_time)" +
//                "  from rpt_call_log_activity2  " +
//                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<" + String.valueOf(endtimeOrig) + "    " +
//                getHostnameQuery(ipAddress) +
//                "  group by contact_number" +
//                ") c "
//                ;
//
//        System.out.println("sSqlActivity1:" + sSqlActivity1);
//        System.out.println("sSqlActivity2:" + sSqlActivity2);
//        System.out.println("sSqlContactsData:" + sSqlContactsData);
//        System.out.println("sSqlHour_steps:" + sSqlHour_steps);
//        System.out.println("sSqlDay_steps:" + sSqlDay_steps);
//        System.out.println("sSqlContacts_global:" + sSqlContacts_global);
//        System.out.println("sSqlData4:" + sSqlData4);
//        System.out.println("sSqlTotalContacts:" + sSqlTotalContacts);
//
//
//        Connection conn = null;
//        Statement stmt = null;
//        ResultSet rs = null;
//
//        List<Object> series = new ArrayList<Object>();
//
//        series.clear();
//        int total_calls = 0;
//        int total_time = 0;
//        int total_contacts = 0;
//        try {
//            Class.forName(classname);
//            conn = DriverManager.getConnection(url);
//            stmt = conn.createStatement();
//            rs = stmt.executeQuery(sSqlActivity1);
//            String rowkey = "";
//            int iDateNo = 0;
//            while (rs.next()) {
//                total_calls += rs.getInt("total_calls");
//                total_time += rs.getInt("total_time");
//                String sDateNo = "";
//                if (interval == 1) {
//                    sDateNo = getDayHourStr(rs.getInt("call_date"), offset);
//                } else if (interval == 2) {
//                    //week
//                    String mondayStr = getMondayStr(rs.getInt("call_date"), offset);
//                    sDateNo = mondayStr;
//                } else if (interval == 3) {
//                    //month
//                    String monthStr = getMonthStr(rs.getInt("call_date"), offset);
//                    sDateNo = monthStr;
//                } else {
//                    //day
//                    String dayStr = getDayStr(rs.getInt("call_date"), offset);
//                    sDateNo = dayStr;
//                }
//
////                System.out.println("call_date:"+rs.getString("call_date"));
////                System.out.println("sDateNo:"+sDateNo);
////                System.out.println("series_data:"+series_data);
////                System.out.println("db total_calls:"+rs.getString("total_calls"));
//
//                activity_data.put(sDateNo, activity_data.get(sDateNo) + rs.getInt("total_calls"));
//
//            }
//            System.out.println("sSqlActivity1 end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//            //sSqlActivity2 data
//            rs = stmt.executeQuery(sSqlActivity2);
//            while (rs.next()) {
//                rowkey = rs.getString("name");
//                //detail
//                if (contacts_name.get(rowkey) == null) {
//                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
//                    contacts_name.put(rowkey, tempMap);
//                }
//                Map<String, Integer> dataMap = contacts_name.get(rowkey);
//                dataMap.put(rs.getString("contact_number"), rs.getInt("total_calls"));
//                contacts_name.put(rowkey, dataMap);
//
//                //total_contacts
//                if (tsources_name.get(rowkey) == null) {
//                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
//                    tempMap.put("total_contacts",0);
//                    tsources_name.put(rowkey, tempMap);
//                }
//                Map<String, Integer> tempMap = tsources_name.get(rowkey);
//                tempMap.put("total_contacts",tempMap.get("total_contacts")+rs.getInt("total_calls"));
//                tsources_name.put(rowkey, tempMap);
//            }
//            System.out.println("sSqlActivity2 end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//            //contacts_data
//            rs = stmt.executeQuery(sSqlContactsData);
//            while (rs.next()) {
//                String sDateNo = "";
//                if (interval == 1) {
//                    sDateNo = getDayHourStr(rs.getInt("call_date"), 0);
//                } else if (interval == 2) {
//                    //week
//                    String mondayStr = getMondayStr(rs.getInt("call_date")*100, 0);
//                    sDateNo = mondayStr;
//                } else if (interval == 3) {
//                    //month
//                    //setup to month 1th
//                    String monthStr = getMonthStr(rs.getInt("call_date")*10000+0100, 0);
//                    sDateNo = monthStr;
//                } else {
//                    //day, had offset on mysql
//                    String dayStr = getDayStr(rs.getInt("call_date")*100,0);
//                    sDateNo = dayStr;
//                }
//
////                System.out.println("call_date:"+rs.getString("call_date"));
////                System.out.println("sDateNo:"+sDateNo);
////                System.out.println("series_data:"+series_data);
////                System.out.println("db total_calls:"+rs.getString("total_calls"));
//
//                int cc = 1;
//                contacts_data.put(sDateNo, contacts_data.get(sDateNo) + cc);
//
//                //contacts_step
//                if (contacts_step.get(sDateNo) == null) {
//                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
//                    tempMap.put("total_count", 0);
//                    contacts_step.put(sDateNo, tempMap);
////                    System.out.println("contacts_step add sDateNo:" + sDateNo);
//                }
//
//                Map<String, Integer> tempMap = contacts_step.get(sDateNo);
//                //detail
//                tempMap.put("total_count",tempMap.get("total_count")+cc);
//                //detail number count
//                tempMap.put(rs.getString("contact_number"),rs.getInt("total_calls"));
//                contacts_step.put(sDateNo, tempMap);
//            }
//            System.out.println("sSqlContactsData end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//            //hour_steps
//            rs = stmt.executeQuery(sSqlHour_steps);
//            while (rs.next()) {
//                hour_steps.put(rs.getString("call_date"), rs.getInt("total_calls"));
//            }
//            System.out.println("sSqlHour_steps end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//            //day_steps
//            rs = stmt.executeQuery(sSqlDay_steps);
//            while (rs.next()) {
//                day_steps.put(rs.getString("call_date"), rs.getInt("total_calls"));
//            }
//            System.out.println("sSqlDay_steps end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//            //contacts_global
//            rs = stmt.executeQuery(sSqlContacts_global);
//            while (rs.next()) {
//                contacts_global.put(rs.getString("contact_number"), rs.getInt("total_calls"));
//            }
//            System.out.println("sSqlContacts_global end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//            //tsource name total calls and duration, and null count
//            rs = stmt.executeQuery(sSqlData4);
//            while (rs.next()) {
//                rowkey = rs.getString("name");
//
//                if (tsources_name.get(rowkey) == null) {
//                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
//                    tempMap.put("total_contacts",0);
//                    tempMap.put("total_calls", 0);
//                    tempMap.put("total_time", 0);
//                    tsources_name.put(rowkey, tempMap);
//                }
//
//                //detail
//                Map<String, Integer> tempMap = tsources_name.get(rowkey);
//                tempMap.put("total_calls", rs.getInt("total_calls"));
//                tempMap.put("total_time", rs.getInt("total_time"));
//                tsources_name.put(rowkey, tempMap);
//
//            }
//            System.out.println("sSqlData4 end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//            //total contacts
//            //tsource name total calls and duration, and null count
//            rs = stmt.executeQuery(sSqlTotalContacts);
//            if (rs.next()) {
//                total_contacts = rs.getInt("cc");
//            }
//
//            System.out.println("sSqlTotalContacts end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (rs != null)
//                    rs.close();
//                if (stmt != null)
//                    stmt.close();
//                if (conn != null)
//                    conn.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            } finally {
//
//            }
//        }
////        activeReport.put("series",series);
////        System.out.println(series_data.toString());
////        System.out.println(series_data_name.toString());
////        System.out.println(series_name.toString());
//        StringBuilder sb = new StringBuilder();
//        //output format
//        sb.append("{\n");
//        sb.append("\"" + String.valueOf(starttimeOrig) + " ~ " + String.valueOf(endtimeOrig) + "\": {\n");
//        sb.append("\"total_calls\": " + String.valueOf(total_calls) + ",\n");
//        sb.append("\"total_time\": " + String.valueOf(total_time) + ",\n");
//        sb.append("\"total_contacts\": " + String.valueOf(total_contacts) + ",\n");
//        Double average_time = total_time/(total_calls*1.0);
//        sb.append("\"average_time\": " + String.valueOf(average_time) + ",\n");
//        sb.append("\"color\": \"" + createRandomColor() + "\",\n");
//
//        sb.append("\"timeline_chart\": {\n");
//        sb.append("\"xAxis\": {\n");
//        sb.append("\"categories\": [\n");
//        for (int ii = 0; ii < dates.size(); ii++) {
//            if ((ii + 1) == dates.size())
//                sb.append("\"" + dates.get(ii) + "\"");
//            else
//                sb.append("\"" + dates.get(ii) + "\",");
//        }
//        sb.append("]\n");
//        sb.append("},\n");
//        sb.append("\"series\": {\n");
//        sb.append("\"activity_data\": [\n");
//        for (int ii = 0; ii < dates.size(); ii++) {
//            if ((ii + 1) == dates.size())
//                sb.append(String.valueOf(activity_data.get(dates.get(ii))) + "\n");
//            else
//                sb.append(String.valueOf(activity_data.get(dates.get(ii))) + ",\n");
//        }
//        sb.append("],\n");
//
//        //contacts_data
//        sb.append("\"contacts_data\": [\n");
//        for (int ii = 0; ii < dates.size(); ii++) {
//            if ((ii + 1) == dates.size())
//                sb.append(String.valueOf(contacts_data.get(dates.get(ii))) + "\n");
//            else
//                sb.append(String.valueOf(contacts_data.get(dates.get(ii))) + ",\n");
//        }
//        sb.append("],\n");
//
//        //contacts_step
//        sb.append("\"contacts_step\": {\n");
//        int ii = 0;
//        for (String rowkey : contacts_step.keySet()) {
//            Map<String, Integer> tempMap =contacts_step.get(rowkey);
//            //get max
//            String max_number = "";
//            int max_number_value = 0;
//            for (String sKey : tempMap.keySet()) {
//                if (sKey.equals("total_count")) {
//                    continue;
//                }
//                int iValue = tempMap.get(sKey);
//                if (max_number.equals("") || max_number_value<iValue) {
//                    max_number = sKey;
//                    max_number_value = iValue;
//                }
//            }
//            sb.append("\"" + rowkey + "\": {\n");
//
//            sb.append("\"total_count\": "+tempMap.get("total_count")+",\n");
//            sb.append("\"max\": {\n");
//            sb.append("\"name\": \""+max_number+"\",\n");
//            sb.append("\"value\": "+max_number_value+"\n");
//            sb.append("},\n");
//
//            int iNumberNo = 0;
//            for (String sKey : tempMap.keySet()) {
//                iNumberNo++;
//                if (sKey.equals("total_count")) {
//                    continue;
//                }
//                if (iNumberNo == tempMap.size())
//                    sb.append("\""+sKey+"\": "+tempMap.get(sKey)+"\n");
//                else
//                    sb.append("\""+sKey+"\": "+tempMap.get(sKey)+",\n");
//            }
//            if ((ii + 1) == contacts_step.size())
//                sb.append("}\n");
//            else
//                sb.append("},\n");
//            ii++;
//        }
//
//        sb.append("}\n");
//        sb.append("}\n");
//        sb.append("},\n");
//
//        //hour_steps
//        sb.append("\"hour_steps\": {\n");
//        int max_hour_value = 0;
//        String max_hour = "";
//        for (String sKey : hour_steps.keySet()) {
//            if (max_hour.equals("") || max_hour_value<hour_steps.get(sKey)) {
//                max_hour = sKey;
//                max_hour_value = hour_steps.get(sKey);
//            }
//            sb.append("\""+sKey+"\": "+hour_steps.get(sKey)+",\n");
//        }
//        //hour_steps max
//        sb.append("\"max\": {\n");
//        sb.append("\"name\": "+max_hour+",\n");
//        sb.append("\"value\": "+String.valueOf(max_hour_value)+"\n");
//        sb.append("}\n");
//        sb.append("},\n");
//
//        //day_steps
//        sb.append("\"day_steps\": {\n");
//        int max_day_value = 0;
//        String max_day = "";
//        for (String sKey : day_steps.keySet()) {
//            if (max_day.equals("") || max_day_value<day_steps.get(sKey)) {
//                max_day = sKey;
//                max_day_value = day_steps.get(sKey);
//            }
//        }
//        //day_steps max
//        sb.append("\"max\": {\n");
//        sb.append("\"name\": \""+max_day+"\",\n");
//        sb.append("\"value\": "+String.valueOf(max_day_value)+"\n");
//        sb.append("},\n");
//
//        int iCount = 0;
//        for (String sKey : day_steps.keySet()) {
//            if (iCount==day_steps.keySet().size()-1) {
//                sb.append("\"" + sKey + "\": " + day_steps.get(sKey) + "\n");
//            } else {
//                sb.append("\"" + sKey + "\": " + day_steps.get(sKey) + ",\n");
//            }
//            iCount++;
//        }
//        sb.append("},\n");
//
//        //contacts_blobal
//        sb.append("\"contacts_global\": {\n");
//        int max_contacts_global_value = 0;
//        String max_contacts_global = "";
//        for (String sKey : contacts_global.keySet()) {
//            if (max_contacts_global.equals("") || max_contacts_global_value<contacts_global.get(sKey)) {
//                max_contacts_global = sKey;
//                max_contacts_global_value = contacts_global.get(sKey);
//            }
//        }
//        //contacts_global max
//        sb.append("\"max\": {\n");
//        sb.append("\"name\": \""+max_contacts_global+"\",\n");
//        sb.append("\"value\": "+String.valueOf(max_contacts_global_value)+"\n");
//        sb.append("},\n");
//
//        iCount = 0;
//        for (String sKey : contacts_global.keySet()) {
//            if (iCount==contacts_global.keySet().size()-1) {
//                sb.append("\"" + sKey + "\": " + contacts_global.get(sKey) + "\n");
//            } else {
//                sb.append("\"" + sKey + "\": " + contacts_global.get(sKey) + ",\n");
//            }
//            iCount++;
//        }
//        sb.append("},\n");
//
//        //tsources
//        sb.append("\"tsources\": {\n");
//        //max
//        String max_name = "";
//        int max_value = 0;
//        for (String sKey : tsources_name.keySet()) {
//            Map<String, Integer> dataMap2 = tsources_name.get(sKey);
//            if (max_name.equals("") || max_value<dataMap2.get("total_calls")) {
//                max_name = sKey;
//                max_value = dataMap2.get("total_calls");
//            }
//        }
//
//        sb.append("\"max\": {\n");
//        sb.append("\"name\": \""+max_name+"\",\n");
//        sb.append("\"value\": "+String.valueOf(max_value)+"\n");
//        sb.append("},\n");
//
//        ii = 0;
//        for (String sKey : tsources_name.keySet()) {
//            Map<String, Integer> dataTsources_name = tsources_name.get(sKey);
//            sb.append("\"" + sKey + "\": {\n");
//            sb.append("\"total_calls\": " + String.valueOf(dataTsources_name.get("total_calls")) + ",\n");
//            sb.append("\"total_duration\": " + String.valueOf(dataTsources_name.get("total_time")) + ",\n");
//            sb.append("\"total_contacts\": " + String.valueOf(dataTsources_name.get("total_contacts")) + ",\n");
//            //contacts
//            //get null count
//            int null_count = dataTsources_name.get("total_calls")-dataTsources_name.get("total_contacts");
//
//            sb.append("\"contacts\": {\n");
//            if (null_count>0) {
//                if (dataTsources_name.get("total_contacts")==0) {
//                    sb.append("\"null\": " + String.valueOf(null_count) );
//                }
//                else {
//                    sb.append("\"null\": " + String.valueOf(null_count) + ",");
//                }
//
//
//            }
//            Map<String, Integer> dataMap3 = contacts_name.get(sKey);
//            if (dataMap3 != null) {
//                int i = 0;
//                for (String sKey2 : dataMap3.keySet()) {
//                    if (i == 0) {
//                        i = 1;
//                    } else {
//                        sb.append(",\n");
//                    }
//                    sb.append("\"" + sKey2 + "\": " + dataMap3.get(sKey2));
//                }
//                sb.append("\n");
//            }
//            sb.append("},\n");
//
//            sb.append("\"color\": \"" + createRandomColor() + "\"\n");
//            if (ii == tsources_name.keySet().size() - 1)
//                sb.append("}\n");
//            else
//                sb.append("},\n");
//
//            ii++;
//        }
//        sb.append("}\n");
//        sb.append("}\n");
//        sb.append("}\n");
//        System.out.println("sb end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");
//
//        return sb.toString();
    }


    public static void main(String[] args){
        HbaseUtilNew mHbase = new HbaseUtilNew();
        Map<String,String> map = new HashMap<String,String> ();
        map.put("A","a");
        map =mHbase.getOneTimeAndQualifierRecord("cdr","581270");
        System.out.println(map.toString());
    }


}