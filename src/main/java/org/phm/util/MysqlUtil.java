package org.phm.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by siglex.li 2016-09-4.
 */
public final class MysqlUtil {

//    private final static String classname = "com.mysql.jdbc.Driver";
    private final static String classname = "com.mysql.cj.jdbc.Driver";

    private final static String url = "jdbc:mysql://127.0.0.1:3306/bigdata?useSSL=false&serverTimezone=UTC&user=bigdata&password=RkHado@p202!!&useUnicode=true&characterEncoding=UTF8";
//    private final static String url = "jdbc:mysql://127.0.0.1:3306/dash?user=root&password=123456&useUnicode=true&characterEncoding=UTF8";

    public static String getIpAddress(HttpServletRequest request) {
        String Xip = request.getHeader("X-Real-IP");
        String XFor = request.getHeader("X-Forwarded-For");
        if (StringUtils.isNotEmpty(XFor) && !"unKnown".equalsIgnoreCase(XFor)) {
            //多次反向代理后会有多个ip值，第一个ip才是真实ip
            int index = XFor.indexOf(",");
            if (index != -1) {
                return XFor.substring(0, index);
            } else {
                return XFor;
            }
        }
        XFor = Xip;
        if (StringUtils.isNotEmpty(XFor) && !"unKnown".equalsIgnoreCase(XFor)) {
            return XFor;
        }
        if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
            XFor = request.getHeader("Proxy-Client-IP");
        }
        if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
            XFor = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
            XFor = request.getHeader("HTTP_CLIENT_IP");
        }
        if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
            XFor = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
            XFor = request.getRemoteAddr();
        }
        return XFor;
    }

    public static String getDayStr(int iCurrday, int offset) {
        iCurrday = addHour(iCurrday, offset);
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;

        return pad(iMonth, 2) + "-" +pad(iDay, 2);
    }
    @Test
    public void TestIt() {
        System.out.println(getDayHourStr(2020120916,2));
    }
    public static String getDayHourStr(int iCurrday, int offset) {
        iCurrday = addHour(iCurrday, offset);
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;
        int iHour = iCurrday - iYear * 1000000 - iMonth * 10000 - iDay*100;
//        System.out.println(iCurrday);
//        System.out.println(iCurrday - iYear * 1000000);
//        System.out.println(iCurrday - iYear * 1000000 - iMonth * 10000);
//        System.out.println(iHour);

        return pad(iMonth, 2) + "-" +pad(iDay, 2)+ " " +pad(iHour, 2);
    }

    public static String getMonthStr(int iCurrday, int offset) {
        iCurrday = addHour(iCurrday, offset);
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;

        String[] monthList = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(",");
        return monthList[iMonth - 1] + "-" + String.valueOf(iYear);
    }

    public static int addDays(int iCurrday, int days) {
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;
        Date date = new Date(iYear - 1900, iMonth - 1, iDay, 0, 0);
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        cld.add(Calendar.DATE, days);
        date = cld.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHH");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        return Integer.parseInt(df.format(date));
    }

    public static int addMonths(int iCurrday, int months) {
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = 1;
        Date date = new Date(iYear - 1900, iMonth - 1, iDay, 0, 0);
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        cld.add(Calendar.MONTH, months);
        date = cld.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHH");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        return Integer.parseInt(df.format(date));
    }

    public static int getDateInt(String sTimestamp) {
        int iDate = 0;
        try {
            Long dateLong = Long.parseLong(sTimestamp) * 1000;
            Date date3 = new Date(dateLong);//Long型毫秒数转换为Date型
            DateFormat df = new SimpleDateFormat("yyyyMMddHH");
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            iDate = Integer.parseInt(df.format(date3));
        } catch (Exception e) {
            iDate = 0;
        }
        return iDate;

    }

    public static String getViewBy1Str(int iCurrday) {
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;
        return pad(iMonth, 2) + "-" + pad(iDay, 2) + String.valueOf(iYear);
    }

    public static String getMondayStr(int iCurrday, int offset) {
        iCurrday = addHour(iCurrday, offset);
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;
        Date date = new Date(iYear - 1900, iMonth - 1, iDay);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar cld = Calendar.getInstance();
//        System.out.println("date:" + date);
        cld.setTime(date);
//        System.out.println("cld:" + df.format(cld.getTime()));

        int dayWeek = cld.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cld.add(Calendar.DAY_OF_MONTH, -1);
        }
//        System.out.println("cld:" + df.format(cld.getTime()));

        cld.setFirstDayOfWeek(Calendar.MONDAY);//2020100201
        int day = cld.get(Calendar.DAY_OF_WEEK);
        cld.add(Calendar.DATE, cld.getFirstDayOfWeek() - day);
//        System.out.println("cld:" + df.format(cld.getTime()));

        String ss = df.format(cld.getTime());
//        ss= ss.substring(5,7)+"-"+ss.substring(8);

        return ss;
    }

    public static String createRandomColor() {
        String red;
        String green;
        String blue;
        Random random = new Random();
        red = Integer.toHexString(random.nextInt(256)).toUpperCase();
        green = Integer.toHexString(random.nextInt(256)).toUpperCase();
        blue = Integer.toHexString(random.nextInt(256)).toUpperCase();

        red = red.length() == 1 ? "0" + red : red;
        green = green.length() == 1 ? "0" + green : green;
        blue = blue.length() == 1 ? "0" + blue : blue;
        String color = "#" + red + green + blue;

//        System.out.println(color);

        return color.toUpperCase();
    }

    public static int getHour(int iCurrday, int offset) {
        iCurrday = addHour(iCurrday, offset);
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;
        int iHour = (iCurrday - iYear * 1000000 - iMonth * 10000 - iDay * 100);
        return iHour;
    }

    public static int addHour(int iCurrday, int offset) {
        int iYear = iCurrday / 1000000;
        int iMonth = (iCurrday - iYear * 1000000) / 10000;
        int iDay = (iCurrday - iYear * 1000000 - iMonth * 10000) / 100;
        int iHour = (iCurrday - iYear * 1000000 - iMonth * 10000 - iDay * 100);
        Date date = new Date(iYear - 1900, iMonth - 1, iDay, iHour, 0, 0);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHH");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        cld.add(Calendar.HOUR, offset);
        String ss = df.format(cld.getTime());
        return Integer.parseInt(ss);
    }
    public static String addSecond(String sCurrday, int offset) {
        //sCurrday 20210102235849
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = null;
        try {
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            date = df.parse(sCurrday);
            Calendar cld = Calendar.getInstance();
            cld.setTime(date);
            cld.add(Calendar.SECOND, offset);
            return df.format(cld.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
    @Test
    public void TestAddSecond() {
        String ss = "20210102235849";
        System.out.println(addSecond(ss,100));
    }

    public static String pad(int number, int digits) {
        String sResult = String.valueOf(number);

        while (sResult.length() < digits) {
            sResult = "0" + sResult;
        }
        return sResult;
    }

    public static String getHostnameQuery(String ipAddress) {
        String sSql = "select hostnames " +
                "from hostnames  " +
                "where request_ip='" + ipAddress + "' ";

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String hostNameSsql = "";
        String hostNames = ipAddress;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(url);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sSql);
            while (rs.next()) {
                hostNames = rs.getString("hostnames");
                break;
            }

            for (String ss : hostNames.split(",")) {
                if (hostNameSsql.equals("")) {
                    hostNameSsql = " hostname='" + ss + "'";
                } else {
                    hostNameSsql += " or hostname='" + ss + "'";
                }
            }

            hostNameSsql = " and ( " + hostNameSsql + " ) ";

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
            }
        }
        return hostNameSsql;
    }
    public static String getHostnameQueryForPhoenix(String ipAddress) {
        String sSql = "select hostnames " +
                "from hostnames  " +
                "where request_ip='" + ipAddress + "' ";

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String hostNameSsql = "";
        String hostNames = ipAddress;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(url);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sSql);
            while (rs.next()) {
                hostNames = rs.getString("hostnames");
                break;
            }

            for (String ss : hostNames.split(",")) {
                if (hostNameSsql.equals("")) {
                    hostNameSsql = " \"hostname\"='" + ss + "'";
                } else {
                    hostNameSsql += " or \"hostname\"='" + ss + "'";
                }
            }

            hostNameSsql = " and ( " + hostNameSsql + " ) ";

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
            }
        }
        return hostNameSsql;
    }
    public static String getHostname(String ipAddress) {
        String sSql = "select hostnames " +
                "from hostnames  " +
                "where request_ip='" + ipAddress + "' ";

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String hostNames = ipAddress;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(url);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sSql);
            while (rs.next()) {
                hostNames = rs.getString("hostnames");
                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
            }
        }
        return hostNames;
    }

    public static String getCallLogActivityReport(int starttime, int endtime, int offset, int interval,String customerId, String view_by, String ipAddress) {
        int starttimeOrig = starttime;
        int endtimeOrig = endtime;
        if (offset != 0) {
            starttime = addHour(starttime, offset);
            endtime = addHour(endtime, offset);
        }
        List<String> dates = new ArrayList<String>();
        if (view_by.equals("1")) {

        } else if (interval == 1) {
            //hour
            for (int i = 0; i < 24; i++) {
                dates.add(pad(i, 2));
            }
        } else if (interval == 2) {
            //week
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;

            while (currtime <= e1) {
                String mondayStr = getMondayStr(currtime, 0);
                dates.add(mondayStr);
                currtime = addDays(currtime, 7);
                if (currtime>e1) {
                    String mondayStr2 = getMondayStr(currtime,0);
                    if (!mondayStr.equals(mondayStr2)) {
                        dates.add(mondayStr2);
                    }
                }
            }
        } else if (interval == 3) {
            //month
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;
            while (currtime <= e1) {
                String monthStr = getMonthStr(currtime, 0);
                dates.add(monthStr);
                currtime = addMonths(currtime, 1);
//                if (currtime>e1) {
//                    monthStr = getMonthStr(currtime,0);
//                    dates.add(monthStr);
//                }
            }
        } else {
            //days report
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;
            while (currtime <= e1) {
                String dayStr = getDayStr(currtime, 0);
                dates.add(dayStr);
                currtime = addDays(currtime, 1);
//                if (currtime > e1) {
//                    dayStr = getDayStr(currtime);
//                    dates.add(dayStr);
//                }
            }
        }
        //init series_data
        Map<String, Integer> series_data = new TreeMap<String, Integer>();
        Map<String, Map<String, Integer>> series_data_name = new TreeMap<String, Map<String, Integer>>();
        Map<String, Map<String, String>> series_name = new TreeMap<String, Map<String, String>>();
        Map<String, Map<String, Integer>> contacts_name = new TreeMap<String, Map<String, Integer>>();

        Map<String, Map<String, String>> view_by_1_list = new TreeMap<String, Map<String, String>>();


        for (int ii = 0; ii < dates.size(); ii++) {
            series_data.put(dates.get(ii), 0);
        }

//        System.out.println("xAxis:"+xAxis);
        //write series
        int iDiscarded = 0;
        String sSqlActivity1 = "select ifnull(name,'OTHERS') as name ,call_date,sum(total_calls) as total_calls,sum(total_time) as total_time " +
                "from rpt_call_log_activity1  " +
                "where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress)
                ;
        if ( customerId!=null&& !customerId.equals("")) {
            sSqlActivity1 +=" and customerId = '"+customerId+"'";
        }
        sSqlActivity1 +=" group by ifnull(name,'OTHERS') ,call_date";

        String sSqlActivity2 = "select ifnull(name,'OTHERS') as name,ifnull(contact_number,'') as contact_number,sum(total_calls) as total_calls " +
                "from rpt_call_log_activity2  " +
                "where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress)
                ;
        if ( customerId!=null&& !customerId.equals("")) {
            sSqlActivity2 +=" and customerId = '"+customerId+"'";
        }
        sSqlActivity2 +=" group by ifnull(name,'OTHERS') ,ifnull(contact_number,'') ";

        System.out.println("sSqlActivity1:" + sSqlActivity1);
        System.out.println("sSqlActivity2:" + sSqlActivity2);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        List<Object> series = new ArrayList<Object>();

        series.clear();
        int total_calls = 0;
        int total_time = 0;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(url);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sSqlActivity1);
            String rowkey = "";
            int iDateNo = 0;
            while (rs.next()) {
                rowkey = rs.getString("name");
                total_calls += rs.getInt("total_calls");
                total_time += rs.getInt("total_time");
                String sDateNo = "";
                if (view_by.equals("1")) {
                    sDateNo = getViewBy1Str(rs.getInt("call_date"));
                    if (view_by_1_list.get(sDateNo) == null) {
                        Map<String, String> tempMap = new HashMap<String, String>();
                        tempMap.put("total_calls", "0");
                        tempMap.put("total_time", "0");
                        tempMap.put("color", createRandomColor());
                        view_by_1_list.put(sDateNo, tempMap);
                    }
                    Map<String, String> dataMap2 = view_by_1_list.get(sDateNo);
                    dataMap2.put("total_calls", String.valueOf(Integer.parseInt(dataMap2.get("total_calls")) + rs.getInt("total_calls")));
                    dataMap2.put("total_time", String.valueOf(Integer.parseInt(dataMap2.get("total_time")) + rs.getInt("total_time")));
                    view_by_1_list.put(sDateNo, dataMap2);

                } else if (interval == 1) {
                    iDateNo = getHour(rs.getInt("call_date"), offset);
                    sDateNo = dates.get(iDateNo);
                } else if (interval == 2) {
                    //week
                    String mondayStr = getMondayStr(rs.getInt("call_date"), offset);
                    sDateNo = mondayStr;
                } else if (interval == 3) {
                    //month
                    String monthStr = getMonthStr(rs.getInt("call_date"), offset);
                    sDateNo = monthStr;
                } else {
                    //day
                    String dayStr = getDayStr(rs.getInt("call_date"), offset);
                    sDateNo = dayStr;
                }

//                System.out.println("call_date:"+rs.getString("call_date"));
//                System.out.println("sDateNo:"+sDateNo);
//                System.out.println("series_data:"+series_data);
//                System.out.println("db total_calls:"+rs.getString("total_calls"));
                if (!view_by.equals("1")) {

                    series_data.put(sDateNo, series_data.get(sDateNo) + rs.getInt("total_calls"));
                    if (series_data_name.get(rowkey) == null) {
                        Map<String, Integer> tempMap = new HashMap<String, Integer>();
                        for (int ii = 0; ii < dates.size(); ii++) {
                            tempMap.put(dates.get(ii), 0);
                        }
                        series_data_name.put(rowkey, tempMap);
                    }
                    if (series_name.get(rowkey) == null) {
                        Map<String, String> tempMap = new HashMap<String, String>();
                        tempMap.put("total_calls", "0");
                        tempMap.put("total_time", "0");
                        tempMap.put("name", rowkey);
                        tempMap.put("color", createRandomColor());
                        series_name.put(rowkey, tempMap);
                    }
                    Map<String, Integer> dataMap = series_data_name.get(rowkey);
                    dataMap.put(sDateNo, dataMap.get(sDateNo) + rs.getInt("total_calls"));
                    series_data_name.put(rowkey, dataMap);

                    Map<String, String> dataMap2 = series_name.get(rowkey);
                    dataMap2.put("total_calls", String.valueOf(Integer.parseInt(dataMap2.get("total_calls")) + rs.getInt("total_calls")));
                    dataMap2.put("total_time", String.valueOf(Integer.parseInt(dataMap2.get("total_time")) + rs.getInt("total_time")));
                    series_name.put(rowkey, dataMap2);
                }


            }

            if (!view_by.equals("1")) {
                rs = stmt.executeQuery(sSqlActivity2);
                while (rs.next()) {
                    rowkey = rs.getString("name");
                    if (contacts_name.get(rowkey) == null) {
                        Map<String, Integer> tempMap = new HashMap<String, Integer>();
                        contacts_name.put(rowkey, tempMap);
                    }
                    Map<String, Integer> dataMap = contacts_name.get(rowkey);
                    dataMap.put(rs.getString("contact_number"), rs.getInt("total_calls"));
                    contacts_name.put(rowkey, dataMap);

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {

            }
        }
//        activeReport.put("series",series);
//        System.out.println(series_data.toString());
//        System.out.println(series_data_name.toString());
//        System.out.println(series_name.toString());
        StringBuilder sb = new StringBuilder();
        if (view_by.equals("1")) {
            sb.append("{\n");
            for (String sKey : view_by_1_list.keySet()) {
                Map<String, String> dataMap2 = view_by_1_list.get(sKey);
                sb.append("\"" + sKey + "\": {");
                sb.append("\"total_calls\": " + dataMap2.get("total_calls") + ",");
                sb.append("\"total_time\": " + dataMap2.get("total_time") + ",");
                sb.append("\"color\": \"" + dataMap2.get("color") + "\"");
                sb.append("}\n");
            }
            sb.append("}\n");
        } else {
            //output format
            sb.append("{\n");
            sb.append("\"xAxis\": {\n");
            sb.append("\"categories\": [\n");
            for (int ii = 0; ii < dates.size(); ii++) {
                if ((ii + 1) == dates.size())
                    sb.append("\"" + dates.get(ii) + "\"");
                else
                    sb.append("\"" + dates.get(ii) + "\",");
            }
            sb.append("]\n");
            sb.append("},\n");
            sb.append("\"series\": [\n");
            int iFirst = 0;
            for (String sKey : series_name.keySet()) {
                Map<String, String> dataMap2 = series_name.get(sKey);
                if (iFirst == 0) {
                    iFirst = 1;
                } else {
                    sb.append("},\n");
                }
                sb.append("{\n");
                sb.append("\"total_calls\": " + dataMap2.get("total_calls") + ",\n");
                sb.append("\"total_time\": " + dataMap2.get("total_time") + ",\n");
                sb.append("\"name\": \"" + dataMap2.get("name") + "\",\n");
                sb.append("\"period_contacts\": {\n");
                Map<String, Integer> dataMap4 = contacts_name.get(sKey);
                if (dataMap4 != null) {
                    int i = 0;
                    for (String sKey2 : dataMap4.keySet()) {
                        if (i == 0) {
                            i = 1;
                        } else {
                            sb.append(",\n");
                        }
                        sb.append("\"" + sKey2 + "\": " + dataMap4.get(sKey2));
                    }
                    sb.append("\n");
                }
                sb.append("},\n");
                sb.append("\"color\": \"" + dataMap2.get("color") + "\",\n");
                Map<String, Integer> dataMap3 = series_data_name.get(sKey);
                sb.append("\"series_data\": [\n");
                for (int ii = 0; ii < dates.size(); ii++) {
                    if ((ii + 1) == dates.size())
                        sb.append(String.valueOf(dataMap3.get(dates.get(ii))) + "\n");
                    else
                        sb.append(String.valueOf(dataMap3.get(dates.get(ii))) + ",\n");

                }
                sb.append("]\n");

            }
            sb.append("}\n");
            sb.append("],\n");
            //write tabular
            sb.append("\"tabular\": {\n");
            sb.append("\"globals\": {\n");
            sb.append("\"total_calls\": " + String.valueOf(total_calls) + ",\n");
            sb.append("\"total_time\": " + String.valueOf(total_time) + ",\n");
            sb.append("\"series_data\": [\n");
            for (int ii = 0; ii < dates.size(); ii++) {
                if ((ii + 1) == dates.size())
                    sb.append(String.valueOf(series_data.get(dates.get(ii))) + "\n");
                else
                    sb.append(String.valueOf(series_data.get(dates.get(ii))) + ",\n");
            }
            sb.append("]\n");
            sb.append("}\n");
            sb.append("}\n");
            sb.append("}\n");
        }


        return sb.toString();
    }

    public String getOverViewReport(int starttime, int endtime, int offset, int interval,String customerId, String ipAddress) {
        Date dtStart = new Date();
        int starttimeOrig = starttime;
        int endtimeOrig = endtime;
        if (offset != 0) {
            starttime = addHour(starttime, offset);
            endtime = addHour(endtime, offset);
        }
        List<String> dates = new ArrayList<String>();
        if (interval == 1) {
            //hour
            int s1 = starttime ;
            int e1 = endtime ;
            int currtime = s1;
            while (currtime <= e1) {
                String dayStr = getDayHourStr(currtime, 0);
                dates.add(dayStr);
                currtime = addHour(currtime, 1);
//                if (currtime > e1) {
//                    dayStr = getDayStr(currtime);
//                    dates.add(dayStr);
//                }
            }
        } else if (interval == 2) {
            //week
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;

            while (currtime <= e1) {
                String mondayStr = getMondayStr(currtime, 0);
                System.out.println(mondayStr);
                dates.add(mondayStr);
                currtime = addDays(currtime, 7);
                if (currtime>e1) {
                    String mondayStr2 = getMondayStr(currtime,0);
                    if (!mondayStr.equals(mondayStr2)) {
                        dates.add(mondayStr2);
                    }
                }
            }
        } else if (interval == 3) {
            //month
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;
            while (currtime <= e1) {
                String monthStr = getMonthStr(currtime, 0);
                dates.add(monthStr);
                currtime = addMonths(currtime, 1);
//                if (currtime>e1) {
//                    monthStr = getMonthStr(currtime,0);
//                    dates.add(monthStr);
//                }
            }
        } else {
            //days report
            int s1 = starttime / 100 * 100;
            int e1 = endtime / 100 * 100;
            int currtime = s1;
            while (currtime <= e1) {
                String dayStr = getDayStr(currtime, 0);
                dates.add(dayStr);
                currtime = addDays(currtime, 1);
//                if (currtime > e1) {
//                    dayStr = getDayStr(currtime);
//                    dates.add(dayStr);
//                }
            }
        }
        //init series_data
        Map<String, Integer> activity_data = new TreeMap<String, Integer>();
        Map<String, Integer> contacts_data = new TreeMap<String, Integer>();
        Map<String, Integer> hour_steps = new TreeMap<String, Integer>();
        Map<String, Integer> day_steps = new TreeMap<String, Integer>();
        Map<String, Integer> contacts_global = new TreeMap<String, Integer>();
        Map<String, Map<String, Integer>> contacts_name = new TreeMap<String, Map<String, Integer>>();
        Map<String, Map<String, Integer>> tsources_name = new TreeMap<String, Map<String, Integer>>();
        Map<String, Map<String, Integer>> contacts_step = new TreeMap<String, Map<String, Integer>>();



        for (int ii = 0; ii < dates.size(); ii++) {
            activity_data.put(dates.get(ii), 0);
            contacts_data.put(dates.get(ii), 0);
        }

//        System.out.println("xAxis:"+xAxis);
        //write series
        int iDiscarded = 0;
        String sSqlCustomerId = "";
        if ( customerId!=null&& !customerId.equals("")) {
            sSqlCustomerId =" and customerId = '"+customerId+"' ";
        }
        String sSqlActivity1 = "select call_date,sum(total_calls) as total_calls,sum(total_time) as total_time " +
                "from rpt_call_log_activity1  " +
                "where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress) ;
        if ( customerId!=null&& !customerId.equals("")) {
            sSqlActivity1 +=" and customerId = '"+customerId+"'";
        }
        sSqlActivity1 +=" group by call_date";

        String sSqlActivity2 = "select ifnull(name,'OTHERS') as name ,ifnull(contact_number,'') as contact_number,sum(total_calls) as total_calls,sum(total_time) as total_time " +
                "from rpt_call_log_activity2  " +
                "where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress)
                ;

        if ( customerId!=null&& !customerId.equals("")) {
            sSqlActivity2 +=" and customerId = '"+customerId+"'";
        }
        sSqlActivity2 +=" group by ifnull(name,'OTHERS'),ifnull(contact_number,'')";

        String sSqlContactsData = "select call_date,ifnull(contact_number,'') as contact_number,sum(total_calls) as total_calls from (" +
                "  select convert(fn_intdate(call_date,"+ String.valueOf(offset) +")/100,UNSIGNED INTEGER) as call_date,contact_number,total_calls " +
                "  from rpt_call_log_activity2  " +
                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress) + sSqlCustomerId +
                "  ) c " +
                "  group by call_date,ifnull(contact_number,'')  " +
                "  order by call_date"
                ;
        //only for interval 0
        if (interval == 1) {
            sSqlContactsData = "select call_date,ifnull(contact_number,'') as contact_number,sum(total_calls) as total_calls from (" +
                    "  select fn_intdate(call_date,"+ String.valueOf(offset) +") as call_date,contact_number,total_calls " +
                    "  from rpt_call_log_activity2  " +
                    "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                    getHostnameQuery(ipAddress) + sSqlCustomerId +
                    "  ) c " +
                    "  group by call_date,ifnull(contact_number,'')  " +
                    "  order by call_date"
            ;
        } else if (interval == 2) {
            sSqlContactsData = "select call_date,ifnull(contact_number,'') as contact_number,sum(total_calls) as total_calls from (" +
                    "  select fn_getintmonday(call_date,"+ String.valueOf(offset) +") as call_date,contact_number,total_calls " +
                    "  from rpt_call_log_activity2  " +
                    "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                    getHostnameQuery(ipAddress) + sSqlCustomerId +
                    "  ) c " +
                    "  group by call_date,ifnull(contact_number,'')  " +
                    "  order by call_date"
            ;
        } else if (interval == 3) {
            sSqlContactsData = "select call_date,ifnull(contact_number,'') as contact_number,sum(total_calls) as total_calls from (" +
                    "  select convert(fn_intdate(call_date,"+ String.valueOf(offset) +")/10000,UNSIGNED INTEGER) as call_date,contact_number,total_calls " +
                    "  from rpt_call_log_activity2  " +
                    "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                    getHostnameQuery(ipAddress) + sSqlCustomerId +
                    "  ) c " +
                    "  group by call_date,ifnull(contact_number,'') " +
                    "  order by call_date"
            ;
        }

        String sSqlHour_steps = " select call_date,sum(total_calls) as total_calls ,sum(total_time) as total_time" +
                " from (  select fn_gethour(call_date,2) call_date,total_calls ,total_time  " +
                " from rpt_call_log_activity1" +
                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress) + sSqlCustomerId +
                "  ) c " +
                " group by call_date    " +
                " order by call_date";

        String sSqlDay_steps = " select call_date,sum(total_calls) as total_calls ,sum(total_time) as total_time" +
                " from (  select fn_getday(call_date,2) call_date,total_calls ,total_time  " +
                " from rpt_call_log_activity1" +
                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress) + sSqlCustomerId +
                "  ) c " +
                " group by call_date    " +
                " order by call_date";

        String sSqlContacts_global = "select ifnull(contact_number,'') as contact_number,sum(total_calls) as total_calls " +
                "  from rpt_call_log_activity2  " +
                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  " +
                getHostnameQuery(ipAddress) + sSqlCustomerId +
                "  group by ifnull(contact_number,'')  "
                ;

        String sSqlData4 = "select ifnull(name,'OTHERS') as name,sum(total_calls) as total_calls,sum(total_time) as total_time "
                + " from rpt_call_log_activity1  "
                + " where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "  "
                + getHostnameQuery(ipAddress) + sSqlCustomerId
                + "  group by ifnull(name,'OTHERS') "
                ;

        String sSqlTotalContacts = "select count(*) as cc from (" +
                "  select contact_number,sum(total_time)" +
                "  from rpt_call_log_activity2  " +
                "  where call_date>=" + String.valueOf(starttimeOrig) + " and call_date<=" + String.valueOf(endtimeOrig) + "    " +
                getHostnameQuery(ipAddress) + sSqlCustomerId +
                "  group by contact_number" +
                ") c "
                ;

        System.out.println("sSqlActivity1:" + sSqlActivity1);
        System.out.println("sSqlActivity2:" + sSqlActivity2);
        System.out.println("sSqlContactsData:" + sSqlContactsData);
        System.out.println("sSqlHour_steps:" + sSqlHour_steps);
        System.out.println("sSqlDay_steps:" + sSqlDay_steps);
        System.out.println("sSqlContacts_global:" + sSqlContacts_global);
        System.out.println("sSqlData4:" + sSqlData4);
        System.out.println("sSqlTotalContacts:" + sSqlTotalContacts);


        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        List<Object> series = new ArrayList<Object>();

        series.clear();
        int total_calls = 0;
        int total_time = 0;
        int total_contacts = 0;
        try {
            Class.forName(classname);
            conn = DriverManager.getConnection(url);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sSqlActivity1);
            String rowkey = "";
            int iDateNo = 0;
            while (rs.next()) {
                total_calls += rs.getInt("total_calls");
                total_time += rs.getInt("total_time");
                String sDateNo = "";
                if (interval == 1) {
                    sDateNo = getDayHourStr(rs.getInt("call_date"), offset);
                } else if (interval == 2) {
                    //week
                    String mondayStr = getMondayStr(rs.getInt("call_date"), offset);
                    sDateNo = mondayStr;
                } else if (interval == 3) {
                    //month
                    String monthStr = getMonthStr(rs.getInt("call_date"), offset);
                    sDateNo = monthStr;
                } else {
                    //day
                    String dayStr = getDayStr(rs.getInt("call_date"), offset);
                    sDateNo = dayStr;
                }

//                System.out.println("call_date:"+rs.getString("call_date"));
//                System.out.println("sDateNo:"+sDateNo);
//                System.out.println("series_data:"+series_data);
//                System.out.println("db total_calls:"+rs.getString("total_calls"));

                activity_data.put(sDateNo, activity_data.get(sDateNo) + rs.getInt("total_calls"));

            }
            System.out.println("sSqlActivity1 end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

            //sSqlActivity2 data
            rs = stmt.executeQuery(sSqlActivity2);
            while (rs.next()) {
                rowkey = rs.getString("name");
                //detail
                if (contacts_name.get(rowkey) == null) {
                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
                    contacts_name.put(rowkey, tempMap);
                }
                Map<String, Integer> dataMap = contacts_name.get(rowkey);
                dataMap.put(rs.getString("contact_number"), rs.getInt("total_calls"));
                contacts_name.put(rowkey, dataMap);

                //total_contacts
                if (tsources_name.get(rowkey) == null) {
                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
                    tempMap.put("total_contacts",0);
                    tsources_name.put(rowkey, tempMap);
                }
                Map<String, Integer> tempMap = tsources_name.get(rowkey);
                tempMap.put("total_contacts",tempMap.get("total_contacts")+rs.getInt("total_calls"));
                tsources_name.put(rowkey, tempMap);
            }
            System.out.println("sSqlActivity2 end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

            //contacts_data
            rs = stmt.executeQuery(sSqlContactsData);
            while (rs.next()) {
                String sDateNo = "";
                if (interval == 1) {
                    sDateNo = getDayHourStr(rs.getInt("call_date"), 0);
                } else if (interval == 2) {
                    //week
                    String mondayStr = getMondayStr(rs.getInt("call_date")*100, 0);
                    sDateNo = mondayStr;
                } else if (interval == 3) {
                    //month
                    //setup to month 1th
                    String monthStr = getMonthStr(rs.getInt("call_date")*10000+0100, 0);
                    sDateNo = monthStr;
                } else {
                    //day, had offset on mysql
                    String dayStr = getDayStr(rs.getInt("call_date")*100,0);
                    sDateNo = dayStr;
                }

//                System.out.println("call_date:"+rs.getString("call_date"));
//                System.out.println("sDateNo:"+sDateNo);
//                System.out.println("series_data:"+series_data);
//                System.out.println("db total_calls:"+rs.getString("total_calls"));

                int cc = 1;
                contacts_data.put(sDateNo, contacts_data.get(sDateNo) + cc);

                //contacts_step
                if (contacts_step.get(sDateNo) == null) {
                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
                    tempMap.put("total_count", 0);
                    contacts_step.put(sDateNo, tempMap);
//                    System.out.println("contacts_step add sDateNo:" + sDateNo);
                }

                Map<String, Integer> tempMap = contacts_step.get(sDateNo);
                //detail
                tempMap.put("total_count",tempMap.get("total_count")+cc);
                //detail number count
                tempMap.put(rs.getString("contact_number"),rs.getInt("total_calls"));
                contacts_step.put(sDateNo, tempMap);
            }
            System.out.println("sSqlContactsData end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

            //hour_steps
            rs = stmt.executeQuery(sSqlHour_steps);
            while (rs.next()) {
                hour_steps.put(rs.getString("call_date"), rs.getInt("total_calls"));
            }
            System.out.println("sSqlHour_steps end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

            //day_steps
            rs = stmt.executeQuery(sSqlDay_steps);
            while (rs.next()) {
                day_steps.put(rs.getString("call_date"), rs.getInt("total_calls"));
            }
            System.out.println("sSqlDay_steps end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

            //contacts_global
            rs = stmt.executeQuery(sSqlContacts_global);
            while (rs.next()) {
                contacts_global.put(rs.getString("contact_number"), rs.getInt("total_calls"));
            }
            System.out.println("sSqlContacts_global end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

            //tsource name total calls and duration, and null count
            rs = stmt.executeQuery(sSqlData4);
            while (rs.next()) {
                rowkey = rs.getString("name");

                if (tsources_name.get(rowkey) == null) {
                    Map<String, Integer> tempMap = new HashMap<String, Integer>();
                    tempMap.put("total_contacts",0);
                    tempMap.put("total_calls", 0);
                    tempMap.put("total_time", 0);
                    tsources_name.put(rowkey, tempMap);
                }

                //detail
                Map<String, Integer> tempMap = tsources_name.get(rowkey);
                tempMap.put("total_calls", rs.getInt("total_calls"));
                tempMap.put("total_time", rs.getInt("total_time"));
                tsources_name.put(rowkey, tempMap);

            }
            System.out.println("sSqlData4 end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

            //total contacts
            //tsource name total calls and duration, and null count
            rs = stmt.executeQuery(sSqlTotalContacts);
            if (rs.next()) {
                total_contacts = rs.getInt("cc");
            }

            System.out.println("sSqlTotalContacts end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {

            }
        }
//        activeReport.put("series",series);
//        System.out.println(series_data.toString());
//        System.out.println(series_data_name.toString());
//        System.out.println(series_name.toString());
        StringBuilder sb = new StringBuilder();
        //output format
        sb.append("{\n");
        sb.append("\"" + String.valueOf(starttimeOrig) + " ~ " + String.valueOf(endtimeOrig) + "\": {\n");
        sb.append("\"total_calls\": " + String.valueOf(total_calls) + ",\n");
        sb.append("\"total_time\": " + String.valueOf(total_time) + ",\n");
        sb.append("\"total_contacts\": " + String.valueOf(total_contacts) + ",\n");
        Double average_time = total_time/(total_calls*1.0);
        sb.append("\"average_time\": " + String.valueOf(average_time) + ",\n");
        sb.append("\"color\": \"" + createRandomColor() + "\",\n");

        sb.append("\"timeline_chart\": {\n");
        sb.append("\"xAxis\": {\n");
        sb.append("\"categories\": [\n");
        for (int ii = 0; ii < dates.size(); ii++) {
            if ((ii + 1) == dates.size())
                sb.append("\"" + dates.get(ii) + "\"");
            else
                sb.append("\"" + dates.get(ii) + "\",");
        }
        sb.append("]\n");
        sb.append("},\n");
        sb.append("\"series\": {\n");
        sb.append("\"activity_data\": [\n");
        for (int ii = 0; ii < dates.size(); ii++) {
            if ((ii + 1) == dates.size())
                sb.append(String.valueOf(activity_data.get(dates.get(ii))) + "\n");
            else
                sb.append(String.valueOf(activity_data.get(dates.get(ii))) + ",\n");
        }
        sb.append("],\n");

        //contacts_data
        sb.append("\"contacts_data\": [\n");
        for (int ii = 0; ii < dates.size(); ii++) {
            if ((ii + 1) == dates.size())
                sb.append(String.valueOf(contacts_data.get(dates.get(ii))) + "\n");
            else
                sb.append(String.valueOf(contacts_data.get(dates.get(ii))) + ",\n");
        }
        sb.append("],\n");

        //contacts_step
        sb.append("\"contacts_step\": {\n");
        int ii = 0;
        for (String rowkey : contacts_step.keySet()) {
            Map<String, Integer> tempMap =contacts_step.get(rowkey);
            //get max
            String max_number = "";
            int max_number_value = 0;
            for (String sKey : tempMap.keySet()) {
                if (sKey.equals("total_count")) {
                    continue;
                }
                int iValue = tempMap.get(sKey);
                if (max_number.equals("") || max_number_value<iValue) {
                    max_number = sKey;
                    max_number_value = iValue;
                }
            }
            sb.append("\"" + rowkey + "\": {\n");

            sb.append("\"total_count\": "+tempMap.get("total_count")+",\n");
            sb.append("\"max\": {\n");
            sb.append("\"name\": \""+max_number+"\",\n");
            sb.append("\"value\": "+max_number_value+"\n");
            sb.append("},\n");

            int iNumberNo = 0;
            for (String sKey : tempMap.keySet()) {
                iNumberNo++;
                if (sKey.equals("total_count")) {
                    continue;
                }
                if (iNumberNo == tempMap.size())
                    sb.append("\""+sKey+"\": "+tempMap.get(sKey)+"\n");
                else
                    sb.append("\""+sKey+"\": "+tempMap.get(sKey)+",\n");
            }
            if ((ii + 1) == contacts_step.size())
                sb.append("}\n");
            else
                sb.append("},\n");
            ii++;
        }

        sb.append("}\n");
        sb.append("}\n");
        sb.append("},\n");

        //hour_steps
        sb.append("\"hour_steps\": {\n");
        int max_hour_value = 0;
        String max_hour = "";
        for (String sKey : hour_steps.keySet()) {
            if (max_hour.equals("") || max_hour_value<hour_steps.get(sKey)) {
                max_hour = sKey;
                max_hour_value = hour_steps.get(sKey);
            }
            sb.append("\""+sKey+"\": "+hour_steps.get(sKey)+",\n");
        }
        //hour_steps max
        sb.append("\"max\": {\n");
        sb.append("\"name\": "+max_hour+",\n");
        sb.append("\"value\": "+String.valueOf(max_hour_value)+"\n");
        sb.append("}\n");
        sb.append("},\n");

        //day_steps
        sb.append("\"day_steps\": {\n");
        int max_day_value = 0;
        String max_day = "";
        for (String sKey : day_steps.keySet()) {
            if (max_day.equals("") || max_day_value<day_steps.get(sKey)) {
                max_day = sKey;
                max_day_value = day_steps.get(sKey);
            }
        }
        //day_steps max
        sb.append("\"max\": {\n");
        sb.append("\"name\": \""+max_day+"\",\n");
        sb.append("\"value\": "+String.valueOf(max_day_value)+"\n");
        sb.append("},\n");

        int iCount = 0;
        for (String sKey : day_steps.keySet()) {
            if (iCount==day_steps.keySet().size()-1) {
                sb.append("\"" + sKey + "\": " + day_steps.get(sKey) + "\n");
            } else {
                sb.append("\"" + sKey + "\": " + day_steps.get(sKey) + ",\n");
            }
            iCount++;
        }
        sb.append("},\n");

        //contacts_blobal
        sb.append("\"contacts_global\": {\n");
        int max_contacts_global_value = 0;
        String max_contacts_global = "";
        for (String sKey : contacts_global.keySet()) {
            if (max_contacts_global.equals("") || max_contacts_global_value<contacts_global.get(sKey)) {
                max_contacts_global = sKey;
                max_contacts_global_value = contacts_global.get(sKey);
            }
        }
        //contacts_global max
        sb.append("\"max\": {\n");
        sb.append("\"name\": \""+max_contacts_global+"\",\n");
        sb.append("\"value\": "+String.valueOf(max_contacts_global_value)+"\n");
        sb.append("},\n");

        iCount = 0;
        for (String sKey : contacts_global.keySet()) {
            if (iCount==contacts_global.keySet().size()-1) {
                sb.append("\"" + sKey + "\": " + contacts_global.get(sKey) + "\n");
            } else {
                sb.append("\"" + sKey + "\": " + contacts_global.get(sKey) + ",\n");
            }
            iCount++;
        }
        sb.append("},\n");

        //tsources
        sb.append("\"tsources\": {\n");
        //max
        String max_name = "";
        int max_value = 0;
        for (String sKey : tsources_name.keySet()) {
            Map<String, Integer> dataMap2 = tsources_name.get(sKey);
            if (dataMap2 == null)
                continue;
            if (dataMap2.get("total_calls") == null)
                continue;
            if (max_name.equals("") || max_value<dataMap2.get("total_calls")) {
                max_name = sKey;
                max_value = dataMap2.get("total_calls");
            }
        }

        sb.append("\"max\": {\n");
        sb.append("\"name\": \""+max_name+"\",\n");
        sb.append("\"value\": "+String.valueOf(max_value)+"\n");
        sb.append("},\n");

        ii = 0;
        for (String sKey : tsources_name.keySet()) {
            Map<String, Integer> dataTsources_name = tsources_name.get(sKey);
            sb.append("\"" + sKey + "\": {\n");
            sb.append("\"total_calls\": " + String.valueOf(dataTsources_name.get("total_calls")) + ",\n");
            sb.append("\"total_duration\": " + String.valueOf(dataTsources_name.get("total_time")) + ",\n");
            sb.append("\"total_contacts\": " + String.valueOf(dataTsources_name.get("total_contacts")) + ",\n");
            //contacts
            //get null count
            int null_count = 0;
            if (dataTsources_name.get("total_calls")!=null && dataTsources_name.get("total_contacts")!=null)
                null_count= dataTsources_name.get("total_calls")-dataTsources_name.get("total_contacts");

            sb.append("\"contacts\": {\n");
            if (null_count>0) {
                if (dataTsources_name.get("total_contacts")==0) {
                    sb.append("\"null\": " + String.valueOf(null_count) );
                }
                else {
                    sb.append("\"null\": " + String.valueOf(null_count) + ",");
                }
            }
            Map<String, Integer> dataMap3 = contacts_name.get(sKey);
            if (dataMap3 != null) {
                int i = 0;
                for (String sKey2 : dataMap3.keySet()) {
                    if (i == 0) {
                        i = 1;
                    } else {
                        sb.append(",\n");
                    }
                    sb.append("\"" + sKey2 + "\": " + dataMap3.get(sKey2));
                }
                sb.append("\n");
            }
            sb.append("},\n");

            sb.append("\"color\": \"" + createRandomColor() + "\"\n");
            if (ii == tsources_name.keySet().size() - 1)
                sb.append("}\n");
            else
                sb.append("},\n");

            ii++;
        }
        sb.append("}\n");
        sb.append("}\n");
        sb.append("}\n");
        System.out.println("sb end:"+String.valueOf((int)((new Date()).getTime()-dtStart.getTime()))+" mil seconds");

        return sb.toString();
    }

}