package org.phm;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MainControl {

    public static void main( String[] args )
    {
        SpringApplication.run(MainControl.class, args);
     }
}


