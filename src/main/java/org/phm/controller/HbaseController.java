package org.phm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;
import org.phm.util.HbaseUtilNew;
import org.phm.util.MysqlUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import java.net.URLDecoder;

@RestController
@RequestMapping("/rest")
public class HbaseController {

    @RequestMapping(value = "/hbase/{id}", method = GET, produces = "application/json")
    public Map<String, String> getCDR(@PathVariable String id) {
        HbaseUtilNew mHbase = new HbaseUtilNew();
        Map<String, String> map = new HashMap<String, String>();
        System.out.println("id:" + id);
        map = mHbase.getOneTimeAndQualifierRecord("cdr", id);
        System.out.println(map.toString());
        return map;
    }

    @RequestMapping(value = "/CallLogs/create_activity_report", method = GET, produces = "application/json")
    public void activity_report_create(
            @RequestParam(value = "start_id", required = true) String start_id
            , @RequestParam(value = "end_id", required = true) String end_id
            , @RequestParam(value = "hostname", required = true) String hostname
            ,HttpServletRequest request) {
        //recovery call logs from hadoop to mysql
        String ipAddress = MysqlUtil.getIpAddress(request);
        if (ipAddress.equals("118.116.39.13")) {
            HbaseUtilNew mHbase = new HbaseUtilNew();
            Map<String, String> map = new HashMap<String, String>();
            System.out.println("activity_report_create");
            HbaseUtilNew hbase = new HbaseUtilNew();
            hbase.create_activity_report(hostname,start_id,end_id);
        }
    }

    @RequestMapping(value = "/CallLogs/re_create_reports", method = GET, produces = "application/json")
    public void re_create_reports(HttpServletRequest request) {
        //recovery call logs from hadoop to mysql
        String ipAddress = MysqlUtil.getIpAddress(request);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress); HbaseUtilNew mHbase = new HbaseUtilNew();
        Map<String, String> map = new HashMap<String, String>();
        System.out.println("re_create_reports");
        HbaseUtilNew hbase = new HbaseUtilNew();
        hbase.re_create_report(ipAddress);
    }

    @RequestMapping(value = "/CallLogs/addFromMysql", method = GET, produces = "application/json")
    public void call_addFromMysql(
            @RequestParam(value = "start_id", required = true) String start_id
            , @RequestParam(value = "end_id", required = true) String end_id
            ,HttpServletRequest request) {
        //recovery call logs from hadoop to mysql
        String ipAddress = MysqlUtil.getIpAddress(request);
        if (ipAddress.equals("110.184.178.165")||ipAddress.equals("117.173.70.242")) {
            HbaseUtilNew mHbase = new HbaseUtilNew();
            Map<String, String> map = new HashMap<String, String>();
            System.out.println("call_addFromMysql");
            HbaseUtilNew hbase = new HbaseUtilNew();
            ipAddress = "208.73.232.196";
            hbase.call_addFromMysql(ipAddress,start_id,end_id);
        }
    }
    @RequestMapping(value = "/CallLogs/recovery", method = GET, produces = "application/json")
    public void recoveryCallLogs(HttpServletRequest request) {
        //recovery call logs from hadoop to mysql
        String ipAddress = MysqlUtil.getIpAddress(request);
        if (ipAddress.equals("222.211.147.226")) {
            HbaseUtilNew mHbase = new HbaseUtilNew();
            Map<String, String> map = new HashMap<String, String>();
            System.out.println("recovery");
            HbaseUtilNew hbase = new HbaseUtilNew();
            ipAddress = "208.73.232.196";
            hbase.recoveryCallLog(ipAddress);
        }
    }
    @RequestMapping(value = "/CallLogs/testPhoenix", method = GET, produces = "application/json")
    public String testPhoenix(HttpServletRequest request) {
        //recovery call logs from hadoop to mysql
        String ipAddress = MysqlUtil.getIpAddress(request);
        HbaseUtilNew mHbase = new HbaseUtilNew();
        System.out.println("testPhoenix");
        HbaseUtilNew hbase = new HbaseUtilNew();
        return hbase.testPhoenix(ipAddress);
    }
    @RequestMapping(value = "/CallLogs/update_caller_number", method = GET, produces = "application/json")
    public void update_caller_number(HttpServletRequest request) {
        //update all hadoop data , setup fields to new cdrs
        String ipAddress = MysqlUtil.getIpAddress(request);
        if (ipAddress.equals("222.211.146.219")) {
            HbaseUtilNew mHbase = new HbaseUtilNew();
            Map<String, String> map = new HashMap<String, String>();
            System.out.println("222.211.146.219");
            HbaseUtilNew hbase = new HbaseUtilNew();
            hbase.update_caller_number();
        }
    }

    @RequestMapping(value = "/CallLogs/update20210319", method = GET, produces = "application/json")
    public void updateCallLogs20210319(HttpServletRequest request) {
        //update all hadoop data , setup fields to new cdrs
        String ipAddress = MysqlUtil.getIpAddress(request);
        if (ipAddress.equals("110.191.248.52")) {
            HbaseUtilNew mHbase = new HbaseUtilNew();
            Map<String, String> map = new HashMap<String, String>();
            System.out.println("update20210319");
            HbaseUtilNew hbase = new HbaseUtilNew();
            hbase.updateCallLogs20210319();
        }
    }
    /*
    get cdrs count
     */
    @RequestMapping(value = "/hbase/count", method = GET, produces = "application/json")
    public Map<String, Integer> getCDRCount() {
        HbaseUtilNew mHbase = new HbaseUtilNew();
        Date dateFirst = new Date();
        int iCount = mHbase.getTableCount("cdr");
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("cdr", iCount);

        Date now = new Date();
        long times = now.getTime() - dateFirst.getTime();
        long day = times / (24 * 60 * 60 * 1000);
        long hour = (times / (60 * 60 * 1000) - day * 24);
        long min = ((times / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long sec = (times / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        StringBuffer sb = new StringBuffer();
        if (day > 0) {
            sb.append(day + " day ");
        } else if (hour > 0) {
            sb.append(hour + " hour ");
        } else if (min > 0) {
            sb.append(min + " minute ");
        } else if (sec > 0) {
            sb.append(sec + " second ");
        } else if (times >= 0) {
            sb.append(times + " mil second ");
        } else {
            sb.append(" other :" + times);
        }

        System.out.println("get count takes:" + sb.toString());
        System.out.println(map.toString());
        return map;
    }

    @RequestMapping(value = "/rpt/actRpt", method = GET, produces = "application/json")
    public Map<String, Object> activity_report_old(@RequestParam(value = "start_date") String start_date
            , @RequestParam(value = "end_date") String end_date
            , @RequestParam(value = "view_by") String view_by
            , @RequestParam(value = "offset") String offset
            , @RequestParam(value = "interval") String interval) {
        HbaseUtilNew mHbase = new HbaseUtilNew();
        //setup parameter
        start_date = start_date.replace("-", "").substring(0, 8);
        end_date = end_date.replace("-", "").substring(0, 8);
        String sDate = start_date.substring(4, 5) + "-" + start_date.substring(6, 7);
//        System.out.println("start_date:"+start_date+" end_date:"+end_date);
        FilterList list = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        //start time
        BinaryComparator bc1 = new BinaryComparator(Bytes.toBytes(start_date));
        Filter f1 = new RowFilter(CompareFilter.CompareOp.GREATER_OR_EQUAL, bc1);
        list.addFilter(f1);
        //end time
        BinaryComparator bc2 = new BinaryComparator(Bytes.toBytes(end_date));
        Filter f2 = new RowFilter(CompareFilter.CompareOp.LESS_OR_EQUAL, bc2);
        list.addFilter(f2);

        Map<String, Object> map = new HashMap<String, Object>();
//        System.out.println("activity_report start");
        map = mHbase.selectLikeRowkeyFilter("cdr", list, Integer.valueOf(interval), sDate);
//        System.out.println(map.toString());
        return map;
    }


    @RequestMapping(value = "/CallLogs/activity_report", method = GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String activity_report(@RequestParam(value = "start_date") String start_date
            , @RequestParam(value = "end_date") String end_date
            , @RequestParam(value = "view_by") String view_by
            , @RequestParam(value = "offset") int offset
            , @RequestParam(value = "interval") String sInterval
            , @RequestParam(value = "customerId", required = false) String customerId
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);

        System.out.println("request ip:" + ipAddress);
        System.out.println("start_date:" + start_date);
        System.out.println("end_date:" + end_date);
        System.out.println("view_by:" + view_by);
        System.out.println("offset:" + offset);
        System.out.println("interval:" + sInterval);
        System.out.println("customerId:" + customerId);
        int interval = 0;
        view_by = (Integer.parseInt(view_by) <= 0) ? "2" : view_by;

        if (sInterval.equals("1") || sInterval.equals("hour"))
            interval = 1;
        else if (sInterval.equals("2") || sInterval.equals("week"))
            interval = 2;
        else if (sInterval.equals("3") || sInterval.equals("month"))
            interval = 3;
        else
            interval = 0;

        MysqlUtil mysqlUtil = new MysqlUtil();
        return mysqlUtil.getCallLogActivityReport(MysqlUtil.getDateInt(start_date), MysqlUtil.getDateInt(end_date),offset, interval,customerId, view_by, ipAddress);
    }

    @RequestMapping(value = "/CallLogs/overview_report", method = GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String overview_report(@RequestParam(value = "start_date") String start_date
            , @RequestParam(value = "end_date") String end_date
            , @RequestParam(value = "offset") int offset
            , @RequestParam(value = "customerId", required = false) String customerId
            , @RequestParam(value = "interval") String sInterval, HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);

        System.out.println("overview_report request ip:" + ipAddress);
        System.out.println("start_date:" + start_date);
        System.out.println("end_date:" + end_date);
        System.out.println("offset:" + offset);
        System.out.println("customerId:" + customerId);
        System.out.println("interval:" + sInterval);
        int interval = 0;

        if (sInterval.equals("1") || sInterval.equals("hour"))
            interval = 1;
        else if (sInterval.equals("2") || sInterval.equals("week"))
            interval = 2;
        else if (sInterval.equals("3") || sInterval.equals("month"))
            interval = 3;
        else
            interval = 0;

        MysqlUtil mysqlUtil = new MysqlUtil();
        return mysqlUtil.getOverViewReport(MysqlUtil.getDateInt(start_date)
                , MysqlUtil.getDateInt(end_date), offset, interval,customerId, ipAddress);
    }

    @Test
    public void TestDecode() throws UnsupportedEncodingException, JSONException {
//        String jsonStr = "%7B%22limit%22%3A10%2C%22skip%22%3A0%2C%22where%22%3A%7B%22or%22%3A%5B%7B%22Phonebook.name%22%3A%7B%22like%22%3A%22%25FFAL%25%22%7D%7D%5D%2C%22created%22%3A%7B%22between%22%3A%5B%222021-01-25T05%3A00%3A00.000Z%22%2C%222021-01-26T04%3A59%3A00.000Z%22%5D%7D%7D%2C%22order%22%3A%22created%20DESC%22%7D";
//        jsonStr = URLDecoder.decode(jsonStr, "UTF-8");
//        System.out.println(jsonStr);

        String jsonStr = "cdr=%7B%22net_quality%22%3A%22-%22%2C%22connc_time%22%3A%220%22%2C%22session_id%22%3A%229d90558f+00000000+02004cf0+6050c854%22%2C%22calling%22%3A%227863028580%22%2C%22start_time%22%3A%221615906900%22%2C%22duration%22%3A%220%22%2C%22term_cause%22%3A%22503_SERVICE_UNAVAIL%22%2C%22end_time%22%3A%221615906900%22%2C%22term_source%22%3A%22JoinedTermInd%22%2C%22nap%22%3A%22VEN_382_ATT_22%22%2C%22freed_time%22%3A%221615906900%22%2C%22mos%22%3A%22-%22%2C%22called%22%3A%228882404652%22%2C%22prosbc_drct%22%3A1%7D";
        if (jsonStr.startsWith("cdr=")) {
            jsonStr = URLDecoder.decode(jsonStr, "UTF-8");
            jsonStr = jsonStr.substring(4, jsonStr.length());
            System.out.println(jsonStr);
            JSONObject jsonObj = JSONObject.parseObject(jsonStr);
            System.out.println("session_id:" + jsonObj.getString("session_id"));
        }

//        int limit = jsonObj.getInteger("limit");

//
//
//        JSONObject jsonObj = JSONObject.parseObject(jsonStr);
//        System.out.println("limit:" + jsonObj.get("limit"));
//        int limit = jsonObj.getInteger("limit");
//        int skip = jsonObj.getInteger("skip");
//        String sWhere = jsonObj.getString("where");
//        System.out.println("where:" + sWhere);
//        JSONArray jsonArry = jsonObj.getJSONObject("where").getJSONObject("created").getJSONArray("between");
//        String sDateStart = jsonArry.getString(0).replace("-", "").replace("T", "").replace(":", "");
//        String sDateEnd = jsonArry.getString(1).replace("-", "").replace("T", "").replace(":", "");
//        int iStartDate = Integer.parseInt(sDateStart.substring(0, 10));
//        int iEndDate = Integer.parseInt(sDateEnd.substring(0, 10));
//        System.out.println("sDateStart:" + sDateStart);
//        System.out.println("sDateEnd:" + sDateEnd);
//        System.out.println("iStartDate:" + iStartDate);
//        System.out.println("iEndDate:" + iEndDate);
//        jsonStr = "%7B%22limit%22%3A10%2C%22skip%22%3A0%2C%22where%22%3A%7B%22or%22%3A%5B%7B%22Phonebook.name%22%3A%7B%22like%22%3A%22%25712%25%22%7D%7D%2C%7B%22Phonebook.city%22%3A%7B%22like%22%3A%22%25712%25%22%7D%7D%2C%7B%22Phonebook.state%22%3A%7B%22like%22%3A%22%25712%25%22%7D%7D%2C%7B%22Phonebook.country%22%3A%7B%22like%22%3A%22%25712%25%22%7D%7D%2C%7B%22OpNumber.TrackingSources.name%22%3A%7B%22like%22%3A%22%25712%25%22%7D%7D%2C%7B%22OpNumber.tracking_number%22%3A%7B%22like%22%3A%22%25712%25%22%7D%7D%2C%7B%22callerNumber%22%3A%7B%22like%22%3A%22%25712%25%22%7D%7D%5D%2C%22created%22%3A%7B%22between%22%3A%5B%222021-02-01T05%3A00%3A00.000Z%22%2C%222021-02-02T04%3A59%3A00.000Z%22%5D%7D%7D%2C%22order%22%3A%22created%20DESC%22%7D";
//        jsonStr = URLDecoder.decode(jsonStr, "UTF-8");
//        jsonStr = "%7B%22limit%22%3A10%2C%22skip%22%3A0%2C%22where%22%3A%7B%22created%22%3A%7B%22between%22%3A%5B%222021-01-25T05%3A00%3A00.000Z%22%2C%222021-01-26T04%3A59%3A00.000Z%22%5D%7D%7D%2C%22order%22%3A%22created%20DESC%22%7D";
//        jsonStr = URLDecoder.decode(jsonStr, "UTF-8");
//        System.out.println(jsonStr);
//        jsonObj = JSONObject.parseObject(jsonStr);
//        JSONArray jsonOR = jsonObj.getJSONObject("where").getJSONArray("or");
//        String sFilter = "";
//        if (jsonOR != null) {
//            sFilter = jsonOR.getJSONObject(0).getJSONObject("Phonebook.name").getString("like").replace("%", "");
//            System.out.println("sFilter:" + sFilter);
//        }
    }

    @RequestMapping(value = "/CallLogsExport", method = GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String calllog_filterExport(
            @RequestParam(value = "start_date", required = true) String start_date
            , @RequestParam(value = "end_date", required = true) String end_date
            , @RequestParam(value = "search") String search
            , @RequestParam(value = "fields") String fields
            , @RequestParam(value = "customerId", required = false) String customerId
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("calllog_filter request ip:" + ipAddress);
        System.out.println("start_date:" + start_date);
        System.out.println("end_date:" + end_date);
        System.out.println("search:" + search);
        System.out.println("fields:" + fields);
        System.out.println("customerId:" + customerId);

        String sDateStart = start_date.replace("-", "").replace("T", "").replace(":", "");
        String sDateEnd = end_date.replace("-", "").replace("T", "").replace(":", "");
        sDateStart = sDateStart.substring(0, 14);
        sDateEnd = sDateEnd.substring(0, 14);
        HbaseUtilNew hbase = new HbaseUtilNew();
        return hbase.getCDRsExport(search,fields,customerId, sDateStart, sDateEnd, ipAddress);
    }

    @RequestMapping(value = "/CallLogsCountTest", method = GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String calllog_filterCountTest(
            @RequestParam(value = "start_date", required = true) String start_date
            , @RequestParam(value = "end_date", required = true) String end_date
            , @RequestParam(value = "search", required = false) String search
            , @RequestParam(value = "fields", required = false) String fields
            , @RequestParam(value = "offset", required = false) Integer offset
            , @RequestParam(value = "hour", required = true) Integer hour //-1 mean all hours
            , @RequestParam(value = "interval", required = false) String sInterval
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);
        System.out.println("calllog_filter_count request ip:" + ipAddress);
        System.out.println("start_date:" + start_date);
        System.out.println("end_date:" + end_date);
        System.out.println("search:" + search);
        System.out.println("fields:" + fields);
        System.out.println("Interval:" + sInterval);
        System.out.println("offset:" + offset);

        try {
//            String jsonStr = URLDecoder.decode(sFilter, "UTF-8").replace("%","");
            String sDateStart = "";
            String sDateEnd = "";
            sDateStart = start_date.replace("-", "").replace("T", "").replace(":", "");
            sDateEnd = end_date.replace("-", "").replace("T", "").replace(":", "");
            sDateStart = sDateStart.substring(0, 14);
            sDateEnd = sDateEnd.substring(0, 14);
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.getCDRsCountTest(hour,search,fields,sInterval,offset, sDateStart, sDateEnd, ipAddress);
        } catch (Exception e) {
            System.out.println("calllog_filter exception:" + e.toString());
        }
        return "";
    }

    @RequestMapping(value = "/CallLogsExportTest", method = GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String calllog_filterExportTest(
            @RequestParam(value = "start_date", required = true) String start_date
            , @RequestParam(value = "end_date", required = true) String end_date
            , @RequestParam(value = "search", required = false) String search
            , @RequestParam(value = "fields", required = false) String fields
            , @RequestParam(value = "offset", required = false) Integer offset
            , @RequestParam(value = "hour", required = false) Integer hour
            , @RequestParam(value = "interval", required = false) String sInterval
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);
        System.out.println("calllog_filter_count request ip:" + ipAddress);
        System.out.println("start_date:" + start_date);
        System.out.println("end_date:" + end_date);
        System.out.println("search:" + search);
        System.out.println("fields:" + fields);
        System.out.println("Interval:" + sInterval);
        System.out.println("offset:" + offset);

        try {
//            String jsonStr = URLDecoder.decode(sFilter, "UTF-8").replace("%","");
            String sDateStart = "";
            String sDateEnd = "";
            sDateStart = start_date.replace("-", "").replace("T", "").replace(":", "");
            sDateEnd = end_date.replace("-", "").replace("T", "").replace(":", "");
            sDateStart = sDateStart.substring(0, 14);
            sDateEnd = sDateEnd.substring(0, 14);
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.getCDRsExportTest(hour,search,fields,sInterval,offset, sDateStart, sDateEnd, ipAddress);
        } catch (Exception e) {
            System.out.println("calllog_filter export exception:" + e.toString());
        }
        return "";
    }

    @RequestMapping(value = "/CallLogsCount", method = GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String calllog_filterCount(
            @RequestParam(value = "start_date", required = true) String start_date
            , @RequestParam(value = "end_date", required = true) String end_date
            , @RequestParam(value = "search") String search
            , @RequestParam(value = "customerId", required = false) String customerId
            , @RequestParam(value = "fields") String fields
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);


        System.out.println("calllog_filter_count request ip:" + ipAddress);
        System.out.println("start_date:" + start_date);
        System.out.println("end_date:" + end_date);
        System.out.println("search:" + search);
        System.out.println("fields:" + fields);
        System.out.println("customerId:" + customerId);

        try {
//            String jsonStr = URLDecoder.decode(sFilter, "UTF-8").replace("%","");
            String sDateStart = "";
            String sDateEnd = "";
            sDateStart = start_date.replace("-", "").replace("T", "").replace(":", "");
            sDateEnd = end_date.replace("-", "").replace("T", "").replace(":", "");
            sDateStart = sDateStart.substring(0, 14);
            sDateEnd = sDateEnd.substring(0, 14);
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.getCDRsCount(search,fields,customerId, sDateStart, sDateEnd, ipAddress);
        } catch (Exception e) {
            System.out.println("calllog_filter exception:" + e.toString());
        }
        return "";
    }




    @RequestMapping(value = "/CallLogs", method = GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String calllog_filter(
            @RequestParam(value = "start_date", required = true) String start_date
            , @RequestParam(value = "end_date", required = true) String end_date
            , @RequestParam(value = "limit", required = true) int limit
            , @RequestParam(value = "skip", required = true) int skip
            , @RequestParam(value = "search") String search
            , @RequestParam(value = "customerId", required = false) String customerId
            , @RequestParam(value = "fields") String fields
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);


        System.out.println("calllog_filter request ip:" + ipAddress);
        System.out.println("start_date:" + start_date);
        System.out.println("end_date:" + end_date);
        System.out.println("limit:" + limit);
        System.out.println("skip:" + skip);
        System.out.println("search:" + search);
        System.out.println("fields:" + fields);
        System.out.println("customerId:" + customerId);

        try {
//            String jsonStr = URLDecoder.decode(sFilter, "UTF-8").replace("%","");
            String sDateStart = "";
            String sDateEnd = "";
            sDateStart = start_date.replace("-", "").replace("T", "").replace(":", "");
            sDateEnd = end_date.replace("-", "").replace("T", "").replace(":", "");
            sDateStart = sDateStart.substring(0, 14);
            sDateEnd = sDateEnd.substring(0, 14);
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.getCDRs(search,fields, sDateStart, sDateEnd, ipAddress,customerId, limit, skip);
        } catch (Exception e) {
            System.out.println("calllog_filter exception:" + e.toString());
        }
        return "";
    }
    @RequestMapping(value = "/RawCdrs/Add", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String rawCdrs_add(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("rawCdrs_add request ip:" + ipAddress);
        System.out.println("rawCdrs_add json:" + sData);
//        JSONObject jsonObj = JSONObject.parseObject(sData);
//
//        try {
//            HbaseUtilNew hbase = new HbaseUtilNew();
////            return ""; //only test
//            return hbase.addCDRs(ipAddress, jsonObj);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "";
//        }
        //export
        String rowKey = ipAddress+"_000000001";
        StringBuilder sb = new StringBuilder();
        //output format
        sb.append("{\n");
        sb.append("\"id\":" + rowKey + "\n");
        sb.append("}\n");
        return sb.toString();
    }

    @RequestMapping(value = "/CallLogs/Add", method = POST, produces = "application/json; charset=utf-8")
//    @RequestMapping(value = "/CallLogs/Add", method = POST, produces = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseBody
    public String calllog_add(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        if (ipAddress.equals("208.73.232.209")) {
//            System.out.println("calllog_add request ip:" + ipAddress);
//            System.out.println("calllog_add json:" + sData);
//        }
        ipAddress = MysqlUtil.getHostname(ipAddress);
        JSONObject jsonObj = JSONObject.parseObject(sData);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.addCDRs(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/OpNumbers/UpdateTrackingSource", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String opNumber_updateTrackingSource(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("opNumber_update request ip:" + ipAddress);
        System.out.println("opNumber_update json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.update_opNumber_trackingSource(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    @RequestMapping(value = "/OpNumbers/Update", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String opNumber_update(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("opNumber_update request ip:" + ipAddress);
        System.out.println("opNumber_update json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.update_opNumber(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/OpNumbers/Delete", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String opNumber_delete(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("opNumber_delete request ip:" + ipAddress);
        System.out.println("opNumber_delete json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);
        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.delete_opNumber(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    //customers
    @RequestMapping(value = "/Customers/Update", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String customer_update(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("customer_update request ip:" + ipAddress);
        System.out.println("customer_update json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.update_customer(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/Customers/Delete", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String customer_delete(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("customer_delete request ip:" + ipAddress);
        System.out.println("customer_delete json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.delete_customer(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    @RequestMapping(value = "/OpNumbers/UpdateCustomer", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String opNumber_updateCustomer(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("opNumber_updateCustomer request ip:" + ipAddress);
        System.out.println("opNumber_updateCustomer json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);

        return "";

//        try {
//            HbaseUtilNew hbase = new HbaseUtilNew();
////            return ""; //only test
//            return hbase.update_customer(ipAddress, jsonObj);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "";
//        }
    }

    @RequestMapping(value = "/TrackingSources/Update", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String trackingSources_update(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("TrackingSources request ip:" + ipAddress);
        System.out.println("TrackingSources json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);
        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.update_trackingSources(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/TrackingSources/Delete", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String trackingSources_delete(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("trackingSources_delete request ip:" + ipAddress);
        System.out.println("trackingSources_delete json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);
        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.delete_trackingSources(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/TrackingSources/Sync", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String trackingSources_sync(HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("trackingSources_sync request ip:" + ipAddress);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);
        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.sync_trackingSources(ipAddress);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    @RequestMapping(value = "/ReceivingNumbers/Update", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String receivingNumbers_update(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

//        System.out.println("receivingNumbers_update request ip:" + ipAddress);
//        System.out.println("receivingNumbers_update json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);
        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.update_receivingNumber(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/ReceivingNumbers/Delete", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String receivingNumbers_delete(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("receivingNumbers_delete request ip:" + ipAddress);
        System.out.println("receivingNumbers_delete json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);
        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
            return hbase.delete_receivingNumber(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/SipGateways/Update", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String sipGateways_update(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);

        System.out.println("SipGateways request ip:" + ipAddress);
        System.out.println("SipGateways json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.update_sipGateways(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/SipGateways/Delete", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String sipGateways_delete(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);

        System.out.println("sipGateways_delete request ip:" + ipAddress);
        System.out.println("sipGateways_delete json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.delete_sipGateways(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/Phonebooks/Update", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String phonebooks_update(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);

        System.out.println("Phonebooks request ip:" + ipAddress);
        System.out.println("Phonebooks json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.update_phonebooks(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @RequestMapping(value = "/Phonebooks/Delete", method = POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String phonebooks_delete(@RequestBody(required=true) String sData
            , HttpServletRequest request, HttpServletResponse rsp) {
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        String ipAddress = MysqlUtil.getIpAddress(request);

        System.out.println("phonebooks_delete request ip:" + ipAddress);
        System.out.println("phonebooks_delete json:" + sData);
        JSONObject jsonObj = JSONObject.parseObject(sData);
        //change to server ip
        ipAddress = MysqlUtil.getHostname(ipAddress);

        try {
            HbaseUtilNew hbase = new HbaseUtilNew();
//            return ""; //only test
            return hbase.delete_phonebooks(ipAddress, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

//    public String calllog_filter(@RequestParam(value = "filter") String sFilter, HttpServletRequest request,HttpServletResponse rsp) {
//        rsp.addHeader("Access-Control-Allow-Origin", "*");
//        String ipAddress = MysqlUtil.getIpAddress(request);
//        ipAddress = MysqlUtil.getHostname(ipAddress);
//
//
//        System.out.println("calllog_filter request ip:"+ipAddress);
//        System.out.println("filter:"+sFilter);
//
//        try {
////            String jsonStr = URLDecoder.decode(sFilter, "UTF-8").replace("%","");
//            String jsonStr = sFilter.replace("%","");
//            System.out.println("jsonStr:"+jsonStr);
//            JSONObject jsonObj = JSONObject.parseObject(jsonStr);
//            int limit = jsonObj.getInteger("limit");
//            int skip = jsonObj.getInteger("skip");
//            String sWhere = jsonObj.getString("where");
//            System.out.println("limit:"+limit);
//            System.out.println("skip:"+skip);
//            System.out.println("where:"+sWhere);
//            JSONObject jsonWhere = jsonObj.getJSONObject("where");
//            sFilter = "";
//            String sDateStart = "";
//            String sDateEnd = "";
//            JSONArray jsonAnd =jsonWhere.getJSONArray("and");
//            if (jsonAnd!=null) {
//                System.out.println("get jsonAnd");
//
//                for (int ii=0;ii<jsonAnd.size();ii++) {
//                    JSONObject jObj = jsonAnd.getJSONObject(ii);
//                    if (sDateStart.equals("")) {
//                        JSONObject jCreated = jObj.getJSONObject("created");
//                        if (jCreated != null) {
//                            System.out.println("get jCreated");
//                            JSONArray jsonArry = jCreated.getJSONArray("between");
//                            sDateStart = jsonArry.getString(0).replace("-", "").replace("T", "").replace(":", "");
//                            sDateEnd = jsonArry.getString(1).replace("-", "").replace("T", "").replace(":", "");
//                            sDateStart = sDateStart.substring(0, 14);
//                            sDateEnd = sDateEnd.substring(0, 14);
//                        }
//                    }
//                    if (sFilter.equals("")) {
//                        String ss = jObj.getString("OpNumber.TrackingSources.name");
//                        if (ss != null) {
//                            System.out.println("get string OpNumber.TrackingSources.name");
//                            sFilter = ss;
//                        } else {
//                            JSONArray jsonOR = jObj.getJSONArray("or");
//                            if (jsonOR != null) {
//                                System.out.println("get jsonOR");
//                                for (int jj = 0;jj<jsonOR.size();jj++) {
//                                    JSONObject obj = jsonOR.getJSONObject(jj);
//                                    JSONObject oVal = obj.getJSONObject("OpNumber.TrackingSources.name");
//                                    if (oVal!=null){
//                                        sFilter = oVal.getString("like").replace("%", "");
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            } else {
//                System.out.println("jsonAnd null");
//                JSONArray jsonArry = jsonWhere.getJSONObject("created").getJSONArray("between");
//                sDateStart = jsonArry.getString(0).replace("-","").replace("T","").replace(":","");
//                sDateEnd = jsonArry.getString(1).replace("-","").replace("T","").replace(":","");
//                sDateStart = sDateStart.substring(0,14);
//                sDateEnd = sDateEnd.substring(0,14);
//            }
//            System.out.println("sFilter:"+sFilter);
//            HbaseUtilNew hbase = new HbaseUtilNew();
//            return hbase.getCDRs(sFilter,sDateStart,sDateEnd,ipAddress,limit,skip);
//        }
//        catch (Exception e) {
//            System.out.println("calllog_filter exception:"+e.toString());
//        }
//        return "";
//    }

}
